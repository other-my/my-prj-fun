#docker build <DOCKERFILE_PATH> --tag <IMAGE_NAME>
#<DOCKERFILE_PATH> - путь к файлу Dockerfile_one (. - текущая директория),
#<IMAGE_NAME> - имя, под которым образ будет создан
#Создаем все образы

docker build <DOCKERFILE_PATH> --tag <IMAGE_NAME>

../proxy-service/


docker build ./authentication-api/ --tag octory.ru/catalog-authentication:v1.0

docker build ./proxy-service/ --tag octory.ru/catalog-proxy:v1.0

docker build ./content-service/ --tag octory.ru/catalog-content:v1.0

docker build ./octory-email/ --tag octory.ru/catalog-email:v1.0

#web

docker build ./octory-marketplace-web/ --tag octory.ru/catalog-web:v1.0

#web email - пока не используем
docker build ./octory-marketplace-email-api/ --tag octory.ru/catalog-email:v1.0



# возможно запускать надо email-service
# проверить какой из них

# Запускаем Docker-compose
docker-compose up -d
#Вы также можете указать docker-compose запустить только один сервис, например
$ docker-compose up zuri -d

#Для просмотра запущенных контейнеров:
docker-compose ps

docker-compose down

docker exec -ti authentication_db-v01 /bin/sh

psql -U postgres -W postgres

\c authentication
\dt users.*
select * from users.users;
select * from users.roles;

docker logs authentication_db-v01
docker logs content_db-v01

docker logs catalog-authentication-v01
docker logs catalog-proxy-v01


docker logs catalog-email-v01
docker logs catalog-web-v01


