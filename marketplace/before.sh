01. Создаем дир для БД
02. Создаем БД

1. Создаем образы
  Посмотрим что у нас есть
  docker images
  Преходим в корневой каталог и запускаем создание образа
  Начнем с
  docker build ./authentication-api/ --tag octory.ru/authentication-service:v1.0
  проверяем   docker images

2. По аналогии создаем все остальные образы
  проверяем   docker images
3. С образом web придеьтся повозиться
  docker build ./octory-marketplace-web/ --tag octory.ru/marketplace-web:v1.0

  # Запустить контейнер с автоматическим удалением (после остановки)
  docker run --rm -ti -d --name marketplace-web octory.ru/marketplace-web:v1.0

  docker run --rm -ti -d -p 9007:80 --name marketplace-web octory.ru/marketplace-web:v1.0


  docker run --rm -ti -d -p 8080:8080 --name catalog-email octory.ru/catalog-email:v1.0

  docker exec -ti catalog-email /bin/sh

  docker logs catalog-email

  docker container inspect catalog-email

  # порт надо будет прописать в docker-compose

  # Войти в командную оболочку в контейнере
    docker exec -ti marketplace-web /bin/sh
  или
    docker exec -ti marketplace-web /bin/bash


  Остановим контейнер
    docker stop < name > если стоял флаг rm то он будет удален, если нет удалим его
    docker stop marketplace-web
    docker rm < name >
    docker rm marketplace-web
  удалим образ
    docker rmi < name >
    docker rmi octory.ru/marketplace-web:v1.0



docker ps -a


Это перенести в DockerFile
Создаем каталоги для БД - тут они все
mkdir -p /home/octory-db/authentication_db  && mkdir -p /home/octory-db/content_db &&
chmod 777 /home/octory-db/authentication_db && chmod 777 /home/octory-db/content_db

или так
sudo mkdir -p /home/octory-db/authentication_db
sudo mkdir -p /home/octory-db/content_db
sudo chmod 777 /home/octory-db/authentication_db
sudo chmod 777 /home/octory-db/content_db
