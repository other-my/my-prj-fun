
Теперь нам надо установить GitLab Runner
Открываем интсрукцию https://docs.gitlab.com/runner/install/


Set up a specific Runner for a project
Install GitLab Runner and ensure it's running. Установим раннер на своем сервере. Для этого набираем в поиске Gitlab Ubuntu Runner Repository именно через репозиторий а не инсталл, откроется вкладка с подрбной инструкцией.
https://docs.gitlab.com/runner/install/linux-repository.html





Дадим пользователяю Runner права на Docker
adduser gitlab-runner docker





По умолчанию, runner всегда будет пытаться скачать образ Docker с внешнего источника. Если на целевом компьютере, где запускается runner у нас уже есть нужный образ и мы хотим, чтобы использовался именно он, открываем файл:
vi /etc/gitlab-runner/config.toml

Среди:

runners

... находим созданный раннер (определяем по описанию, которое мы задавали при регистрации) и в нем также находим [runners.docker]. Добавим опцию pull_policy:

runners
...
[runners.docker]
...
pull_policy = "if-not-present"

Перезапустим сервис:

systemctl restart gitlab-runner

После установки gitlab-runner разрешаем автозапуск сервиса и стартуем его:

systemctl enable gitlab-runner --now

****************

Внутри контейнера выполним команду регистрации. Регистрация происходит в интерактивном режиме.

gitlab-runner register
Отвечаем на вопросы:

Runtime platform                                    arch=amd64 os=linux pid=27 revision=888ff53t version=13.8.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/):
http://git.company.name/
Enter the registration token:
vuQ6bcjuEPqc8dVRRhgY
Enter a description for the runner:
[c6558hyonbri]: runner_two
Enter tags for the runner (comma-separated):

Registering runner... succeeded                     runner=YJt3v3Qg
Enter an executor: parallels, shell, virtualbox, docker+machine, kubernetes, custom, docker, docker-ssh+machine, docker-ssh, ssh:
docker
Enter the default Docker image (for example, ruby:2.6):
maven:3.3.9-jdk-8
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded
Тут все просто:

Адрес вашего gitlab.
Токен авторизации. Посмотреть его можно в настройках гурппы/проекта в разделе CI/CD Runners.
Название раннера.
Теги ранера, можно пропустить нажав Enter.
Исполнитель сборки. Вводим docker.
Образ, который будет использоваться по умолчанию, если не установлен другой.
После этого в настройках проекта можно посмотреть доступные раннеры.

После регистрации, в папке /home/user/runner_name появится файл с настройками конфигурации config.toml. Нам нужно добавить docker volume для кэширования промежуточных результатов.

volumes = ["gitlab-runner-builds:/builds", "gitlab-runner-cache:/cache"]
Copy

Проблема кэширования

В начале статьи я рассказал о проблеме кеширования. Ее можно решить с помощью монтирования одного volume к разным раннерам. То есть во втором своем раннере так же укажите volumes = ["gitlab-runner-cache:/cache"].

Таким образом разные раннеры будут иметь единый кэш.

В итоге файл конфигурации выглядит так:

concurrent = 1
check_interval = 0

[session_server]
session_timeout = 1800

[[runners]]
name = "runner_name"
url = "gitlab_url"
token = "token_value"
executor = "docker"
[runners.custom_build_dir]
[runners.cache]
[runners.cache.s3]
[runners.cache.gcs]
[runners.cache.azure]
[runners.docker]
tls_verify = false
image = "maven:3.3.9-jdk-8"
privileged = false
disable_entrypoint_overwrite = false
oom_kill_disable = false
disable_cache = false
volumes = ["gitlab-runner-builds:/builds", "gitlab-runner-cache:/cache"]
shm_size = 0
Copy

После изменения перезапускаем раннер.

docker restart gitlab-runner-name


*************************

Для того чтобы убедится, что всё настроено и работает нормально выполните такую команду:

sudo gitlab-runner verify

Она должна показать сообщение is_alive. Настройка gitlab runner на сервере завершена.


***************************************

Допустим, у нас есть папка /storage, владельцем которой является пользователь /root:
ls -l / | grep storage
У папки такие права, что записывать в нее данные может только root, а другие пользователи могут их только смотреть. Нам надо предоставить к ней доступ на запись и чтение пользователю sergiy.
Давайте создадим группу storage и добавим в нее пользователя sergiy:
sudo groupadd storage
sudo usermod -aG storage sergiy
Смотрим список групп нашего пользователя:
groups sergiy
После добавления пользователя в группу нужно перелогиниться, чтобы система увидела это изменение. Затем меняем группу нашей папки на storage:
sudo chgrp storage /home/storage
Если нужно дать права на папку в Linux еще какому-нибудь пользователю, то достаточно добавить его в эту группу. Посмотрим список пользователей группы:
members storage
Проверим как изменились права у нашей папки
ls -l / | grep storage
Осталось подправить права для группы, нужно дать разрешение на чтение и запись:
chmod g+rw /home/storage

Чтобы посмотреть всех пользователей
cat /etc/passwd
Смотрим список групп у пользователя gitlab-runner
groups gitlab-runner

sudo groupadd octory
sudo usermod -aG octory gitlab-runner
sudo chgrp octory /home/octory-2022
chmod g+rw /home/octory-2022


