#DevOps
#DevOps-Skillbox

https://www.youtube.com/watch?v=gIluX7hkc6o

> Так как мы делали на обучении в реальной жизни ни когда делать не нужно - это не правильно деплоить прямо на машину , на девелопере еще куда не шло, см. разбор первой работы, когда имеется отдельно Runner и отдельно сервер, где Runner нет

Почитать https://habr.com/ru/company/softmart/blog/309102/

Знакомство CI\CD
![[Pasted image 20211229074938.png]]
Всегда надо делать как минимум две ветки - одна для продакш, вторая для разработки.
Под CD понимается два варианта представленных на рис. Или одни или другой, бывает что они оба реалзиованы в компании. Все завист от задач.
GitLab имеет встроенный механизм CiCD
Примерный процесс
![[Pasted image 20211229081300.png]]
В GitLab
![[Pasted image 20211229081355.png]]
Все это называется pipeline
![[Pasted image 20211229081544.png]]
А каждый процесс внутри называется Stage
![[Pasted image 20211229081656.png]]
А внтури него суцествует конечная задача Job
![[Pasted image 20211229081803.png]]
Т.е. Ci состоит из 
pipeline
	Stage	
		Job

pipeline для разных веток, один и тоже файл Ci может быть разным для разных веток
![[Pasted image 20211229082432.png]]
Верхняя это ветка Development

### Создадим вторую ветку develop
Смотрим ветки
`git branch`
Создадим вторую ветку develop
`git branch develop`
проверяем
`git branch`
Переходим во вновь созданную ветку develop.
`git checkout develop`

Открываем GitLab и переключимся на ветку develop
создаем файл .gitlab-ci.yml
можно его создать в локалке и запушить в гитлаб, нет разницы
и нажимаем +
![[Pasted image 20211229112034.png]]
Выбираем File задем его тип, тут можно сразу применить один из стандартных шаблонов
![[Pasted image 20211229112142.png]]

Создадим стадии, тут мы указываем в каком порядке они будут запускаться, причем их может быть больше чем реалзиций, пустые просто не будут выполнены и все
stages:
    - test
    - build
    - deploy
	
теперь опишем стадии

Т.к. билдить мы будем в докере, то нам необходимо его установить, это деалется как указано в нашем примере

откроем Container Registry
![[Pasted image 20211229113815.png]]
в котором приписаны все нужные нам команды, добавим их в наш файл

Т.к. авто конфиг не может см вводить юзер пароль, нам надо как то решить эту проблему 
В качестве юзера будетм использовать -u gitlab-ci-token

Открываем старницу https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
и там найдем все #Predefined-variables-reference
Их все моно использовать в конфигурации. Находим там `CI_JOB_TOKEN`, это спец токен который генерируется для Job от имени юзера ,который запустил pipeline
Т.е. по этому токену CI будет авторизован от имени юзера, который его запустил и будет иметь все его права.
ее и будем использовать в качестве пароля
-p ${CI_JOB_TOKEN}

далее идет адрес сервера registry.gitlab.com
можно заменить его тоже на переменную {CI_REGISTRY}
так проще перносить конфиг на другой репо
Аналогично поступим и с именем нашего проекта
alexneon002/docker-cicd
``${CI_PROJECT_PATH}/docker-cicd`
`${CI_PROJECT_PATH}/${CI_PROJECT_NAME}`
Теперь добавим тег для образа
CI_COMMIT_REF_SLUG - имя ветки
Можно данной версии дать название latest, это не обязательно, просто для тех кто любит исползовать latest 

Вот файл полностью
```
        - docker tag ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}
                     ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:latest
```
и запушим его тоже
> Для продакшена так (latest) ни когда не делать!!!

```
# описываем все этапы
stages:
    - test
    - build
    - deploy

# описываем этап test
django test:
    stage: test
    image: python:3-alpine
    script:
        - pip install -r requirements.txt
        - python manage.py test --noinput

# описываем этап test
docker build:
    stage: build
    image: docker:stable
    services:
        - docker: dind
    script:
        - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
        - docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG} .
        - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}
        - docker tag ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}
                     ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:latest
        - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:latest 
````
Коммитим изменения и проверяем что все запущено
![[Pasted image 20211229133627.png]]
> Чтобы CI заработал необходимо привязать карту к аккаунту

Открываем pipeline и видим что все запущено, тесты пройдены
![[Pasted image 20211229134014.png]]
Можно щелкнуть по Id и открыть более подробную инфу
посмотерть как прошли тесты и тд
![[Pasted image 20211229134202.png]]
тут мы видим что тесты и сборка прошли успешно
Заходим в ## Container Registry и видим там наш образ и два тега
![[Pasted image 20211229134518.png]]

Т.е. мы выполнили тестирование  проекта и собрали образ на основе нашего Dockerfile в проекте
Причем этот образ можно скопровать (рядом с ним иконка). И использовать на своем локальном компе или ваще где угодно. 

## Deploy
А теперь задеплоим это все на сервер
Заходим на наш сервер
например
ssh root@188.130.139.104
под виндой через Putty
188.130.139.104
Теперь нам надо установить GitLab Runner
Открываем интсрукцию https://docs.gitlab.com/runner/install/

To install GitLab Runner:

1.  Add the official GitLab repository:
    
    For Debian/Ubuntu/Mint:
    
    ```
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    ```
        
    Debian users should use [APT pinning](https://docs.gitlab.com/runner/install/linux-repository.html#apt-pinning).
    
2.  Install the latest version of GitLab Runner, or skip to the next step to install a specific version:
    
    [Starting with GitLab Runner 14.0](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4845) the `skel` directory usage is [disabled](https://docs.gitlab.com/runner/install/linux-repository.html#disable-skel) by default to prevent [`No such file or directory` job failures](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1379)
    
    For Debian/Ubuntu/Mint:
    
    ```
    sudo apt-get install gitlab-runner
    ```
       
    ### Это делать не нужно
	
3.  To install a specific version of GitLab Runner:
    
    For DEB based systems:
    
    ```
    apt-cache madison gitlab-runner
    sudo apt-get install gitlab-runner=10.0.0
    ```
    
    For RPM based systems:
    
    ```
    yum list gitlab-runner --showduplicates | sort -r
    sudo yum install gitlab-runner-10.0.0-1
    ```
    
4.  [Register a runner](https://docs.gitlab.com/runner/register/index.html).
    

After completing the step above, a runner should be started and be ready to be used by your projects!

Make sure that you read the [FAQ](https://docs.gitlab.com/runner/faq/index.html) section which describes some of the most common problems with GitLab Runner.

Выполняем все эти команды

### Устанавливаем Docker
Ставим Docker, если его нет на сервере.
Дадим пользователяю Runner права на Docker
`adduser gitlab-runner docker`

#Регистрация-Runner
Далее идем в GitLab
Setting-> CI/CD -> Runner
![[Pasted image 20211229142634.png]]

Копируем адрес нашего проекта и токен его
![[Pasted image 20211229143454.png]]
и производим регистрацию нашего gitlab runner на сервере

`gitlab-runner register`
он справшивает адрес нашего проекта и токен его (это берем со страницы настроек gitlab)
далее спрашивает название: dev-shell
далее спрашивает таг: dev-shell можно два тега задать dev, docker
далее спрашивает экзекьютер: shell
На всякий случай сделаем еще один runner, хотя он особо не нужен
`gitlab-runner register`
далее спрашивает название: dev-docker
далее спрашивает таг: можно два тега задать dev-docker, docker
далее спрашивает экзекьютер: docker
если экзекьютер выбрать docker, то спросит на каком  image будет стоиться  по умолчанию, например указать python:3-alpine

Обновим страницу гитлабе и увидим два наших раннера
![[Pasted image 20211229144928.png]]

Т.е. мы получили, что у нас есть отдельный сервер, который работает как Runner И который может выполнять нащи задачи
Добавим в наш конфиг новый Job
```
Deploy to dev:

	stage: deploy

	script:

		- docker rm -f django || true

		- docker run -d -p 8539:8000 --name django registry.gitlab.com/alexneon002/docker-cicd/docker-cicd

	tags:

		- dev-shell
```
image тут не нужен, т.к. это shell экзекьютер
надо указать что мы хотим выпонить этот job на раннере который на сервере
У нашего раннера есть тег dev-shell, по нему мы и укажем где выполнять нашу задачу
tags: dev-shell
дабавим
script:
	- docker run -d -p 8539:8000 --name django registry.gitlab.com/alexneon002/docker-cicd/docker-cicd
имя контейнера можно найти в нашем job deploy
добавим 
	- docker rm -f django || true
это мы проверяем, если контейнер сущестует, то сначала удалим его
Нажимае коммит и все должно запуститься, и деплой.

Если репозиторий private, то такая схема не запускается.
Необходимо зайти под пользователем gitlab-runner
Посмотреть всех подльзователей можно командой 
`cat /etc/passwd`
Переключимся на него
`su - gitlab-runner`
Необходимо залогиться от докера
`docker login https://registry.gitlab.com`
После этого заработает
Все лоигны хранятся в файле ~/.docker/config.json
Чтобы посмотреть скрытые файлы, надо выполнить
`ls -a`
Чтобы разлогиниться надо выполнить
`docker logout https://registry.gitlab.com`
Вариант второй - как можно решить эту проблему в CI
добавим ввод пароля в деплоее, вот так, что то у меня так не сработало. Хотя может проблема была не в этом
```
Deploy to dev:
 stage: deploy
 script:
 	- docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
 	- docker rm -f django || true
 	- docker run -d -p 8539:8000 --name django registry.gitlab.com/alexneon002/docker-cicd/docker-cicd:develop
 tags: 
 	- dev-shell
``` 

Чтобы обновлять образы добавим в деплой команду
`- docker pull registry.gitlab.com/alexneon002/docker-cicd/docker-cicd:develop`

Разрешим все хосты исправив файл
mysite/settings.py
испрвив
ALLOWED_HOSTS = ['*']

Чтобы реализовать проект на микросервисах, я так думаю, что надо прописать все билды, всех сервисах
А в деплое запускать docker-compose

Осталось решить проблему с копированием файла docker-compose на сервер
Попробовать сделать это в секции билд. 
Ranner получает доступ ко всем файлам проекта и любой из них моно скопировать.
Т.к. когда он запускается он скачивает к себе состояние коммита, т.е. все, что у вас есть в коммите у вас окажется на сервере в специально дирреуктории билд раннера. Т.е. специально не надо ни чего перекидывать. Смотрим вторую работу там есть пример как его перекидывать на сервер


Вот инфа
Программа уже скачивает исходники на сервер, но дальше вы можете сделать с ними всё что хотите. Настраивать раннер загружать исходники прямо в папку веб-сервера не стоит, программа для этого не предназначена. Для копирования исходников лучше использовать rsync. Эта утилита позволяет копировать только изменившиеся файлы. Например, для копирования файлов из репозитория в **/var/www/project** необходимо добавить такую команду в секцию script:

`rsync -av --no-perms --no-owner --no-group --exclude ".git*" $CI_PROJECT_DIR/ /var/www/project`

