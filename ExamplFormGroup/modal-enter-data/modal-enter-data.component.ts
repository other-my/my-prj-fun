import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalRegisterComponent} from "../modal-register/modal-register.component";
import {ModalRegisterNextComponent} from "../modal-register-next/modal-register-next.component";
import {ModalEnterNameComponent} from "../modal-enter-name/modal-enter-name.component";
import {DataService} from "../../services/data.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Answer, ResponseData} from "../../shared/classes/rest-data/ResponseData";
import {ModalErrorComponent} from "../modal-error/modal-error.component";
import {EMPTY} from "rxjs";
import {DialogErrorHttpComponent} from "../../components/dialog/dialog-error-http/dialog-error-http.component";
import {MatDialog} from "@angular/material/dialog";
import {Md5} from "ts-md5";
import {CheckCode} from "../../model/CheckCode";
import {SendAuth} from "../../shared/classes/rest-data/SendAuth";

/**
 * modal-04
 * Форма ввода кода с смс и отправка его на проверку в бэк
 */
@Component({
  selector: 'app-modal-enter-data',
  templateUrl: './modal-enter-data.component.html',
  styleUrls: ['./modal-enter-data.component.css']
})
export class ModalEnterDataComponent implements OnInit {

  // @Input() public codeValidation: string = "sa";
  public codeValidation!: string;
  public phoneNumber!: string;
  public userCheckCode!: CheckCode;

  codeForm : FormGroup = new FormGroup({
    codeByte: new FormControl('', [
      Validators.required,
      Validators.maxLength(4),
      Validators.min(4),
      Validators.pattern("^[0-9]{4,4}$")
    ]),
  });


  constructor(
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private authService: AuthService,
    private dialog: MatDialog
  ) { }

  get codeByte() { return this.codeForm.get('codeByte')!; }

  ngOnInit(): void {
    this.dataService.codeValueOb.subscribe(
      result=>{
        this.codeValidation = result;
      });

    // Прочитаем номер тел
    this.dataService.phoneNumberOb.subscribe(
      result=>{
        this.phoneNumber = result;
      });


    console.log("codeValue = " + this.codeValidation);
  }

  /**
   * Отпарвка кода на проверку
   */
  sendCodeAndGetAuth() {
    console.log("next");

    if (this.codeForm && this.codeForm.value == null) {
      console.log("Error ");
      return;
    }
    this.codeValidation = this.codeForm.value.codeByte.toString();
    this.codeValidation = "1111";
    console.log("codeValidation = " , this.codeValidation);


    this.userCheckCode = new CheckCode(
      this.phoneNumber,
    "client",
      this.codeValidation,
      0
      // this.getPhoneMd5(this.phoneNumber)
    );

    console.log("phone = " + this.userCheckCode._phone);
    console.log("role = " + this.userCheckCode._role);
    console.log("uid = " + this.userCheckCode._id);
    console.log("code = " + this.userCheckCode._code);


    //8356532279 - Этот номер заведен на 1111

    this.authService.sendCheckCode(this.userCheckCode).subscribe(
      (result: Answer) => {
        console.log(" sendCheckCode Answer");
        if (result.status.code === 200) {
          console.log(" sendCheckCode status.code === 200 ");
          const authInfo = result.responce as SendAuth;

          console.log(" token =  ", authInfo.token);
          this.authService.setCookie(JSON.stringify(authInfo));
          this.next();

          // прочитаем
          // this.Category = result.responce as EmployeeOwner[];

        }
      }

    )
  }

  next() {
    this.activeModal.close();
    const modalRef = this.modalService.open(ModalEnterNameComponent);
    modalRef.componentInstance.name = 'World';
  }


  back() {
    this.activeModal.close();
    const modalRef = this.modalService.open(ModalRegisterComponent);
    modalRef.componentInstance.name = 'World';
  }

  getPhoneMd5(phone: string): string {
    let md5 = new Md5();
    const hashMd5 = md5.appendStr(phone).end().toString();
    console.log("hashMd5 = " + hashMd5)
    return hashMd5;
  }


}
