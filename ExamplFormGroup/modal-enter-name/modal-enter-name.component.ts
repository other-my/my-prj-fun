import { Component, OnInit } from '@angular/core';
import {ModalEnterDataComponent} from "../modal-enter-data/modal-enter-data.component";
import {ModalNextComponent} from "../modal-next/modal-next.component";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalRegisterEndComponent} from "../modal-register-end/modal-register-end.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SendAuth} from "../../shared/classes/rest-data/SendAuth";
import {DataService} from "../../services/data.service";
import {AuthService} from "../../services/auth.service";
import {CookieService} from "ngx-cookie-service";
import {Auth} from "../../shared/classes/auth/Auth";
import {Answer} from "../../shared/classes/rest-data/ResponseData";
import {Account} from "../../shared/classes/account/Account";
import {AccountDto} from "../../shared/classes/account/AccountDto";

/**
 * modal-05
 * Форма ввода имени ползователя
 */
@Component({
  selector: 'app-modal-enter-name',
  templateUrl: './modal-enter-name.component.html',
  styleUrls: ['./modal-enter-name.component.css']
})
export class ModalEnterNameComponent implements OnInit {

  nameForm!: FormGroup;
  private authInfo!: SendAuth;
  private user! : Auth;
  private account!: AccountDto;

  private name!: string;
  private lastName!: string;


  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private dataService: DataService,
    private cookieService: CookieService,
    private authService: AuthService,
    public activeModal: NgbActiveModal) {
    this.createForm();
  }

  ngOnInit(): void {
    this.dataService.authInfo$.subscribe(result => {
      let curToken = result.token;
      if (curToken === undefined || curToken === null || curToken.length === 0) {
        this.authInfo = JSON.parse(this.cookieService.get( 'authInfo'));
        this.dataService.sendAuthInfoBacket(this.authInfo);
      } else {
        this.authInfo = result;
      }
    });
  }

  createForm() {
    this.nameForm = this.formBuilder.group({
      name: [null, Validators.required],
      lastName: [null, Validators.required]
    });
  }

  checkData() {
    let name = this.nameForm.get('name')?.value;
    let lastName = this.nameForm.get('lastName')?.value;

    if (name && lastName && name.length > 0 && lastName.length > 0) {
      console.log(" get token checkData =  ", this.authInfo.token);

      this.authService.getUser(this.authInfo.token).subscribe(
        (result: Answer) => {
          if (result.status.code === 200){
            this.user = result.responce as Auth;
            console.log("this.user = " , this.user);
            console.log("name = " , name);

            this.account = new AccountDto();
            this.account._name = name;
            this.account._family = lastName;
            this.authService.addClient(this.authInfo.token, this.account).subscribe(
              (resultClient: Answer) => {
                if (resultClient.status.code === 200){
                  this.next();
                } else {
                  console.log("Данные по клиенту не удалось сохранить");
                }
              })
          }
        })
    }


  }

  next() {
    this.activeModal.close();
    const modalRef = this.modalService.open(ModalRegisterEndComponent);
    modalRef.componentInstance.name = 'World';
  }

  back() {
    this.activeModal.close();
    const modalRef = this.modalService.open(ModalEnterDataComponent);
    modalRef.componentInstance.name = 'World';
  }


}
