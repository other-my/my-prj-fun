import { Component, OnInit } from '@angular/core';
import {ValidationRegisterService} from "../../../../core/authentication/validation-register.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../../../core/authentication/auth.service";

/**
 * Форма регистрации
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    public auth: AuthService,
    private router: Router,
    // public renderComponentService: RenderComponentService,

    private fb: FormBuilder, /*unused import of validation service*/
    public validation: ValidationRegisterService) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  regForm!: FormGroup;
  display = 'none';
  message!: string;

  // unused field with interface RegisterData, maybe i should make reference directly from Reactive Form.
  // userRegister: RegisterData = {
  //   email: null,
  //   password: null,
  //   firstName: null,
  //   lastName: null,
  //   taxIdentificationNumber: null,
  //   codeReason: null,
  //   address: null,
  //   msisdn: null,
  //   isMarketplace: null,
  //   nameMarketPlace: null
  // };

  createForm() {
    this.regForm = this.fb.group({
      email: [null, [Validators.required,
        Validators.email]],
      password: [null, [Validators.required,
        Validators.minLength(5)]],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      taxIdentificationNumber: [null, [Validators.required,
        Validators.pattern(/^[0-9]*$/)]],
      codeReason: [null, [Validators.required,
        Validators.pattern(/^[0-9]*$/)]],
      address: [null, Validators.required],
      msisdn: [null, [Validators.required,
        Validators.pattern(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)]],
      isMarketplace: [null, Validators.required],
      nameMarketPlace: [null, Validators.required],
    });
  }



  register(): any {
    this.auth.registerProxy(this.regForm.value).subscribe(data => {
      switch (data) {
        case 'SUCCESS':
          this.message = 'Регистрация прошла успешно!';
          console.log("this.message = " + this.message);

          // Открываем окно ConfirmComponent подтверждение регистрации
          this.router.navigate(['confirm']);
          // this.renderComponentService.renderAuth(2);
          break;

        case 'FAIL':
          this.message = 'Произошла ошибка сервера, пройдите регистрацию еще раз';
          break;

        case 'PHONE_EXISTS':
          this.message = 'Данный номер телефона уже зарегистрирован';
          break;

        case 'PASSWORD_INCORRECT':
          this.message = 'Неправильный пароль';
          break;

        case 'CODE_REASON_EXISTS':
          this.message = 'Данный номер КПП уже зарегистрирован';
          break;

        case 'IDENTIFICATION_NUMBER_EXISTS':
          this.message = 'Данный номер ИНН уже зарегистрирован';
          break;

        case 'ALREADY_EXISTS':
          this.message = 'Аккаунт уже зарегистрирован';
          break;
      }
      this.openModal();
    });

  }

  openModal(): void {
    this.display = 'block';
  }

  onCloseHandled(): void {
    this.display = 'none';
  }

}
