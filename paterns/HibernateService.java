public interface HibernateService<T extends Identifiable<I>, I extends Serializable>
        extends CrudService<T, I> {

    Optional<T> searchUnique(String property, Object value);

    Optional<T> searchUnique(List<Filter> filters);

    ListResult<T> findAll();

    int count();


    String convertToStringIds(Collection<T> entities);
}
