@Slf4j
public class CurrencyController
        extends AbstractCrudRestController<String, CurrencyEntity, Currency, CurrencyService,
        Converter<CurrencyEntity, Currency>, Converter<Currency, CurrencyEntity>>
        implements CurrencyResource {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + Currency.RESOURCE;

    @Autowired
    public CurrencyController(
            CurrencyService currencyService,
            Converter<CurrencyEntity, Currency> entityToDtoConverter,
            Converter<Currency, CurrencyEntity> dtoToEntityConverter) {
        super(currencyService, entityToDtoConverter, dtoToEntityConverter);
    }

    @ApiOperation(value = "Получить валюту")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = Currency.class)})
    @GetMapping(value = RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<Currency>> get(
            @PathVariable(RestApiConstants.PARAM_ID) String id) {
        return getEntity(id);
    }

    @ApiOperation(value = "Создать валюту")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_CREATED, message = RestApiConstants.MESSAGE_CREATED,
                    response = Currency.class)})
    @PostMapping
    @RestApiValidation(RestApiValidatorConfig.CONSTRAINTS_REST_API_VALIDATOR)
    public ResponseEntity<DataResponse<Currency>> post(@RequestBody DataRequest<Currency> request) {
        return postEntity(request);
    }

    @ApiOperation(value = "Обновить валюту")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = Currency.class)})
    @PutMapping(value = RestApiConstants.VARIABLE_ID)
    @RestApiValidation(RestApiValidatorConfig.CONSTRAINTS_REST_API_VALIDATOR)
    public ResponseEntity<DataResponse<Currency>> put(
            @PathVariable(RestApiConstants.PARAM_ID) String id,
            @RequestBody DataRequest<Currency> request) {
        return putEntity(id, request);
    }


}
