package ru.home.myfirstdevops.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.home.myfirstdevops.dto.User;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    private UserService userService;

    /**
     * Перед каждым тестом чистим список
     */
    @BeforeEach
    void prepare() {
        System.out.println("Before each: " + this);
        userService = new UserService();
    }

    @Test
    void usersEmptyIfNoUserAdded() {
        System.out.println("Test 1: " + this);
        var users = userService.getAll();
        assertTrue(users.isEmpty(), () -> "User list should be empty");
    }

    @Test
    void usersSizeIfUserAdded() {
        userService.addUser(new User());
        userService.addUser(new User());

        var users = userService.getAll();
        assertEquals(2, users.size());
    }

    @Test
    void usersSizeIfUserAddedBeforeDelete() {
        userService.addUser(new User(1L, "01", "c-01"));
        userService.addUser(new User(2L, "02", "c-02"));
        userService.addUser(new User(3L, "03", "c-03"));
        userService.addUser(new User(4L, "04", "c-04"));
        userService.addUser(new User(5L, "05", "c-05"));

        userService.deleteUser(2L);
        userService.deleteUser(5L);

        var users = userService.getAll();
        assertEquals(3, users.size());
    }

    @Test
    void userGetIfUserAdd() {
        User user = new User(1L, "Hans", "Germany");
        userService.addUser(user);
        assertEquals("Hans", userService.getUser(1L).getName());
        assertEquals("Germany", userService.getUser(1L).getCountry());
    }

    @DisplayName("Get User If Add")
    @ParameterizedTest
    @CsvSource(value = {
            "1, Hans, Germany",
            "2, Иван, Russian",
            "3, Reno, France"
    })
    void usersGetIfUserAdded(Long id, String name, String country) {
        User user = new User(id, name, country);
        userService.addUser(user);
        assertEquals(name, userService.getUser(id).getName());
        assertEquals(country, userService.getUser(id).getCountry());
    }

}
