package ru.home.myfirstdevops.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.home.myfirstdevops.dto.Car;
import ru.home.myfirstdevops.dto.User;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CarServiceIT {

    @Autowired
    private CarService carService;

    /**
     * Перед каждым тестом чистим список
     */
    @BeforeEach
    void prepare() {
        System.out.println("Before each: --- ");
        carService.deleteAllCars();
    }

    @Test
    void usersSizeIfUserAdded() {
        System.out.println("First IT Test: ");
        carService.addCar(new Car());
        carService.addCar(new Car());

        var cars = carService.getAll();
        assertEquals(2, cars.size());
    }

    @Test
    void usersSizeIfUserAddedBeforeDelete() {
        carService.addCar(new Car(1L, "BMW", "01", "X"));
        carService.addCar(new Car(2L, "BMW", "02", "X"));
        carService.addCar(new Car(3L, "BMW", "03", "X"));
        carService.addCar(new Car(4L, "BMW", "04", "X"));
        carService.addCar(new Car(5L, "BMW", "05", "X"));

        carService.deleteCar(2L);
        carService.deleteCar(5L);

        var cars = carService.getAll();
        assertEquals(3, cars.size());
    }

    @Test
    void userGetIfUserAdd() {
        Car car = new Car(1L, "BMW", "01", "X");
        carService.addCar(car);
        assertEquals("BMW", carService.getCar(1L).getManufacture());
        assertEquals("01", carService.getCar(1L).getName());
        assertEquals("X", carService.getCar(1L).getModel());
    }



}
