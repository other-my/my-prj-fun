package ru.home.myfirstdevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstDevopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyFirstDevopsApplication.class, args);
    }

}
