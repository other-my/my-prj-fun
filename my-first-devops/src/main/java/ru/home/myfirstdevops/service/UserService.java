package ru.home.myfirstdevops.service;

import ru.home.myfirstdevops.dto.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Это будет обычный класс
 */
public class UserService {

    private final List<User> users = new ArrayList<>();

    public List<User> getAll() {
        return users;
    }

    public User getUser(Long id) {
        User found;
        found = null;
        for (User userList : users) {
            if (Objects.equals(userList.getId(), id)) {
                found = userList;
                break;
            }
        }
        return found;
    }

    public void addUser(User user) {
        users.add(user);
    }

    public User deleteUser(Long id) {
        User user = getUser(id);
        if (user != null) {
            users.remove(user);
            return user;
        }
        return null;
    }

}
