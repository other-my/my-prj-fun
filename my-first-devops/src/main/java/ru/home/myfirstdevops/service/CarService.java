package ru.home.myfirstdevops.service;

import org.springframework.stereotype.Service;
import ru.home.myfirstdevops.dto.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Это будет Spring Сервис
 */
@Service
public class CarService {

    private final List<Car> cars = new ArrayList<>();

    public List<Car> getAll() {
        return cars;
    }

    public Car getCar(Long id) {
        Car found;
        found = null;
        for (Car carList : cars) {
            if (Objects.equals(carList.getId(), id)) {
                found = carList;
                break;
            }
        }
        return found;
    }

    public void addCar(Car user) {
        cars.add(user);
    }

    public Car deleteCar(Long id) {
        Car car = getCar(id);
        if (car != null) {
            cars.remove(car);
            return car;
        }
        return null;
    }

    public void deleteAllCars() {
        cars.clear();
    }

}
