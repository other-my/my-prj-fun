package ru.home.myfirstdevops.dto;

public class Car {

    private Long id;
    private String manufacture;
    private String name;
    private String model;

    public Car() {
    }

    public Car(Long id, String manufacture, String name, String model) {
        this.id = id;
        this.manufacture = manufacture;
        this.name = name;
        this.model = model;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
