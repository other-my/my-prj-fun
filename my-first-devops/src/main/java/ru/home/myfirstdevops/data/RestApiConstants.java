package ru.home.myfirstdevops.data;

public final class RestApiConstants {

    private RestApiConstants() {
    }

    public static final String VERSION_1 = "v1";
    public static final String URL_DELIMITER = "/";
    public static final String API_VERSION_1 = "/api/" + VERSION_1 + URL_DELIMITER;

    public static final String PARAM_ID = "id";
    public static final String VARIABLE_ID = URL_DELIMITER + "{" + PARAM_ID + "}";

    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_SORT = "sort";
    public static final String PARAM_FILTERS = "filters";

    public static final int CODE_OK = 200;
    public static final int CODE_CREATED = 201;
    public static final int CODE_NOT_FOUND = 404;

    public static final String MESSAGE_OK = "OK";
    public static final String MESSAGE_CREATED = "CREATED";
    public static final String MESSAGE_NOT_FOUND = "NOT FOUND";
}
