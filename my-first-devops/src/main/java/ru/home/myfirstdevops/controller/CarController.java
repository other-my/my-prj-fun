package ru.home.myfirstdevops.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.home.myfirstdevops.data.RestApiConstants;
import ru.home.myfirstdevops.dto.Car;
import ru.home.myfirstdevops.service.CarService;

import java.util.List;

@RestController
@RequestMapping(CarController.API_URL)
@Slf4j
public class CarController {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "car";

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping(value = "/add")
    public ResponseEntity<Car> post(@RequestBody Car request) {
        carService.addCar(request);
        log.info("Car {} added in ArrayList", request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/get-all")
    public ResponseEntity<List<Car>> getAll() {
        List<Car> users = carService.getAll();
        log.info("List all users {}", users);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Car> get(
            @PathVariable(RestApiConstants.PARAM_ID) Long id) {
        Car user = carService.getCar(id);
        log.info("Get user {}", user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<Car> delete(
            @PathVariable(RestApiConstants.PARAM_ID) Long id) {
        Car user = carService.deleteCar(id);
        log.info("Delete user {}", user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
