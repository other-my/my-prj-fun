package ru.home.myfirstdevops.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.home.myfirstdevops.data.RestApiConstants;
import ru.home.myfirstdevops.dto.User;
import ru.home.myfirstdevops.service.UserService;

import java.util.List;

@RestController
@RequestMapping(UserController.API_URL)
@Slf4j
public class UserController {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "user";

    private final UserService userService;

    public UserController() {
        this.userService = new UserService();
    }

    @PostMapping(value = "/add")
    public ResponseEntity<User> post(@RequestBody User request) {
        userService.addUser(request);
        log.info("User {} added in ArrayList", request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/get-all")
    public ResponseEntity<List<User>> getAll() {
        List<User> users = userService.getAll();
        log.info("List all users {}", users);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<User> get(
            @PathVariable(RestApiConstants.PARAM_ID) Long id) {
        User user = userService.getUser(id);
        log.info("Get user {}", user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<User> delete(
            @PathVariable(RestApiConstants.PARAM_ID) Long id) {
        User user = userService.deleteUser(id);
        log.info("Delete user {}", user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
