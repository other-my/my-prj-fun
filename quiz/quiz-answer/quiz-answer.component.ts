import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {QuizQuestion} from '../../../../shared/data/shema/quiz/quizQuestion';
import {SelectionModel} from '@angular/cdk/collections';
import {Quiz} from '../../../../shared/data/shema/quiz/quiz';
import {QuizQuestionType} from '../../../../shared/data/shema/quiz/quizQuestionType';
import {MatPaginator} from '@angular/material/paginator';
import {QuizAnswer} from '../../../../shared/data/shema/quiz/QuizAnswer';
import {ListResponse} from '../../../../shared/data/shema/http/ListResponse';
import {StatusType} from '../../../../shared/data/enums/StatusType';
import {MatDialog} from '@angular/material/dialog';
import {QuizQuestionService} from '../../../../core/http/quiz/quiz-question.service';
import {QuizAnswerService} from '../../../../core/http/quiz/quiz-answer.service';
import {QuizQuestionEditDialogComponent} from '../../../dialogs/quiz/quiz-question-edit-dialog/quiz-question-edit-dialog.component';
import {QuizQuestionDto} from '../../../../shared/data/classes/quiz/QuizQuestionDto';
import {DialogAction} from '../../../../shared/data/enums/DialogAction';
import {ConfirmDialogComponent} from '../../../dialogs/confirm-dialog/confirm-dialog.component';
import {DataResponse} from '../../../../shared/data/shema/http/DataResponse';
import {QuizAnswerEditDialogComponent} from '../../../dialogs/quiz/quiz-answer-edit-dialog/quiz-answer-edit-dialog.component';
import {QuizAnswerDto} from '../../../../shared/data/classes/quiz/QuizAnswerDto';

@Component({
  selector: 'app-quiz-answer',
  templateUrl: './quiz-answer.component.html',
  styleUrls: ['./quiz-answer.component.scss']
})
export class QuizAnswerComponent implements OnInit {

  public dataSource: MatTableDataSource<QuizAnswer>;
  public displayedColumns: string[] = ['select', 'quizQuestionName', 'position', 'score', 'answer', 'action'];
  public selection = new SelectionModel<QuizAnswer>(true, []);

  quizQuestions!: QuizQuestion[];
  quizAnswer!: QuizAnswer[];

  public isShowFilterInput = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private quizAnswerService: QuizAnswerService,
    private quizQuestionService: QuizQuestionService,
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<QuizAnswer>(this.quizAnswer);
    this.dataSource.paginator = this.paginator;

    this.getAllQuizQuestion();

  }

  /**
   * Получим все вопросы
   */
  getAllQuizQuestion(){
    this.quizQuestionService
      .getAllQuizQuestion('tokenResult')
      .subscribe(
        (result: ListResponse<QuizQuestion>) => {
          console.log(' QuizQuestion data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.quizQuestions = [];
            this.quizQuestions.push(...result.listResult.list as QuizQuestion[]);
            this.getAllQuizAnswer();
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }

  /**
   * Получим все ответы
   */
  getAllQuizAnswer(){
    this.quizAnswerService
      .getAllQuizAnswer('tokenResult')
      .subscribe(
        (result: ListResponse<QuizAnswer>) => {
          console.log(' QuizQuestion data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.quizAnswer = [];
            this.quizAnswer.push(...result.listResult.list as QuizAnswer[]);
            this.addQuizQuestionNameInTable();

            // this.addTenantNamesInTable();
            this.dataSource = new MatTableDataSource<QuizAnswer>(this.quizAnswer);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }


  openAddDialog() {
    this.dialog.open(QuizAnswerEditDialogComponent, {
      // передаем новый пустой объект для заполнения
      data: [new QuizAnswerDto(
        '', '', 0, '', 0
      ),
        this.quizQuestions,
        'Создание нового ответа'
      ],
      width: '400px',
      disableClose: true
    })
      .afterClosed().subscribe(
      value => {
        if (value === DialogAction.SAVE) {
          this.getAllQuizAnswer();
        }
      });
  }

  openEditDialog(quizAnswerDto: QuizAnswerDto) {
    // const curQuizQuestion = new QuizDto(quizQuestionDto.id, quizDto.name, quizDto.description, quizDto.tenantId);
    const dialogRef = this.dialog.open(QuizAnswerEditDialogComponent, {
      // передаем новый пустой объект для заполнения
      data: [quizAnswerDto,
        this.quizQuestions,
        'Изменение ответа'
      ],
      width: '400px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(
      value => {
        if (value === DialogAction.UPDATE || value === DialogAction.DELETE) {
          console.log('DialogAction.UPDATE || DELETE');
          this.getAllQuizAnswer();
        }
      });
  }

  openDeleteDialog(id: string) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dialogTitle: 'Подтвердите действие',
        message: `Вы действительно хотите удалить ответ`
      },
      autoFocus: false,
      disableClose: true
    });

    dialogConfirm.afterClosed().subscribe(result => {
      if (result === DialogAction.OK) {
        // нажали удалить
        this.deleteQuizAnswer(id);
      }
    });
  }

  deleteQuizAnswer(id: string) {
    this.quizAnswerService.deleteQuizAnswer('token', id).subscribe(
      (result: DataResponse<QuizAnswer>) => {
        console.log('delete status = ' + result.status);
        if (result.status === StatusType.SUCCESSFUL) {
          alert('QuizQuestion delete successfully');
          this.getAllQuizAnswer();
        } else {
          alert('error while delete the QuizQuestion');
        }
      });
  }

  addQuizQuestionNameInTable() {
    if (this.quizAnswer === null) {
      console.log('quizQuestions === null ');
    }
    this.quizAnswer.forEach(value => {
      value.quizQuestionName = this.getQuizNameId(value.quizQuestionId);
    });
  }

  /**
   * Получим имя по id.
   * @param id - id
   */
  getQuizNameId(id: string): string {
    if (this.quizQuestions === null) {
      console.log('quizzes === null ');
    }

    let name = '-';
    for (const quiz of this.quizQuestions) {
      if (quiz.id === id) {
        name = quiz.question;
        break;
      }
    }
    return name;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  public checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public showFilterInput(): void {
    this.isShowFilterInput = !this.isShowFilterInput;
    this.dataSource = new MatTableDataSource<QuizAnswer>(this.quizAnswer);
  }

}
