import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {UserQuiz} from '../../../../shared/data/shema/quiz/UserQuiz';
import {SelectionModel} from '@angular/cdk/collections';
import {Quiz} from '../../../../shared/data/shema/quiz/quiz';
import {UserQuizAnswer} from '../../../../shared/data/shema/quiz/UserQuizAnswer';
import {MatDialog} from '@angular/material/dialog';
import {QuizService} from '../../../../core/http/quiz/quiz.service';
import {UserQuizService} from '../../../../core/http/quiz/user-quiz.service';
import {ListResponse} from '../../../../shared/data/shema/http/ListResponse';
import {StatusType} from '../../../../shared/data/enums/StatusType';
import {QuizQuestion} from '../../../../shared/data/shema/quiz/quizQuestion';
import {QuizAnswer} from '../../../../shared/data/shema/quiz/QuizAnswer';
import {QuizQuestionService} from '../../../../core/http/quiz/quiz-question.service';
import {QuizAnswerService} from '../../../../core/http/quiz/quiz-answer.service';
import {UserQuizAnswerService} from '../../../../core/http/quiz/user-quiz-answer.service';

@Component({
  selector: 'app-user-quiz-answer',
  templateUrl: './user-quiz-answer.component.html',
  styleUrls: ['./user-quiz-answer.component.scss']
})
export class UserQuizAnswerComponent implements OnInit {

  public dataSource: MatTableDataSource<UserQuizAnswer>;
  public displayedColumns: string[] = ['userId', 'quizName', 'status', 'score', 'created', 'updated'];
  public selection = new SelectionModel<UserQuizAnswer>(true, []);
  quizzes!: Quiz[];
  quizQuestions!: QuizQuestion[];
  quizAnswer!: QuizAnswer[];
  userQuizzes!: UserQuiz[];

  constructor(
    private dialog: MatDialog,
    private quizService: QuizService,
    private userQuizService: UserQuizService,
    private quizQuestionService: QuizQuestionService,
    private quizAnswerService: QuizAnswerService,
    private userQuizAnswerService: UserQuizAnswerService
  ) { }

  ngOnInit(): void {
  }

  /**
   * Получим все опросы
   */
  getAllQuiz(){
    this.quizService
      .getAllQuiz('tokenResult')
      .subscribe(
        (result: ListResponse<Quiz>) => {
          if (result.status === StatusType.SUCCESSFUL) {
            // console.log('result name = ' + result.listResult.list[0].manufacture.name);
            this.quizzes = [];
            this.quizzes.push(...result.listResult.list as Quiz[]);
          }
          console.log('UserQuizComponent->getAllQuiz данные получены');
          this.getAllUserQuiz();
        });
  }

  /**
   * Получим все quiz пользовтаеля
   */
  getAllUserQuiz(){
    this.userQuizService
      .getAllUserQuiz('tokenResult')
      .subscribe(
        (result: ListResponse<UserQuiz>) => {
          console.log(' UserQuizComponent data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.userQuizzes = [];
            this.userQuizzes.push(...result.listResult.list as UserQuiz[]);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }

  /**
   * Получим все вопросы
   */
  getAllQuizQuestion(){
    this.quizQuestionService
      .getAllQuizQuestion('tokenResult')
      .subscribe(
        (result: ListResponse<QuizQuestion>) => {
          console.log(' QuizQuestion data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.quizQuestions = [];
            this.quizQuestions.push(...result.listResult.list as QuizQuestion[]);
          }
          console.log('quizService данные получены');
        });
  }

  /**
   * Получим все ответы
   */
  getAllQuizAnswer(){
    this.quizAnswerService
      .getAllQuizAnswer('tokenResult')
      .subscribe(
        (result: ListResponse<QuizAnswer>) => {
          console.log(' QuizQuestion data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.quizAnswer = [];
            this.quizAnswer.push(...result.listResult.list as QuizAnswer[]);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }

  /**
   * Получим все ответы пользовтаеля
   */
  getAllUserAnswerQuiz(){
    this.userQuizAnswerService
      .getAllUserQuizAnswer('tokenResult')
      .subscribe(
        (result: ListResponse<UserQuiz>) => {
          console.log(' UserQuizComponent data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.userQuizzes = [];
            this.userQuizzes.push(...result.listResult.list as UserQuiz[]);
            this.addQuizNameInTable();

            // this.addTenantNamesInTable();
            this.dataSource = new MatTableDataSource<UserQuiz>(this.userQuizzes);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }


}
