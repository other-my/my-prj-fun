import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserQuizAnswerComponent } from './user-quiz-answer.component';

describe('UserQuizAnswerComponent', () => {
  let component: UserQuizAnswerComponent;
  let fixture: ComponentFixture<UserQuizAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserQuizAnswerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserQuizAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
