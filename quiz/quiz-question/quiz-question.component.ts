import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Quiz} from '../../../../shared/data/shema/quiz/quiz';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {QuizQuestion} from '../../../../shared/data/shema/quiz/quizQuestion';
import {MatDialog} from '@angular/material/dialog';
import {QuizService} from '../../../../core/http/quiz/quiz.service';
import {QuizQuestionService} from '../../../../core/http/quiz/quiz-question.service';
import {ListResponse} from '../../../../shared/data/shema/http/ListResponse';
import {StatusType} from '../../../../shared/data/enums/StatusType';
import {QuizQuestionTypeService} from '../../../../core/http/quiz/quiz-question-type.service';
import {QuizQuestionType} from '../../../../shared/data/shema/quiz/quizQuestionType';
import {DialogAction} from '../../../../shared/data/enums/DialogAction';
import {QuizQuestionDto} from '../../../../shared/data/classes/quiz/QuizQuestionDto';
import {QuizQuestionEditDialogComponent} from '../../../dialogs/quiz/quiz-question-edit-dialog/quiz-question-edit-dialog.component';
import {ConfirmDialogComponent} from '../../../dialogs/confirm-dialog/confirm-dialog.component';
import {DataResponse} from '../../../../shared/data/shema/http/DataResponse';

/**
 * вопрос квиза.
 */
@Component({
  selector: 'app-quiz-question',
  templateUrl: './quiz-question.component.html',
  styleUrls: ['./quiz-question.component.scss']
})
export class QuizQuestionComponent implements OnInit {

  public dataSource: MatTableDataSource<QuizQuestion>;
  public displayedColumns: string[] = ['select', 'quizName', 'serialNumber', 'quizQuestionTypeName', 'question', 'action'];
  public selection = new SelectionModel<QuizQuestion>(true, []);
  quizzes!: Quiz[];
  quizQuestionTypes!: QuizQuestionType[];
  quizQuestions!: QuizQuestion[];

  public isShowFilterInput = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private quizService: QuizService,
    private quizQuestionTypeService: QuizQuestionTypeService,
    private quizQuestionService: QuizQuestionService,
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<QuizQuestion>(this.quizQuestions);
    this.dataSource.paginator = this.paginator;

    this.getAllQuiz();

  }

  /**
   * Получим все опросы
   */
  getAllQuiz(){
    this.quizService
      .getAllQuiz('tokenResult')
      .subscribe(
        (result: ListResponse<Quiz>) => {
          if (result.status === StatusType.SUCCESSFUL) {
            // console.log('result name = ' + result.listResult.list[0].manufacture.name);
            this.quizzes = [];
            this.quizzes.push(...result.listResult.list as Quiz[]);
          }
          console.log('QuizQuestionComponent->getAllQuiz данные получены');
          this.getAllQuizQuestionType();
        });
  }

  /**
   * Получим все типы ответов
   */
  getAllQuizQuestionType(){
    this.quizQuestionTypeService.getAllQuizQuestionType('tokenResult')
      .subscribe(
        (result: ListResponse<QuizQuestionType>) => {
          if (result.status === StatusType.SUCCESSFUL) {
            this.quizQuestionTypes = [];
            this.quizQuestionTypes.push(...result.listResult.list as QuizQuestionType[]);
          }
          console.log('QuizQuestionComponent->getAllQuizQuestionType данные получены');
          this.getAllQuizQuestion();
        });

  }

  /**
   * Получим все вопросы
   */
  getAllQuizQuestion(){
    this.quizQuestionService
      .getAllQuizQuestion('tokenResult')
      .subscribe(
        (result: ListResponse<QuizQuestion>) => {
          console.log(' QuizQuestion data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.quizQuestions = [];
            this.quizQuestions.push(...result.listResult.list as QuizQuestion[]);
            this.addQuizNameInTable();

            // this.addTenantNamesInTable();
            this.dataSource = new MatTableDataSource<QuizQuestion>(this.quizQuestions);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }


  openAddDialog() {
    this.dialog.open(QuizQuestionEditDialogComponent, {
      // передаем новый пустой объект для заполнения
      data: [new QuizQuestionDto(
        '', '', 0, '', ''
      ),
        this.quizQuestionTypes,
        this.quizzes,
        'Создание нового вопроса'
      ],
      width: '400px',
      disableClose: true
    })
      .afterClosed().subscribe(
      value => {
        if (value === DialogAction.SAVE) {
          this.getAllQuizQuestion();
        }
      });
  }

  openEditDialog(quizQuestionDto: QuizQuestionDto) {
    // const curQuizQuestion = new QuizDto(quizQuestionDto.id, quizDto.name, quizDto.description, quizDto.tenantId);
    const dialogRef = this.dialog.open(QuizQuestionEditDialogComponent, {
      // передаем новый пустой объект для заполнения
      data: [quizQuestionDto,
        this.quizQuestionTypes,
        this.quizzes,
        'Создание нового вопроса'
      ],
      width: '400px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(
      value => {
        if (value === DialogAction.UPDATE || value === DialogAction.DELETE) {
          console.log('DialogAction.UPDATE || DELETE');
          this.getAllQuizQuestion();
        }
      });
  }

  openDeleteDialog(id: string) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dialogTitle: 'Подтвердите действие',
        message: `Вы действительно хотите удалить вопрос`
      },
      autoFocus: false,
      disableClose: true
    });

    dialogConfirm.afterClosed().subscribe(result => {
      if (result === DialogAction.OK) {
        // нажали удалить
        this.deleteQuizQuestion(id);
      }
    });
  }

  deleteQuizQuestion(id: string) {
    this.quizQuestionService.deleteQuizQuestion('token', id).subscribe(
      (result: DataResponse<QuizQuestion>) => {
        console.log('delete status = ' + result.status);
        if (result.status === StatusType.SUCCESSFUL) {
          alert('QuizQuestion delete successfully');
          this.getAllQuizQuestion();
        } else {
          alert('error while delete the QuizQuestion');
        }
      });
  }

  addQuizNameInTable() {
    console.log('addQuizNameInTable ');
    if (this.quizQuestions === null) {
      console.log('quizQuestions === null ');
    }
    this.quizQuestions.forEach(value => {
      value.quizName = this.getQuizNameId(value.quizId);
      value.quizQuestionTypeName = this.getQuizQuestionTypesNameId(value.quizQuestionTypeId);
    });
  }

  /**
   * Получим имя по id.
   * @param id - id
   */
  getQuizNameId(id: string): string {
    if (this.quizzes === null) {
      console.log('quizzes === null ');
    } else {
      console.log('quizzes length = ', this.quizzes.length);
    }
    let name = '-';
    for (const quiz of this.quizzes) {
      if (quiz.id === id) {
        name = quiz.name;
        break;
      }
    }
    return name;
  }

  /**
   * Получим имя по id.
   * @param id - id
   */
  getQuizQuestionTypesNameId(id: string): string {
    if (this.quizQuestionTypes === null) {
      console.log('quizQuestionTypes === null ');
    } else {
      console.log('quizQuestionTypes length = ', this.quizQuestionTypes.length);
    }
    let name = '-';
    for (const quizQuestionTypes of this.quizQuestionTypes) {
      if (quizQuestionTypes.id === id) {
        name = quizQuestionTypes.name;
        break;
      }
    }
    return name;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  public checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public showFilterInput(): void {
    this.isShowFilterInput = !this.isShowFilterInput;
    this.dataSource = new MatTableDataSource<QuizQuestion>(this.quizQuestions);
  }


}
