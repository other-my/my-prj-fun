import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Quiz} from '../../../../shared/data/shema/quiz/quiz';
import {UserQuiz} from '../../../../shared/data/shema/quiz/UserQuiz';
import {MatDialog} from '@angular/material/dialog';
import {QuizService} from '../../../../core/http/quiz/quiz.service';
import {UserQuizService} from '../../../../core/http/quiz/user-quiz.service';
import {ListResponse} from '../../../../shared/data/shema/http/ListResponse';
import {StatusType} from '../../../../shared/data/enums/StatusType';

@Component({
  selector: 'app-user-quiz',
  templateUrl: './user-quiz.component.html',
  styleUrls: ['./user-quiz.component.scss']
})
export class UserQuizComponent implements OnInit {

  public dataSource: MatTableDataSource<UserQuiz>;
  public displayedColumns: string[] = ['userId', 'quizName', 'status', 'score', 'created', 'updated'];
  public selection = new SelectionModel<UserQuiz>(true, []);
  quizzes!: Quiz[];
  userQuizzes!: UserQuiz[];

  constructor(
    private dialog: MatDialog,
    private quizService: QuizService,
    private userQuizService: UserQuizService
  ) { }

  ngOnInit(): void {
    this.getAllQuiz();
  }

  /**
   * Получим все опросы
   */
  getAllQuiz(){
    this.quizService
      .getAllQuiz('tokenResult')
      .subscribe(
        (result: ListResponse<Quiz>) => {
          if (result.status === StatusType.SUCCESSFUL) {
            // console.log('result name = ' + result.listResult.list[0].manufacture.name);
            this.quizzes = [];
            this.quizzes.push(...result.listResult.list as Quiz[]);
          }
          console.log('UserQuizComponent->getAllQuiz данные получены');
          this.getAllUserQuiz();
        });
  }

  /**
   * Получим все quiz пользовтаеля
   */
  getAllUserQuiz(){
    this.userQuizService
      .getAllUserQuiz('tokenResult')
      .subscribe(
        (result: ListResponse<UserQuiz>) => {
          console.log(' UserQuizComponent data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            this.userQuizzes = [];
            this.userQuizzes.push(...result.listResult.list as UserQuiz[]);
            this.addQuizNameInTable();

            // this.addTenantNamesInTable();
            this.dataSource = new MatTableDataSource<UserQuiz>(this.userQuizzes);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }

  addQuizNameInTable() {
    console.log('addQuizNameInTable ');
    if (this.userQuizzes === null) {
      console.log('quizQuestions === null ');
    }
    this.userQuizzes.forEach(value => {
      value.quizName = this.getQuizNameId(value.quizId);
    });
  }

  /**
   * Получим имя по id.
   * @param id - id
   */
  getQuizNameId(id: string): string {
    if (this.quizzes === null) {
      console.log('quizzes === null ');
    } else {
      console.log('quizzes length = ', this.quizzes.length);
    }
    let name = '-';
    for (const quiz of this.quizzes) {
      if (quiz.id === id) {
        name = quiz.name;
        break;
      }
    }
    return name;
  }

}
