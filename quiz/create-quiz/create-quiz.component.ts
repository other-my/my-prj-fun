import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Employee} from "../../../tables/models";
import {TablesService} from '../../../tables/services';

@Component({
  selector: 'app-create-quiz',
  templateUrl: './create-quiz.component.html',
  styleUrls: ['./create-quiz.component.scss']
})
export class CreateQuizComponent implements OnInit {

  public employeeTableData$: Observable<Employee[]>;

  constructor(private service: TablesService) {
    this.employeeTableData$ = service.loadEmployeeTableData();
  }

  ngOnInit(): void {
  }

}
