import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Employee} from '../../../tables/models';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {QuizService} from '../../../../core/http/quiz/quiz.service';
import {ListResponse} from '../../../../shared/data/shema/http/ListResponse';
import {Quiz} from '../../../../shared/data/shema/quiz/quiz';
import {StatusType} from '../../../../shared/data/enums/StatusType';
import {QuizEditDialogComponent} from '../../../dialogs/quiz/quiz-edit-dialog/quiz-edit-dialog.component';
import {QuizDto} from '../../../../shared/data/classes/quiz/QuizDto';
import {TenantService} from '../../../../core/http/tenant/tenant.service';
import {Tenant} from '../../../../shared/data/shema/tenant/tenant';
import {DialogAction} from '../../../../shared/data/enums/DialogAction';
import {ConfirmDialogComponent} from '../../../dialogs/confirm-dialog/confirm-dialog.component';
import {DataResponse} from '../../../../shared/data/shema/http/DataResponse';

/**
 *  Квиз, содержит вопросы и ответы
 */
@Component({
  selector: 'app-table-quiz',
  templateUrl: './table-quiz.component.html',
  styleUrls: ['./table-quiz.component.scss']
})
export class TableQuizComponent implements OnInit {
  @Input() employeeTableData: Employee[];
  public displayedColumns: string[] = ['select', 'name', 'company', 'city', 'state'];
  public dataSource: MatTableDataSource<Employee>;
  public selection = new SelectionModel<Employee>(true, []);

  public dataQuiz: MatTableDataSource<Quiz>;
  public displayedColumnsQuiz: string[] = ['select', 'name', 'description', 'tenantName', 'action'];
  public selectionQuiz = new SelectionModel<Quiz>(true, []);
  quizzes!: Quiz[];
  tenants!: Tenant[];

  public isShowFilterInput = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private quizService: QuizService,
    private tenantService: TenantService,
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<Employee>(this.employeeTableData);
    this.dataSource.paginator = this.paginator;

    this.getAllTenant();
    this.getAllQuiz();
    console.log('table .ngOnInit ');
  }

  /**
   * Получим все приложения
   */
  getAllTenant(){
    this.tenantService
      .getAllTenant('tokenResult')
      .subscribe(
        (result: ListResponse<Tenant>) => {
          console.log(' quizService data is received');
          console.log('status = ' + result.status);
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            console.log('result model = ' + result.listResult.list[0].description);
            // console.log('result name = ' + result.listResult.list[0].manufacture.name);
            this.tenants = [];
            this.tenants.push(...result.listResult.list as Quiz[]);
          }
          console.log('tenants count  = ' + this.tenants.length);
          console.log('tenants count  = ' + this.tenants[0].name);
          console.log('tenantService данные получены');
        });
  }

  /**
   * Получим все опросы
   */
  getAllQuiz(){
    this.quizService
      .getAllQuiz('tokenResult')
      .subscribe(
        (result: ListResponse<Quiz>) => {
          if (result.status === StatusType.SUCCESSFUL) {
            // console.log('result name = ' + result.listResult.list[0].manufacture.name);
            this.quizzes = [];
            this.quizzes.push(...result.listResult.list as Quiz[]);
            this.addTenantNamesInTable();
            this.dataQuiz = new MatTableDataSource<Quiz>(this.quizzes);
          }
        });
  }

  openAddDialog() {
    this.dialog.open(QuizEditDialogComponent, {
      // передаем новый пустой объект для заполнения
      data: [new QuizDto(
        '', '', '', ''
      ),
        this.tenants,
        'Создание нового опроса'
      ],
      width: '400px',
      disableClose: true
    })
      .afterClosed().subscribe(
        value => {
          if (value === DialogAction.SAVE) {
            this.getAllQuiz();
          }
        });
  }

  openEditDialog(quizDto: QuizDto) {
    const curQuiz = new QuizDto(quizDto.id, quizDto.name, quizDto.description, quizDto.tenantId);
    const dialogRef = this.dialog.open(QuizEditDialogComponent, {
      // передаем новый пустой объект для заполнения
      data: [curQuiz,
        this.tenants,
        'Изменение опроса'
      ],
      width: '400px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(
      value => {
        if (value === DialogAction.UPDATE || value === DialogAction.DELETE) {
          console.log('DialogAction.UPDATE || DELETE');
          this.getAllQuiz();
        }
      });
  }

  openDeleteDialog(id: string){
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dialogTitle: 'Подтвердите действие',
        message: `Вы действительно хотите удалить опрос`
      },
      autoFocus: false,
      disableClose: true
    });

    dialogConfirm.afterClosed().subscribe(result => {
      if (result === DialogAction.OK) {
        // нажали удалить
        this.deleteQuiz(id);
      }
    });
  }

  deleteQuiz(id: string) {
    this.quizService.deleteQuiz('token', id).subscribe(
      (result: DataResponse<Quiz>) => {
        console.log('delete status = ' + result.status);
        if (result.status === StatusType.SUCCESSFUL) {
          alert('Quiz delete successfully');
          this.getAllQuiz();
        } else {
          alert('error while delete the product');
        }
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  public checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public showFilterInput(): void {
    this.isShowFilterInput = !this.isShowFilterInput;
    this.dataSource = new MatTableDataSource<Employee>(this.employeeTableData);
  }

  addTenantNamesInTable() {
    this.quizzes.forEach(value => {
      value.tenantName = this.getNameById(value.tenantId);
    });
  }

  /**
   * Получим имя по id.
   * @param id - id
   */
  getNameById(id: string): string {
    let name = '';
    this.tenants.forEach(value => {
      if (value.id === id) {
        return name = value.name;
      }
    });
    return name;
  }

  addTenantInTable() {
    this.quizzes.forEach(value => {
      value.tenantName = this.getNameById(value.tenantId);
    });
  }

  /**
   * Получим имя по id.
   * @param id - id
   */
  getNameId(id: string): string {
    let name = '-';
    for (const tenant of this.tenants) {
      if (tenant.id === id) {
        name = tenant.name;
        break;
      }
    }
    return name;
  }



}
