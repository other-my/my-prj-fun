import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {QuizQuestionType} from '../../../../shared/data/shema/quiz/quizQuestionType';
import {MatDialog} from '@angular/material/dialog';
import {QuizQuestionTypeService} from '../../../../core/http/quiz/quiz-question-type.service';
import {ListResponse} from '../../../../shared/data/shema/http/ListResponse';
import {StatusType} from '../../../../shared/data/enums/StatusType';

/**
 * Типы вариантов от ответа
 */
@Component({
  selector: 'app-quiz-question-type',
  templateUrl: './quiz-question-type.component.html',
  styleUrls: ['./quiz-question-type.component.scss']
})
export class QuizQuestionTypeComponent implements OnInit {

  public dataSource: MatTableDataSource<QuizQuestionType>;
  public displayedColumns: string[] = ['name', 'description', 'type'];
  public selectionModel = new SelectionModel<QuizQuestionType>(true, []);
  quizQuestionTypes!: QuizQuestionType[];

  constructor(
    private dialog: MatDialog,
    private quizQuestionTypeService: QuizQuestionTypeService,
  ) { }

  ngOnInit(): void {
    this.getAllQuizQuestionType();
  }

  /**
   * Получим все типы ответов
   */
  getAllQuizQuestionType(){
    this.quizQuestionTypeService.getAllQuizQuestionType('tokenResult')
      .subscribe(
        (result: ListResponse<QuizQuestionType>) => {
          console.log(' quizService data is received');
          console.log('result = ', result);
          if (result.status === StatusType.SUCCESSFUL) {
            console.log('StatusType.SUCCESSFUL');
            console.log('result model = ' + result.listResult.list[0].description);
            // console.log('result name = ' + result.listResult.list[0].manufacture.name);
            this.quizQuestionTypes = [];
            this.quizQuestionTypes.push(...result.listResult.list as QuizQuestionType[]);
            this.dataSource = new MatTableDataSource<QuizQuestionType>(this.quizQuestionTypes);
          }
          // console.log('quizService manufacture.name = ' + this.machines[0].manufacture.name);
          console.log('quizService данные получены');
        });
  }

}
