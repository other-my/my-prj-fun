import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizQuestionTypeComponent } from './quiz-question-type.component';

describe('QuizQuestionTypeComponent', () => {
  let component: QuizQuestionTypeComponent;
  let fixture: ComponentFixture<QuizQuestionTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizQuestionTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizQuestionTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
