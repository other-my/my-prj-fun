Чтобы прогрузить все пакеты в готовом проетк надо выполнить
`npm install`

## material
Для него понадобятся слдедующие пакеты
`@angular/animations` and `@angular/platform-browser` проверить, обычно они ставятся по умолчанию
Обязательно установить
```
ng add @angular/cdk
```



Последняя версия
```bash
ng add @angular/material
```

произвольная
```
npm i @angular/material@11.2.13
npm install @angular/material@11.2.13

```

## cdk

```
ng add @angular/cdk
```

npm i @angular/cdk@11.2.13

npm i @angular/core@13.1.3

## cookie
Последняя
npm i ngx-cookie-service --save

для 12 Angular
npm i ngx-cookie-service@12.0.3 --save
для 11 Angular
npm i ngx-cookie-service@11.0.2 --save
Удалении последней
npm uninstall ngx-cookie-service --save

ng update ngx-cookie-service@12.0.3 --allow-dirty --force

npm uninstall ngx-cookie-service@11.0.2 --save

## bootstrap
старая версия не работает
npm install ngx-bootstrap --save

Получилось вот так:
npm install bootstrap

В angular.json:
"options": {
"styles": [
"node_modules/bootstrap/dist/css/bootstrap.min.css"
],
"scripts": [
"node_modules/jquery/dist/jquery.min.js",
"node_modules/bootstrap/dist/js/bootstrap.min.js"
]
}
обязательно в файле style.css добавить
@import '~bootstrap/dist/css/bootstrap.min.css';
без этого не работает

## Библиотеки для шаблонов
###  @coreui/angular
https://coreui.io/angular/docs/4.0/icons
coreui/angular](https://coreui.io/angular)
https://www.npmjs.com/package/@coreui/angular

CoreUI Installation

`npm install @coreui/angular --save`
### CSS

Copy-paste the stylesheet `<link>` into your `<head>` before all other stylesheets to load our CSS. В файл style.scc:
<link rel="stylesheet" href="node_modules/@coreui/coreui/dist/css/coreui.min.css">
В демо проекте, сделали так, В файл style.scc:
`@import "~@coreui/coreui/scss/coreui";`

С ней проблема требует определенные подверсии angular/common, надо эксперементировать
npm i @coreui/angular@4.0.0-alpha.3


npm uninstall  @coreui/angular@4.0.0-alpha.3 --save
npm uninstall @coreui/icons --save

### @coreui/icons-angular
### Installation
https://www.npmjs.com/package/@coreui/icons-angular
npm install @coreui/icons
npm install @coreui/icons-angular

npm uninstall @coreui/icons-angular --save

## @angular/common
Требует определенную версию
npm i @angular/common@13.1.3

## Angular CLI
`npm i @angular/cli`
Если возникает ошибка
Required package '@angular/cli' is not installed
### 1. Uninstall and clean (global)
```
rm -rf node_modules dist
npm uninstall -g @angular/cli
npm cache clean
```
### 2. Reinstall and play (global)
```
npm install -g @angular/cli@latest
npm install
ng serve
```



Класс  FaIconComponent
<fa-icon class="ml-auto" *ngIf="sideNavItem.submenu" [icon]="['fas', 'angle-down']"></fa-icon>

1.  `npm install font-awesome --save`
    или
3.  npm i angular-font-awesome
    или
    вот этот компонент
    npm i @fortawesome/angular-fontawesome
    необходим импорт
    import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'


## PrimeNG
npm install primeng --save
npm install primeicons --save
npm install primeflex

prismjs - врое пока не нужен
