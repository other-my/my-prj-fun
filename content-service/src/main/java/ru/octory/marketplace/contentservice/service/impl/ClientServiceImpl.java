package ru.octory.marketplace.contentservice.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.octory.marketplace.common.dto.user.UserInfoDto;
import ru.octory.marketplace.contentservice.config.AuthenticationServiceConfig;
import ru.octory.marketplace.contentservice.service.ClientService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    private final RestTemplate restTemplate;
    private final String authenticationUrl;

    public ClientServiceImpl(RestTemplate restTemplate, AuthenticationServiceConfig authCfg) {
        this.restTemplate = restTemplate;
        this.authenticationUrl = String.format("%s://%s:%s%s", authCfg.getSchema(),
                authCfg.getHost(),
                authCfg.getPort(),
                authCfg.getPath());
    }

    @Override
    public List<UserInfoDto> findOrganizationsByIds(List<String> organizationIds){
        var ids = organizationIds.stream().map(Object::toString)
                .collect(Collectors.joining(","));
        var responseDto = new ArrayList<UserInfoDto>();
        try {
            log.info("findOrganizationsByIds -> authenticationUrl: {}, ids = {}", authenticationUrl, ids);
            var response = restTemplate.exchange(authenticationUrl + "/organizations?ids={ids}",
                    HttpMethod.GET, null, new ParameterizedTypeReference<ArrayList<UserInfoDto>>() {}, ids);
            responseDto = response.getBody();
        }catch (Exception e){
            log.error("addOrganizationToPricelists(size: {}) -> {}", organizationIds.size(), e.getMessage());
        }
        log.info("addOrganizationToPricelists(size: {}) -> size: {}", organizationIds.size(), responseDto.size());
        return responseDto;
    }
}
