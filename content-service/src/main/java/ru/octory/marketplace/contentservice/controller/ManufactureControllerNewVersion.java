package ru.octory.marketplace.contentservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.octory.marketplace.common.data.RestApiConstants;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.converter.Converter;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.service.ManufactureService;

/**
 * Контроллер обработки всех запрсов касающихся производителй
 * @author alex
 */
//@RestController
//@RequestMapping(ManufactureControllerNewVersion.API_URL)
//@AllArgsConstructor
//@Tag(name = "Content API", description = "Контроллер обработки вспех запрсов касающихся машин")
public class ManufactureControllerNewVersion
        extends AbstractCrudRestContentController<Long, ManufactureEntity, ManufactureDto,
        ManufactureService,
        Converter<ManufactureEntity, ManufactureDto>, Converter<ManufactureDto, ManufactureEntity>>{

    // LATER: После того как будут проверены все функции вернуться к этой реализации и
    // попробовать ее подогнать под текущую

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "manufacture";

    @Autowired
    public ManufactureControllerNewVersion(
            ManufactureService manufactureService,
            Converter<ManufactureEntity, ManufactureDto> entityToDtoConverter,
            Converter<ManufactureDto, ManufactureEntity> dtoToEntityConverter) {
        super(manufactureService, entityToDtoConverter, dtoToEntityConverter);
    }


    @Operation(summary = "Добавить производителя", description = "Добавление в БД производителя " +
            "машины или детали")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель добавлен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PostMapping("/add")
    public ResponseEntity<DataResponse<ManufactureDto>> addManufacture(@RequestBody DataRequest<ManufactureDto> request) {
//        var requestId = generateRequestId();
//        contentService.addInventories(relationships, requestId);
//        return requestId;
        return postEntity(request);
    }

}
