package currencymanager.service.impl;

import com.asvoip.ump.currencymanager.service.ExchangeRateService;
import com.asvoip.ump.currencymanager.service.SchedulerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@EnableScheduling
@Service
@AllArgsConstructor
@Slf4j
public class SchedulerServiceImpl implements SchedulerService {

    private final ExchangeRateService createExchangeRates;

    @Scheduled(cron = "${scheduler.cron}", zone = "${scheduler.zone}")
    @Override
    public void launchCreateExchangeRates() {
        log.info("Starting up: launchCreateExchangeRates");
        createExchangeRates.createExchangeRates();
    }
}

/*
#Scheduled

На класс вешаем аннотацию
@EnableScheduling
А на метод аннотацию
@Scheduled

#### Cron
#Cron
Выражения cron имеют вид ‘ * * * * * ? ‘ (minutes, hours, day of month, month, day of week, year(optional) ).

* — выбирает все величины. То есть на месте позиции часа символ * означает, что задание будет выполняться каждый час;

? — незначащая величина;

, — отделяет дополнительные величины. Например, триггер “0 0 11,12 * * ?” будет срабатывать в 11 и 12 часов;

/ — определяет инкремент величины. Например, “0 0 0/2 * * ?” означает, что триггер будет срабатывать каждые 2 часа.
Пример
@Scheduled(cron = "0/30 * * * * ?")
метод будет выполнятся каждые 30 секунд

#### fixedDelay
#fixedDelay
@Scheduled(fixedDelay = 10000)
Задача будет выполняться через 10 сек после запуска, каждые 10 сек.
`fixedDelay` означает фиксированный интервал между концом предыдущего задания и началом нового задания.
**Новое задание всегда будет ждать завершения предыдущего задания**. Его следует использовать в ситуациях, когда вызовы методов должны происходить последовательно.

#### fixedRate
#fixedRate
Эта указывает интервал для выполнения задания через фиксированный интервал времени. Его следует использовать в ситуациях, когда вызовы методов независимы. **Время выполнения метода не учитывается при решении, когда начинать следующее задание**.
@Scheduled(fixedRate = 3000)
будет запускать метод каждые 3 сек
Аннотация `@Async` к методу позволяет выполнить его в отдельном потоке. В результате, когда предыдущее выполнение метода занимает больше времени, чем интервал с фиксированной скоростью, последующий вызов метода сработает, даже если предыдущий вызов все еще выполняется.
Это позволит выполнять несколько запусков метода параллельно в течение перекрывающегося временного интервала.
Без применения аннотации `@Async` метод всегда будет выполняться после завершения предыдущего выполнения, даже если интервал с фиксированной скоростью истек.

#### initialDelay
#initialDelay
мы можем отложить первое выполнение метода, указав интервал с помощью атрибута `initialDelay`,
@Scheduled(initialDelay = 2000, fixedRate = 30000)
Здесь мы установили задержку первого выполнения метода `initialDelay` в  `2` секунды, следующий запуск произойдет через 300сек

#### Вынесение интервала в файл application.properties
scheduler.cron: "0 0 9 * * *"
scheduler.zone: "Europe/Moscow"
interval: 2000

@Scheduled(fixedDelayString = "${interval}")
@Scheduled(cron = "${scheduler.cron}", zone = "${scheduler.zone}")

@Scheduled(
      initialDelayString = "${email.сonsumer.init-delay}",
      fixedRateString = "${email.сonsumer.fixed-rate}"
  )


 */
