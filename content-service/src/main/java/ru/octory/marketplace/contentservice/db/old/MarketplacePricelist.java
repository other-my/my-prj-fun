package ru.octory.marketplace.contentservice.db.old;

import lombok.*;
import ru.octory.marketplace.contentservice.db.old.PriceList;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "marketplace_pricelist_relationship", schema = "inventories")
public class MarketplacePricelist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "id_marketplace")
    private String idMarketplace;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pricelist", nullable = false)
    private PriceList priceList;
}
