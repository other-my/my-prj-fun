package ru.octory.marketplace.contentservice.dao;

import ru.octory.marketplace.common.service.CrudService;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

//public interface ManufactureDao {
public interface ManufactureDao extends CrudService<ManufactureEntity, Long> {
    //Здесь будут добавлены специфичные методы для данной таблицы

//    void deleteForId(String id);
//
//    void deleteByName(String name);
//
    // получение объекта по имени
//    ManufactureEntity getByName(String name);

}
