package ru.octory.marketplace.contentservice.converter;

import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.ComponentPriceDto;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.contentservice.entity.ComponentPriceEntity;

@Component
public class ComponentPriceDtoToEntityConverter extends AbstractIdentifiableConverter<Long,
        ComponentPriceDto, ComponentPriceEntity> {

    @Override
    public ComponentPriceEntity convert(ComponentPriceDto input) {
        final ComponentPriceEntity output = super.convert(input);
        output.setPrice(input.getPrice());
        output.setDateUpdate(input.getDateUpdate());
        return output;
    }

    @Override
    protected ComponentPriceEntity createOutput() {
        return new ComponentPriceEntity();
    }

}
