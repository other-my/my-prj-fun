package ru.octory.marketplace.contentservice.service;

import ru.octory.marketplace.common.dto.data.ListResult;
import ru.octory.marketplace.contentservice.dao.ManufactureDao;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import java.util.List;
import java.util.Optional;

public interface ManufactureService extends ManufactureDao {

    Optional<ManufactureEntity> getByName(String name);

    ListResult<ManufactureEntity> getAll();

}
