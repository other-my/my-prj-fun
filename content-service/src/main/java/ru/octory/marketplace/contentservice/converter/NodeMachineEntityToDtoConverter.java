package ru.octory.marketplace.contentservice.converter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.content.NodeMachineDto;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.contentservice.entity.MachineEntity;
import ru.octory.marketplace.contentservice.entity.NodeMachineEntity;

@AllArgsConstructor
@Component
public class NodeMachineEntityToDtoConverter extends AbstractIdentifiableConverter<Long,
        NodeMachineEntity, NodeMachineDto> {

    @Override
    public NodeMachineDto convert(NodeMachineEntity input) {
        final NodeMachineDto output = super.convert(input);




        return output;
    }

    @Override
    protected NodeMachineDto createOutput() {
        return new NodeMachineDto();
    }

}
