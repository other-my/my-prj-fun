package ru.octory.marketplace.contentservice.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.common.dto.data.ListResponse;

public interface MachineConvertService {

    ResponseEntity<DataResponse<MachineDto>> postEntity(
            @RequestBody DataRequest<MachineDto> request);

    ResponseEntity<DataResponse<MachineDto>> putEntity(DataRequest<MachineDto> request);

    ResponseEntity<DataResponse<MachineDto>> getEntity(Long id);

    ResponseEntity<DataResponse<MachineDto>> getEntity(String name);

    ResponseEntity<ListResponse<MachineDto>> getEntity();

    ResponseEntity<DataResponse<MachineDto>> deleteEntity(Long id);

}
