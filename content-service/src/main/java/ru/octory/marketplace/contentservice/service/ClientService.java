package ru.octory.marketplace.contentservice.service;


import ru.octory.marketplace.common.dto.user.UserInfoDto;

import java.util.List;

public interface ClientService {
    List<UserInfoDto> findOrganizationsByIds(List<String> ids);
}
