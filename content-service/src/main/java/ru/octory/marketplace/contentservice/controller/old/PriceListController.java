package ru.octory.marketplace.contentservice.controller.old;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.octory.marketplace.common.dto.inventory.PricelistDto;
import ru.octory.marketplace.common.enums.universal.Result;
import ru.octory.marketplace.contentservice.service.ClientService;
import ru.octory.marketplace.contentservice.service.PriceListService;

import java.util.List;

/**
 * Контроллер для получение данных относящихся к прайс-листам
 *
 * @author Afanasev Evgeny
 * @version 0.1-MVP
 */
@RestController
@RequestMapping(value = "/pricelist")
@RequiredArgsConstructor
@Tag(name = "PriceList API", description = "Методы для взаимодействия с предложениями от магазинов")
public class PriceListController {

//    private final PriceListService priceListService;
//    private final ClientService clientService;
/*

    @Operation(summary = "Получение pricelist", description = "Получение pricelist по id inventory")
    @GetMapping("/inventory/{id}")
    public List<PricelistDto> getPricelistsByInventory(@PathVariable Long id) {
//        return priceListService.findPricelistsByInventory(id);
        return null;
    }
    @Operation(summary = "Получение pricelist", description = "Получение pricelist по id marketplace")
    @GetMapping("/marketplace/{id}")
    public List<PricelistDto> getPricelistsByMarketplace(@PathVariable Long id) {
//        return priceListService.findPricelistsByInventory(id);
        return null;
    }
    @Operation(summary = "Добавление нового pricelist", description = "Добавление pricelist для marketplace")
    @PostMapping("/")
    public Result addPricelist(@RequestBody PricelistDto dto){
//        return priceListService.addPricelist(dto);
        return null;
    }

 */
}
