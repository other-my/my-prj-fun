package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import java.util.Optional;

@Repository
public interface ManufactureRepositoryOld extends JpaRepository<ManufactureEntity, Integer> {
    Optional<ManufactureEntity> findByName(String manufacture);

}
