package ru.octory.marketplace.contentservice.service;

import org.springframework.web.multipart.MultipartFile;
import ru.octory.marketplace.common.dto.inventory.*;
import ru.octory.marketplace.common.enums.universal.Result;

import java.util.List;

public interface ContentService {

    /**
     * Поиск всех деталей, у которых есть совпадение по part_num, part_num_oem, part_name
     *
     * @return список DTO деталей
     */
//    P<List<RelationshipDto>> findInventoriesByQuery(String query, Integer offset, Integer limit);

    /**
     * Получение всех производителей, которые есть в БД
     *
     * @return DTO производителей
     */
//    List<ManufactureDtoOld> findManufactures();

    /**
     * Получение всех производителей, которые есть в БД
     *
     * @return DTO производителей
     */
//    List<ManufactureDtoOld> findAllManufactures();

    /**
     * Получение всех машин производителя
     *
     * @param id - ID производителя
     * @return список DTO машин
     */
//    List<InventoryDto> findMachines(Integer id);

    /**
     * Получение всех деталей у которых входная деталь является родительской деталью
     *
     * @param id - ID детали
     * @return список DTO деталей с количеством необходимым для родительской детали и номером на картинке
     */
//    List<RelationshipDto> findChildren(Long id);

    /**
     * Получение детали в состав которой входит входная деталь
     *
     * @param id - ID детали
     * @return DTO детали с необходимым количеством входной детали и номером на картинке
     */
//    List<RelationshipDto> findParent(Long id);
    /**
     * Добавление/изменение списка деталей
     *
     * @param inventories - деталь, которая добавляется/изменяется
     * @return результат выполнения операции
     */
//    Result addInventories(List<RelationshipDto> inventories, String requestId);
    /**
     * Добавление детали
     *
     * @param relationshipDto - деталь, которая добавляется/изменяется
     * @return результат выполнения операции
     */
//    Result addInventory(RelationshipDto relationshipDto, String requestId);
    /**
     * Получение всех точек на схеме с дочерними деталями
     *
     * @param id - id детали,
     * @return Список точек с координатами
     */
//    List<PointDto> getInventoryPoints(Long id);
    /**
     * Получение всех точек на схеме с дочерними деталями
     *
     * @param id - id детали,
     * @return Список точек с координатами
     */
    /*
    Result saveSchema(MultipartFile file, Long id);

    InventoryDto findInventoryById(Long id);

    P<List<RelationshipDto>> findInventoriesByPartNumbers(List<String> partNumbers, Integer offset, Integer limit);

    P<List<RelationshipDto>> getLastAddedInventories(int offset, int limit);

    P<List<RelationshipDto>> getLastAddedMachine(String requestId, int offset, int limit);

     */
}
