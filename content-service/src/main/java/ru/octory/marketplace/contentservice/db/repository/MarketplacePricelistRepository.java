package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.db.old.MarketplacePricelist;

import java.util.List;

@Repository
public interface MarketplacePricelistRepository extends JpaRepository<MarketplacePricelist, Long> {
    List<MarketplacePricelist>findByPriceList_idInventory(Long idInventory);
}
