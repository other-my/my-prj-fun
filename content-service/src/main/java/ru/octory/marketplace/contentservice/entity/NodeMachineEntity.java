package ru.octory.marketplace.contentservice.entity;

import lombok.*;
import ru.octory.marketplace.contentservice.data.AbstractIdEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Моя версия БД.
 * Класс узлов машины.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "node_machine", schema = "catalog")
public class NodeMachineEntity extends AbstractIdEntity<Long> {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    /* Если это OEM, то поле code_analog будет пустое,
    а если это аналог, то в поле code_analog будет записан code_oem.
    OEM это или аналог определяет флаг oemOrAnalog.
     */
    // код оригинального узла
    @Column(name = "code_oem", length = 17, nullable = false, unique = true)
    private String codeOEM;

    // код аналога данного узла
    @Column(name = "code_analog", length = 17, nullable = false)
    private String codeAnalog;

    // Название узла
    @Column(name = "name", length = 255, nullable = false)
    private String name;

    // Версия
    @Column(name = "version", length = 11, nullable = false)
    private String version;

    /* Если флаг = true, то это OEM, иначе аналог. */
    @Column(name = "oem_or_analog", nullable = false)
    private Boolean oemOrAnalog;

    // Имя файла с изображением
    @Column(name = "image_file_name", nullable = false)
    private String imageFileName;

    // Производитель
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "manufacture_id")
    private ManufactureEntity manufactureEntity;

    // Стоимость узла
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "price_id")
    private NodePriceEntity nodePriceEntity;

    // Список машин, на которых может быть установлен данный узел
    @ManyToMany(mappedBy="nodeMachineEntities")
    private List<MachineEntity> machineEntities = new ArrayList<>();

    // Список деталей узла
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "link_component_node",
            joinColumns = @JoinColumn(name = "node_machine_id"),
            inverseJoinColumns = @JoinColumn(name = "component_node_id"))
    private List<ComponentNodeEntity> componentNodes = new ArrayList<>();

}
