package ru.octory.marketplace.contentservice.db.old;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "points", schema = "inventories")
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "pivot_x", nullable = false)
    private Integer pivotX;
    @Column(name = "pivot_y", nullable = false)
    private Integer pivotY;
    @OneToOne
    @JoinColumn(name = "relationship_id", nullable = false)
    private InventoryRelationship relationship;

    @Override
    public String toString() {
        return "Point{" +
                "id=" + id +
                ", pivotX=" + pivotX +
                ", pivotY=" + pivotY +
                '}';
    }
}
