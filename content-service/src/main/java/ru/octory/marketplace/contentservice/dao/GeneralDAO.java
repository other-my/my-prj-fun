package ru.octory.marketplace.contentservice.dao;

import ru.octory.marketplace.common.dto.data.Identifiable;
import ru.octory.marketplace.common.dto.data.ListAttributes;
import ru.octory.marketplace.common.dto.data.ListResult;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Интерфейс описывает общее поведение объектов
 */
public interface GeneralDAO<I> {

    // получение всех записей (без постраничности)
    // List<T> getAll();
    // поиск записей с любым количествомм параметров
    //List<T> search(String... searchString);
    // получение объекта по id
    // T get(Long id);


    // save - обновляет или добавляет объект (один метод на 2 действия)
    //T save(T obj);// save - обновляет или добавляет объект (один метод на 2 действия)
    //T update(T obj);// тоже самое save - обновляет или добавляет объект (один метод на 2 действия)
    // удаление объекта
    // void delete(T object);


    // новая реализация

//    Optional<T> get(I id);
//
//    ListResult<T> find(ListAttributes listAttributes);
//
//    void create(T object);
//
//    T create(I id, T object);
//
//    boolean createOrUpdate(T object);
//
//    void update(T object);
//
//    default boolean update(T object, Map<String, Object> data) {
//        throw new UnsupportedOperationException("Method not implemented");
//    }
//
//    boolean delete(I id);
}
