package ru.octory.marketplace.contentservice.converter;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.content.NodeMachineDto;
import ru.octory.marketplace.common.dto.converter.Converter;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.common.dto.converter.impl.ListConverter;
import ru.octory.marketplace.contentservice.entity.MachineEntity;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.entity.NodeMachineEntity;

//@AllArgsConstructor
@Component
public class MachineEntityToDtoConverter extends AbstractIdentifiableConverter<Long,
        MachineEntity, MachineDto> {

    private final Converter<ManufactureEntity, ManufactureDto> manufactureEntityToDtoConverter;
    private final ListConverter<NodeMachineEntity, NodeMachineDto> nodeMachineEntityListConverter;

    @Autowired
    public MachineEntityToDtoConverter(
            ManufactureEntityToDtoConverter manufactureConverter,
            NodeMachineEntityToDtoConverter nodeMachineConverter) {
        super();
        this.manufactureEntityToDtoConverter = manufactureConverter;
        nodeMachineEntityListConverter = new ListConverter<>(nodeMachineConverter);
    }


    @Override
    public MachineDto convert(MachineEntity input) {
        final MachineDto output = super.convert(input);

        output.setCode(input.getCode());
        output.setModel(input.getModel());
        output.setYear(input.getYear());
        output.setImageFileName(input.getImageFileName());
        output.setCode(input.getCode());
        output.setCode(input.getCode());
        output.setManufactureDto(manufactureEntityToDtoConverter.convert(
                input.getManufactureEntity()));
        output.setNodeMachinesDto(nodeMachineEntityListConverter.convert(
                input.getNodeMachineEntities()));
        return output;
    }

    @Override
    protected MachineDto createOutput() {
        return new MachineDto();
    }

}
