package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.db.old.InventoryRelationship;

import java.util.List;

@Repository
public interface InventoryRelationshipRepository extends JpaRepository<InventoryRelationship, Long> {

    List<InventoryRelationship> findByIdParent(Long idParent);

    List<InventoryRelationship> findByIdChild(Long idChild);

    @Query(value = "SELECT COALESCE(ir.id, 0) AS id, ir.id_parent, COALESCE(ir.id_child, i.id) AS id_child, ir.quantity , ir.point_id FROM inventories.inventory_relationship ir " +
            "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id",
            countQuery = "SELECT COUNT(*) FROM inventories.inventory_relationship ir " +
                    "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id",
            nativeQuery = true)
    Page<InventoryRelationship> findAllRightJoinInventory(Pageable pageable);

    @Query(value = "SELECT ir.id, ir.id_parent, COALESCE(ir.id_child, i.id) AS id_child, ir.quantity , ir.point_id FROM inventories.inventory_relationship ir " +
            "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id where i.part_num_oem in ?1 OR i.part_num in ?1",
            countQuery = "SELECT COUNT(*) FROM inventories.inventory_relationship ir " +
                    "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id where i.part_num_oem in ?1 OR i.part_num in ?1",
            nativeQuery = true)
    Page<InventoryRelationship> findRightJoinInventoryByPartNumInOrPartNumOEMIn(List<String> partNum, Pageable page);

    @Query(value = "SELECT ir.id, ir.id_parent, COALESCE(ir.id_child, i.id) AS id_child, ir.quantity , ir.point_id FROM inventories.inventory_relationship ir " +
            "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id where i.part_num_oem = ?1 OR i.part_num = ?1 OR i.part_name = ?1",
            countQuery = "SELECT COUNT(*) FROM inventories.inventory_relationship ir " +
                    "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id where i.part_num_oem = ?1 OR i.part_num = ?1 OR i.part_name = ?1",
            nativeQuery = true)
    Page<InventoryRelationship> findRightJoinByPartNumContainingOrPartNumOEMContainingOrPartNameContaining(String query, Pageable page);

    @Query(value = "SELECT ir.id, ir.id_parent, COALESCE(ir.id_child, i.id) AS id_child, ir.quantity , ir.point_id FROM inventories.inventory_relationship ir " +
            "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id where i.request_id = ?1",
            countQuery = "SELECT COUNT(*) FROM inventories.inventory_relationship ir " +
                    "RIGHT JOIN inventories.inventories i ON ir.id_child = i.id where i.request_id = ?1",
            nativeQuery = true)
    Page<InventoryRelationship> findByRequestId(String requestId, Pageable page);

    boolean existsByIdParentAndIdChild(Long idParent, Long idChild);

}
