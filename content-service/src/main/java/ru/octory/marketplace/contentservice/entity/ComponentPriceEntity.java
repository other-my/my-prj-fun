package ru.octory.marketplace.contentservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.octory.marketplace.contentservice.data.AbstractIdEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Моя версия БД.
 * Класс стоимость детали.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "component_price", schema = "catalog")
public class ComponentPriceEntity extends AbstractIdEntity<Long> {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    // Цена
    @Column(name = "price", nullable = false)
    private Double price;
    // Дата обновления цены
    @Column(name = "date_update", nullable = false)
    private Date dateUpdate;
}
