package ru.octory.marketplace.contentservice.converter;

import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.common.dto.converter.impl.AbstractNamedIdentifiableConverter;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

@Component
public class ManufactureEntityToDtoConverter extends AbstractIdentifiableConverter<Long,
        ManufactureEntity, ManufactureDto> {

    @Override
    public ManufactureDto convert(ManufactureEntity input) {
        final ManufactureDto output = super.convert(input);
        output.setName(input.getName());
        return output;
    }

    @Override
    protected ManufactureDto createOutput() {
        return new ManufactureDto();
    }

}
