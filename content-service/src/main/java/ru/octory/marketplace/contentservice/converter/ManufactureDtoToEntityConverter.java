package ru.octory.marketplace.contentservice.converter;

import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

@Component
public class ManufactureDtoToEntityConverter extends AbstractIdentifiableConverter<Long,
        ManufactureDto, ManufactureEntity> {

    @Override
    public ManufactureEntity convert(ManufactureDto input) {
        final ManufactureEntity output = super.convert(input);
        output.setName(input.getName());
        return output;
    }

    @Override
    protected ManufactureEntity createOutput() {
        return new ManufactureEntity();
    }

}
