package ru.octory.marketplace.contentservice.converter;

import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.ComponentPriceDto;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.contentservice.entity.ComponentPriceEntity;

@Component
public class ComponentPriceEntityToDtoConverter extends AbstractIdentifiableConverter<Long,
        ComponentPriceEntity, ComponentPriceDto> {

    @Override
    public ComponentPriceDto convert(ComponentPriceEntity input) {
        final ComponentPriceDto output = super.convert(input);
        output.setPrice(input.getPrice());
        output.setDateUpdate(input.getDateUpdate());
        return output;
    }

    @Override
    protected ComponentPriceDto createOutput() {
        return new ComponentPriceDto();
    }

}
