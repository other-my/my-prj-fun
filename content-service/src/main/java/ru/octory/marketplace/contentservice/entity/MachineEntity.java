package ru.octory.marketplace.contentservice.entity;

import lombok.*;
import ru.octory.marketplace.contentservice.data.AbstractIdEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Моя версия БД.
 * Класс машин.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "machine", schema = "catalog")
public class MachineEntity extends AbstractIdEntity<Long> {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Long id;

    // Код машины
    @Column(name = "code", length = 17, nullable = false, unique = true)
    private String code;
    // Модель машины
    @Column(name = "model", nullable = false)
    private String model;
    // Год выпуска
    @Column(name = "year", nullable = false)
    private Integer year;

    // Имя файла с изображением
    @Column(name = "image_file_name", nullable = false)
    private String imageFileName;

    // Производитель
    //    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "manufacture_id")
    private ManufactureEntity manufactureEntity;

    // Список узлов машины
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "link_node_machine",
            joinColumns = @JoinColumn(name = "machine_id"),
            inverseJoinColumns = @JoinColumn(name = "node_machine_id"))
    private List<NodeMachineEntity> nodeMachineEntities = new ArrayList<>();

    public void addNodeMachine(NodeMachineEntity nodeMachineEntity) {
        nodeMachineEntities.add(nodeMachineEntity);
    }

    public void removeNodeMachine(NodeMachineEntity nodeMachineEntity) {
        nodeMachineEntities.remove(nodeMachineEntity);
    }

}
