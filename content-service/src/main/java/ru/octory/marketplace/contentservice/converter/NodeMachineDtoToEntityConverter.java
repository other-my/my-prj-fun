package ru.octory.marketplace.contentservice.converter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.content.NodeMachineDto;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.contentservice.entity.MachineEntity;
import ru.octory.marketplace.contentservice.entity.NodeMachineEntity;

@AllArgsConstructor
@Component
public class NodeMachineDtoToEntityConverter extends AbstractIdentifiableConverter<Long,
        NodeMachineDto, NodeMachineEntity> {

    @Override
    public NodeMachineEntity convert(NodeMachineDto input) {
        final NodeMachineEntity output = super.convert(input);

//        output.setCode(input.getCode());
//        output.setModel(input.getModel());
//        output.setCode(input.getCode());
//        output.setCode(input.getCode());
//        output.setManufactureEntity(manufactureDtoToEntityConverter.convert(
//                input.getManufactureDto()));
//        output.setNodeMachineEntities(nodeMachineDtoListConverter.convert(
//                input.getNodeMachinesDto()));
        return output;
    }

    @Override
    protected NodeMachineEntity createOutput() {
        return new NodeMachineEntity();
    }

}
