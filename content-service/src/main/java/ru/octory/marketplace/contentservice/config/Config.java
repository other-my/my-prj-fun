package ru.octory.marketplace.contentservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:platform.properties")
public class Config {
//    @Value("inventory.img.location")
    public String inventoryImgLocation;
}
