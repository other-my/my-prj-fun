package ru.octory.marketplace.contentservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Моя версия БД.
 * Класс детали узла.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "component_node", schema = "catalog")
public class ComponentNodeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /* Если это OEM, то поле code_analog будет пустое,
    а если это аналог, то в поле code_analog будет записан code_oem.
    OEM это или аналог определяет флаг oemOrAnalog.
     */
    // код оригинальной детали
    @Column(name = "code_oem", length = 17, nullable = false, unique = true)
    private String codeOEM;
    // код аналога детали
    @Column(name = "code_analog", length = 17)
    private String codeAnalog;
    // Название детали
    @Column(name = "name", length = 255, nullable = false)
    private String name;
    // Версия
    @Column(name = "version", length = 11, nullable = false)
    private String version;

    // Имя файла с изображением
    @Column(name = "image_file_name")
    private String imageFileName;

    /* Если флаг = true, то это OEM, иначе аналог. */
    @Column(name = "oem_or_analog")
    private Boolean oemOrAnalog;

    // Производитель
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "manufacture_id")
    private ManufactureEntity manufactureEntity;

    // Стоимость узла
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "price_id")
    private ComponentPriceEntity componentPriceEntity;

    // Список узлов, на которых может быть установлена данная деталь
    @ManyToMany(mappedBy="componentNodes")
    private List<NodeMachineEntity> nodeMachines = new ArrayList<>();

}
