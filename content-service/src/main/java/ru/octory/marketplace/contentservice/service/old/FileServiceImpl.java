package ru.octory.marketplace.contentservice.service.old;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.octory.marketplace.contentservice.service.old.FileService;

@Slf4j
@AllArgsConstructor
@Service
public class FileServiceImpl implements FileService {
/*
    private final Config cfg;

    @Override
    public Boolean saveFile(MultipartFile file, Long id){
        var path = String.format(cfg.inventoryImgLocation, id);
        var newFile = new File(path);
        try (var inputStream = file.getInputStream();
             var outputStream =new FileOutputStream(newFile)){
            if (!newFile.exists()) {
                newFile.createNewFile();
            }
            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException ex) {
            log.error("saveFile({}, {}) -> {}", file, id, ex.getMessage());
            return false;
        }
        log.debug("saveFile({}, {}) -> {}", file, id, newFile.getAbsolutePath());
        return true;
    }

 */
}
