package ru.octory.marketplace.contentservice.service;


import ru.octory.marketplace.common.dto.inventory.PricelistDto;
import ru.octory.marketplace.common.enums.universal.Result;
import ru.octory.marketplace.contentservice.db.old.PriceList;

import java.util.List;

public interface PriceListService {

    /**
     * Список предложений с ценами от поставщиков
     *
     * @param id - id детали
     * @return список DTO с ценами
     */
    List<PricelistDto> findPricelistsByInventory(Long id);

    Result addPricelist(PricelistDto dto);

    List<PriceList> findPriceListsByPartNumOEM(String partNumOEM, Integer page, Integer pageSize);
}
