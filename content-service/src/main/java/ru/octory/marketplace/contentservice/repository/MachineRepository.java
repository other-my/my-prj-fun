package ru.octory.marketplace.contentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.entity.MachineEntity;

import java.util.Optional;

@Repository
public interface MachineRepository extends JpaRepository<MachineEntity, Long> {

    //Здесь будут добавлены специфичные методы для данной таблицы, все стандартные методы уже добавлены автоматически
//    void deleteByCategoryName(String name);

    // получение объекта по имени
    Optional<MachineEntity> findMachineEntityByCode(String code);

    void deleteById(Long id);

//    List<MachineEntity> getAllBy

}
