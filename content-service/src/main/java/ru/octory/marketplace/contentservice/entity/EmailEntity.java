package com.asvoip.ump.emailio.entity;

import com.asvoip.ump.dto.Direction;
import com.asvoip.ump.emailio.dto.EmailStatus;
import com.asvoip.ump.sqldbclient.entity.AbstractIdEntity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = EmailEntity.TABLE_NAME)
public class EmailEntity extends AbstractIdEntity<Long> {

    public static final String ATTRIBUTE_STATUS = "status";

    protected static final String TABLE_NAME = "email";
    private static final String COLUMN_SENT_DATE = "sent_date";
    private static final String COLUMN_CHANNEL_ID = "channel_id";

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EmailStatus status;

    @Column(nullable = false)
    private String address;

    @Column
    private String subject;

    @Column
    private String text;

    @Column(name = COLUMN_SENT_DATE)
    private LocalDateTime sentDate;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Direction direction;

    @Column(name = COLUMN_CHANNEL_ID)
    private UUID channelId;

    @OneToMany(mappedBy = TABLE_NAME, cascade = CascadeType.ALL)
    private List<EmailPartEntity> parts;

    @OneToMany(mappedBy = TABLE_NAME, cascade = CascadeType.ALL)
    private List<EmailAttachmentEntity> attachments;
}
