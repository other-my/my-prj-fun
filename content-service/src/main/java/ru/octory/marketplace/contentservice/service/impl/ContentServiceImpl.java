package ru.octory.marketplace.contentservice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContentServiceImpl {
//public class ContentServiceImpl implements ContentService {
/*
    private final ConverterInventoryService converterInventoryService;
    private final ManufactureRepositoryOld manufactureRepositoryOld;
    private final InventoryRepository inventoryRepository;
    private final InventoryRelationshipRepository inventoryRelationshipRepository;
    private final PointRepository pointRepository;
    private final FileService fileService;
    private final TypeRepository typeRepository;
    private Type machine;
    private Type assemble;
    private Type part;

    @PostConstruct
    private void init(){
        machine = typeRepository.findById(InventoryType.MACHINE.getValue())
                .orElse(new Type(0, "MACHINE"));
        assemble = typeRepository.findById(InventoryType.ASSEMBL.getValue())
                .orElse(new Type(1,"ASSEMBL"));
        part = typeRepository.findById(InventoryType.PART.getValue())
                .orElse(new Type(2, "PART"));
    }

    @Override
    public P<List<RelationshipDto>> findInventoriesByQuery(String query, Integer offset, Integer limit) {
        var searchField = URLDecoder.decode(query, StandardCharsets.UTF_8);
        var page = inventoryRelationshipRepository.findRightJoinByPartNumContainingOrPartNumOEMContainingOrPartNameContaining(
                searchField,
                PageRequest.of(offset, limit));
        log.info("findInventoriesByQuery({}, {}, {}) -> size: {}", query, offset, limit, page.getTotalElements());
        return new P<>(converterInventoryService.convertInventoryRelationshipsToRelationshipsDto(page.getContent()),
                page.getTotalPages(),
                page.getNumber());
    }

    @Override
    public List<ManufactureDtoOld> findManufactures() {
        var manufactures = manufactureRepositoryOld.findAll();
        log.info("findManufactures() -> size: {}", manufactures.size());
        return converterInventoryService.convertManufactureToManufactureDto(manufactures);
    }

    @Override
    public List<ManufactureDtoOld> findAllManufactures() {
        var manufactures = manufactureRepositoryOld.findAll();
        log.info("findManufactures() -> size: {}", manufactures.size());
        return converterInventoryService.convertManufactureToManufactureDto(manufactures);
    }


    @Override
    public List<InventoryDto> findMachines(Integer manufactureId) {
        try {
            var inventories = inventoryRepository.findByIdManufactureAndIdType(manufactureId, InventoryType.MACHINE.getValue());
            log.info("findMachines({}) -> size: {}", manufactureId, inventories.size());
            return converterInventoryService.convertInventoriesToInventoriesDto(inventories);
        } catch (Exception e) {
            log.error("findMachines({}) -> {}", manufactureId, e.getMessage());
        }
        return Collections.emptyList();
    }

    @Override
    public List<RelationshipDto> findChildren(Long parentId) {
        var relationships = inventoryRelationshipRepository.findByIdParent(parentId);
        log.info("findChildren({}) -> size: {}", parentId, relationships.size());
        return relationships.size() != 0 ? converterInventoryService.convertInventoryRelationshipsToRelationshipsDto(relationships) : Collections.emptyList();
    }

    @Override
    public List<RelationshipDto> findParent(Long inventoryId) {
        var inventoryRelationship = inventoryRelationshipRepository.findByIdChild(inventoryId);
        log.info("findParent({}) -> exists: {}", inventoryId, inventoryRelationship != null);
        return inventoryRelationship == null ? null : converterInventoryService.convertInventoryRelationshipsToRelationshipsDto(inventoryRelationship);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result addInventories(List<RelationshipDto> inventories, String requestId) {
        var result = Result.SUCCESS;
        try {
            var start = System.currentTimeMillis();
            var count = new AtomicInteger(1);
            converterInventoryService.convertRelationshipDtoToRelationShip(inventories).forEach(inventoryRelationship -> {
                log.info("current relationship: " + count.getAndIncrement() + ". " + inventoryRelationship);
                initialize(inventoryRelationship, requestId);
            });
            var end = System.currentTimeMillis() - start;
            log.info("addInventories({}) -> inventories size: {} -> time: {} ms", inventories, inventories.size(), end);
        } catch (Exception e) {
            log.error("addInventories() -> {}", e.getMessage());
            result = Result.FAIL;
        }
        return result;
    }

    @Override
    public P<List<RelationshipDto>> findInventoriesByPartNumbers(List<String> partNumbers, Integer offset, Integer limit){
        var page = inventoryRelationshipRepository.findRightJoinInventoryByPartNumInOrPartNumOEMIn(partNumbers, PageRequest.of(offset, limit));
        log.info("findInventoriesByPartNumbers({}, {}, {}) -> size: {}", partNumbers, offset, limit, page.getTotalElements());
        return new P<>(converterInventoryService.convertInventoryRelationshipsToRelationshipsDto(page.getContent()),
                page.getTotalPages(),
                page.getNumber());
    }

    @Override
    public Result saveSchema(MultipartFile file, Long id) {
        var result = new AtomicReference<>(Result.SUCCESS);
        var inventory = inventoryRepository.findById(id);
        inventory.ifPresentOrElse(inv -> {
            Boolean path = fileService.saveFile(file, inv.getId());
            inv.setImageExists(path);
            inventoryRepository.save(inv);
        }, () -> result.set(Result.NOT_FOUND));
        log.info("saveSchema({}, {}) -> {}", file, id, result);
        return result.get();
    }
    @Override
    public InventoryDto findInventoryById(Long id){
        var inventory = inventoryRepository.findById(id);
//        log.info("get perents of {} -> {}", id, inventory.get().getParents());
//        log.info("get children of {} -> {}", id, inventory.get().getChildren());
        return converterInventoryService.convertInventoryToInventoryDto(inventory.get());
    }

    @Override
    public List<PointDto> getInventoryPoints(Long id) {
        var points = converterInventoryService.convertPointsToPointsDto(pointRepository.findAllByRelationshipChildren(id));
        log.debug("getInventoryPoints({}) -> {}", id, points);
        return points;
    }
    @Override
    public Result addInventory(RelationshipDto dto, String requestId) {
        var result = Result.SUCCESS;
        try {
            var inventoryRelationship = converterInventoryService.convertRelationshipDtoToRelationShip(dto);
            log.info("inventoryRelationship({}) -> inventoryRelationship: {}", dto, inventoryRelationship);
            initialize(inventoryRelationship, requestId);
        } catch (Exception e) {
            log.error("addInventory({}) -> {}", dto, e);
            result = Result.FAIL;
        }
        return result;
    }

    @Override
    public P<List<RelationshipDto>> getLastAddedInventories(int offset, int limit) {

        try {
            var page = inventoryRelationshipRepository.findAllRightJoinInventory(PageRequest.of(offset, limit, Sort.by(Sort.Direction.DESC, "i.date_added")));
            log.info("getLastAddedInventories({}, {}) -> size: {}", offset, limit, page.getTotalElements());
            return new P<>(converterInventoryService.convertInventoryRelationshipsToRelationshipsDto(page.getContent()),
                    page.getTotalPages(),
                    page.getNumber());

        } catch (Exception e) {
            log.error("getLastAddedInventories({}, {}) -> {}", offset, limit, e);
            return new P<>(Collections.emptyList(),
                    0,
                    0);
        }
    }

    @Override
    public P<List<RelationshipDto>> getLastAddedMachine(String requestId, int offset, int limit) {
        try {
            var page = inventoryRelationshipRepository.findByRequestId(requestId, PageRequest.of(offset, limit, Sort.by(Sort.Direction.DESC, "i.date_added")));
            return new P<>(converterInventoryService.convertInventoryRelationshipsToRelationshipsDto(page.getContent()),
                    page.getTotalPages(),
                    page.getNumber());
        } catch (Exception e) {
            log.error("getLastAddedMachine(, {}, {}, {}) -> {}", requestId, offset, limit, e);
            return new P<>(Collections.emptyList(),
                    0,
                    0);
        }
    }



    private void initialize(InventoryRelationship inventoryRelationship, String requestId) {
        Inventory child = initialize(inventoryRelationship.getInventoryChild(), requestId);
        Inventory parent = initialize(inventoryRelationship.getInventoryParent(), requestId);
        inventoryRelationship.setInventoryChild(child);
//        inventoryRelationship.setIdChild(child.getId());
        if (parent != null) {
            inventoryRelationship.setInventoryParent(parent);
//            inventoryRelationship.setIdParent(parent.getId());
        }
        log.info("relationshipProcessing({}, {})", inventoryRelationship, requestId);
        if (checkRelationshipBeforePersist(inventoryRelationship)) {
            inventoryRelationshipRepository.save(inventoryRelationship);
        }
    }

    private boolean checkRelationshipBeforePersist(InventoryRelationship inventoryRelationship) {
//        return inventoryRelationship.getInventoryChild() != null &&
//                inventoryRelationship.getInventoryParent() != null &&
//                inventoryRelationship.getIdParent() != null &&
//                inventoryRelationship.getIdChild() != null &&
//                !inventoryRelationshipRepository.existsByIdParentAndIdChild(inventoryRelationship.getIdParent(),
//                        inventoryRelationship.getIdChild());
        return true;
    }

    private Inventory initialize(Inventory inventoryFromDocument, String requestId) {
//        if (inventoryFromDocument != null) {
//            var manufacture = manufactureRepositoryOld.findByName(inventoryFromDocument.getManufactureEntity().getName())
//                    .orElse(null);
//            Inventory inventory = findInventoryByAnyFields(inventoryFromDocument, manufacture);
//            if (inventory == null) {
//                inventory = createNewInventory(inventoryFromDocument, manufacture, requestId);
//            }
//            log.info("inventoryProcessing({}, {}) -> {}", inventoryFromDocument, requestId, inventory);
//            return inventory;
//        }
        return null;
    }

    private Inventory findInventoryByAnyFields(Inventory inventoryFromDocument, ManufactureEntity manufactureEntity) {
        if (inventoryFromDocument.getId() != null) {
            return inventoryRepository.findById(inventoryFromDocument.getId()).
                    orElse(null);
        } else if (inventoryFromDocument.getPartNumOEM() != null) {
            return inventoryRepository.findByPartNumOEM(inventoryFromDocument.getPartNumOEM());
        } else if (manufactureEntity != null) {
            return inventoryRepository.findByPartNameAndManufacture(inventoryFromDocument.getPartName(), manufactureEntity);
        } else {
            return inventoryRepository.findByPartName(inventoryFromDocument.getPartName());
        }
    }

    private Inventory createNewInventory(Inventory inventoryFromDocument, ManufactureEntity manufactureEntity, String requestId) {
        switch (inventoryFromDocument.getType().getName()) {
            case INVENTORY_MACHINE -> inventoryFromDocument.setType(machine);
            case INVENTORY_ASSEMBLE -> inventoryFromDocument.setType(assemble);
            case INVENTORY_PART -> inventoryFromDocument.setType(part);
        }

//        inventoryFromDocument.setManufacture(Objects.requireNonNullElseGet(manufacture, () -> manufactureRepository.save(Manufacture.builder()
//                .name(inventoryFromDocument.getManufacture().getName())
//                .build())));
        inventoryFromDocument.setDateAdded(LocalDateTime.now());
        inventoryFromDocument.setRequestId(requestId);
        return inventoryRepository.save(inventoryFromDocument);
    }

 */
}
