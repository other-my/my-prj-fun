package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.db.old.Type;

@Repository
public interface TypeRepository extends JpaRepository<Type, Integer> {
}
