package ru.octory.marketplace.contentservice.service.impl;

import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.octory.marketplace.common.dto.data.ListAttributes;
import ru.octory.marketplace.common.exception.ConverterException;
import ru.octory.marketplace.common.service.StringConverter;
import ru.octory.marketplace.common.service.StringConverters;
import ru.octory.marketplace.common.service.impl.StringConvertersImpl;
import ru.octory.marketplace.contentservice.converter.impl.OperatorConverterImpl;
import ru.octory.marketplace.contentservice.service.RestApiRequestService;

/**
 * REST API Request сервис. Метод Создать атрибуты списка: limit - опционально (1..N), offset -
 * смещение, опционально (0..N). sort - опционально, формат 'sort=attribute1,-attribute2'
 * ('sort=order,-timestamp' ASC by 'order' and DESC by 'timestamp'). filters - опционально, формат
 * 'filters=attribute,value,operator,type;...' опциональный type: 'UUID','OffsetDateTime'
 * ('filters=customerId,a71eb578-93da-4a09-a819-936079b00201,EQ,UUID').
 */
@Service
@Slf4j
public class RestApiRequestServiceImpl implements RestApiRequestService {

    private static final String FILTER_DELIMITER = ";";
    private static final String VALUE_DELIMITER = ",";

    private final OperatorConverterImpl operatorConverter;

    private final StringConverters stringConverters;

    @SuppressWarnings("java:S3740")
    @Autowired
    public RestApiRequestServiceImpl(
            OperatorConverterImpl operatorConverter,
            List<StringConverter<? extends Serializable>> converters) {
        this.operatorConverter = operatorConverter;
        this.stringConverters = new StringConvertersImpl(converters);
    }

    @Override
    public StringConverters getStringConverters() {
        return stringConverters;
    }

    /**
     * Создать атрибуты списка.
     *
     * @param limit   лимит
     * @param offset  смещение
     * @param sort    сортировка
     * @param filters фильтры
     * @return атрибуты списка
     */
    @Override
    public ListAttributes createListAttributes(
            Integer limit, Integer offset, String sort, String filters) {
        final ListAttributes listAttributes = new ListAttributes(limit, offset, sort);
        if (StringUtils.hasLength(filters)) {
            final String[] filtersArray = filters.split(FILTER_DELIMITER);
            for (final String stringFilter : filtersArray) {
                if (StringUtils.hasLength(stringFilter)) {
                    addListFilter(listAttributes, stringFilter);
                }
            }
        }
        return listAttributes;
    }

    private void addListFilter(
            ListAttributes listAttributes, String stringFilter) {
        final String[] elements = stringFilter.split(VALUE_DELIMITER);
        if (elements.length == 3 || elements.length == 4) {
            addListFilterImpl(listAttributes, elements);
        } else {
            throw new ConverterException("Incorrect filter format '" + stringFilter + "'");
        }
    }

    private <T extends Serializable> void addListFilterImpl(
            ListAttributes listAttributes, String... elements) {
        final String property = elements[0].trim();
        if (StringUtils.hasLength(property)) {
            final Integer operator = operatorConverter.get(elements[2].trim());
            final T value = StringUtils.hasLength(elements[1]) ? extractValue(elements) : null;
            listAttributes.addFilter(property, value, operator);
        }
    }

    private <T extends Serializable> T extractValue(String... elements) {
        final String type = elements.length == 4 ? elements[3].trim() : null;
        T value;
        if (type == null) {
            value = (T) elements[1];
        } else {
            value = (T) stringConverters.get(type).convert(elements[1]);
        }
        return value;
    }

}
