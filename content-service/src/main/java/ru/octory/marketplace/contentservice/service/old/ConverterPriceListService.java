package ru.octory.marketplace.contentservice.service.old;

import ru.octory.marketplace.common.dto.inventory.PricelistDto;
import ru.octory.marketplace.contentservice.db.old.Inventory;
import ru.octory.marketplace.contentservice.db.old.MarketplacePricelist;
import ru.octory.marketplace.contentservice.db.old.PriceList;

import java.util.List;

public interface ConverterPriceListService {
    List<PricelistDto> convertMarketplacePlaylistsToPlaylistsDto(List<MarketplacePricelist> marketplacePricelists);

    PricelistDto convertMarketplacePlaylistToPlaylistDto(MarketplacePricelist marketplacePricelist);

    PriceList convertPriceListDtoToPricelist(PricelistDto pricelistDto, Inventory inventory);

    MarketplacePricelist convertPlaylistToMarketplacePricelist(PriceList priceList, String id);
}
