package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.db.old.Point;

import java.util.List;

@Repository
public interface PointRepository extends JpaRepository<Point, Long> {
    @Query(value = "SELECT p FROM Point p JOIN InventoryRelationship ir WHERE ir.idParent = ?1")
    List<Point>findAllByRelationshipChildren(Long id);
}
