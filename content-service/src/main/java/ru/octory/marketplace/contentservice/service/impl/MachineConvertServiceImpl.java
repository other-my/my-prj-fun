package ru.octory.marketplace.contentservice.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.UnexpectedRollbackException;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.*;
import ru.octory.marketplace.common.exception.ConverterException;
import ru.octory.marketplace.contentservice.converter.MachineDtoToEntityConverter;
import ru.octory.marketplace.contentservice.converter.MachineEntityToDtoConverter;
import ru.octory.marketplace.contentservice.entity.MachineEntity;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.service.MachineConvertService;
import ru.octory.marketplace.contentservice.service.MachineService;

import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
public class MachineConvertServiceImpl implements MachineConvertService {

    private final MachineDtoToEntityConverter machineDtoToEntityConverter;
    private final MachineEntityToDtoConverter machineEntityToDtoConverter;
    private final MachineService machineService;

    @Override
    public ResponseEntity<DataResponse<MachineDto>> postEntity(DataRequest<MachineDto> request) {
        // LATER: Сделать проверку что объект manufacture из request существует в таблице
        log.info("MachineConvertServiceImpl -> postEntity -> start");
        log.info("request = {}", request);
        final MachineEntity entity =
                machineDtoToEntityConverter.convert(request.getData());
        log.info("entity getCode = " + entity.getCode());
        log.info("entity getModel = " + entity.getModel());
        log.info("MachineConvertServiceImpl -> postEntity -> create");
        machineService.create(entity);
        log.info("ManufactureConvertServiceImpl -> postEntity -> create--end");
        log.info("entity = {}", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataResponse<MachineDto>> putEntity(DataRequest<MachineDto> request) {
        log.info("MachineConvertServiceImpl -> postEntity -> start");
        log.info("request = {}", request);
        final MachineEntity entity =
                machineDtoToEntityConverter.convert(request.getData());
        log.info("MachineConvertServiceImpl -> postEntity -> create");
        machineService.update(entity);
        log.info("MachineConvertServiceImpl -> postEntity -> create--end");
        log.info("entity = {}", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataResponse<MachineDto>> getEntity(Long id) {
        log.info("MachineConvertServiceImpl -> getEntity -> start");
        log.info("id = " + id);
        final Optional<MachineEntity> optionalEntity = machineService.get(id);
        if (optionalEntity.isEmpty()) {
            log.info("notFoundEntity");
            return notFoundEntity(id);
        }
        final MachineEntity entity = optionalEntity.get();
        log.info("entity = {} ", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ListResponse<MachineDto>> getEntity() {
        log.info("MachineConvertServiceImpl -> getEntity -> start");
        final ListResult<MachineEntity> listResult = machineService.getAll();
        if (listResult.getList().isEmpty()) {
            log.info("notFoundEntity");
            return badRequestListResponse("Производители не найдены");
        }
        try {
            final ListResult<MachineDto> responseListResult = convertListResult(listResult);
            final ListResponse<MachineDto> response = createListResponse(responseListResult);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ConverterException e) {
            if (log.isErrorEnabled()) {
                log.error(e.getMessage());
            }
            return badRequestListResponse(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<DataResponse<MachineDto>> getEntity(String name) {
        log.info("MachineConvertServiceImpl -> getEntity -> start");
        log.info("id = " + name);
        final Optional<MachineEntity> optionalEntity = machineService.getByName(name);
        if (optionalEntity.isEmpty()) {
            log.info("notFoundEntity");
            return notFoundEntity(name);
        }
        final MachineEntity entity = optionalEntity.get();
        log.info("entity = {} ", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataResponse<MachineDto>> deleteEntity(Long id) {
        log.info("MachineConvertServiceImpl -> deleteEntity -> start");
        try {
            final boolean result = machineService.delete(id);
            if (!result) {
                log.info("false");
                return notFoundEntity(id);
            }
            final DataResponse<MachineDto> response = new DataResponse<>();
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (UnexpectedRollbackException exp) {
            log.info("false");
            return notFoundEntity(id);
        }
    }

    private ResponseEntity<DataResponse<MachineDto>> convertEntityToDtoAndCreateResponse(
            MachineEntity entity, HttpStatus created) {
        final MachineDto dto = machineEntityToDtoConverter.convert(entity);
        final DataResponse<MachineDto> response = createDataResponse(dto);
        return new ResponseEntity<>(response, created);
    }

    protected ListResult<MachineDto> convertListResult(ListResult<MachineEntity> listResult) {
        final ListResult<MachineDto> responseListResult = new ListResult<>();
        responseListResult.setCount(listResult.getCount());
        responseListResult.setTotalCount(listResult.getTotalCount());
        listResult.getList().forEach(e -> {
            final MachineDto dto = machineEntityToDtoConverter.convert(e);
            responseListResult.addObject(dto);
        });
        return responseListResult;
    }

    private DataResponse<MachineDto> createDataResponse(MachineDto dto) {
        final DataResponse<MachineDto> dataResponse = new DataResponse<>();
        dataResponse.setData(dto);
        return dataResponse;
    }

    protected ListResponse<MachineDto> createListResponse(ListResult<MachineDto> listResult) {
        final ListResponse<MachineDto> listResponse = new ListResponse<>();
        listResponse.setListResult(listResult);
        return listResponse;
    }

    protected ResponseEntity<DataResponse<MachineDto>> notFoundEntity(Long id) {
        final DataResponse<MachineDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        final String message = "Not found entity by id: " + id;
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    /**
     * После прехода на поиск объектов удалить
     * @param name
     * @return
     */
    protected ResponseEntity<DataResponse<MachineDto>> notFoundEntity(String name) {
        final DataResponse<MachineDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        final String message = "Not found entity by id: " + name;
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    protected ResponseEntity<ListResponse<MachineDto>> badRequestListResponse(String message) {
        final ListResponse<MachineDto> response = new ListResponse<>();
        response.setStatus(StatusType.ERROR);
        log.error(message);
        response.addMessage(HttpStatus.BAD_REQUEST.value(), message);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


}
