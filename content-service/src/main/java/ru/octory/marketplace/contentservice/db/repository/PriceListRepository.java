package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.db.old.PriceList;

import java.util.List;

@Repository
public interface PriceListRepository extends JpaRepository<PriceList, Long> {
    List<PriceList> findByIdInventory(Long idInventory);
    List<PriceList>findByInventory_partNumOEM(String partNumOem);
}
