package ru.octory.marketplace.contentservice.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.common.dto.data.ListResponse;

public interface ManufactureConvertService {

    ResponseEntity<DataResponse<ManufactureDto>> postEntity(
            @RequestBody DataRequest<ManufactureDto> request);

    ResponseEntity<DataResponse<ManufactureDto>> putEntity(DataRequest<ManufactureDto> request);

    ResponseEntity<DataResponse<ManufactureDto>> getEntity(Long id);

    ResponseEntity<DataResponse<ManufactureDto>> getEntity(String name);

    ResponseEntity<ListResponse<ManufactureDto>> getEntity();

    ResponseEntity<DataResponse<ManufactureDto>> deleteEntity(Long id);

}
