package ru.octory.marketplace.contentservice.service.old;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
//public class ConverterInventoryServiceImpl implements ConverterInventoryService {
public class ConverterInventoryServiceImpl {

    /*
    private final PriceListService priceListService;

    @Override
    public List<InventoryDto> convertInventoriesToInventoriesDto(List<Inventory> inventories) {
        return inventories.stream().map(this::convertInventoryToInventoryDto).collect(Collectors.toList());
    }

    @Override
    public InventoryDto convertInventoryToInventoryDto(Inventory inventory) {
        var dto = new InventoryDto();
        dto.setId(inventory.getId());
        dto.setPartNumOEM(inventory.getPartNumOEM());
        dto.setPartNum(inventory.getPartNum());
        dto.setPartName(inventory.getPartName());
        dto.setVersion(inventory.getVersion());
        dto.setYear(inventory.getYear());
        dto.setImageExists(inventory.getImageExists());
        dto.setType(new TypeDto(inventory.getType().getId(), inventory.getType().getName()));
        dto.setManufacture(new ManufactureDtoOld(inventory.getManufactureEntity().getId(), inventory.getManufactureEntity().getName()));
        var priceLists = inventory.getPartNumOEM() != null ?
                priceListService.findPriceListsByPartNumOEM(inventory.getPartNumOEM(), null, null) :
                inventory.getPriceLists();
        dto.setMinPrice(priceLists.stream()
                .map(PriceList::getPrice)
                .min(Integer::compare)
                .orElse(null));
        dto.setMaxPrice(inventory.getPriceLists().size() == 0 ? null : inventory.getPriceLists().stream()
                .map(PriceList::getPrice)
                .max(Integer::compare)
                .orElse(null));
        return dto;
        return null;
    }

    @Override
    public List<ManufactureDtoOld> convertManufactureToManufactureDto(List<ManufactureEntity> manufactureEntities) {
//        return manufactureEntities.stream().map(manufacture -> new ManufactureDtoOld(manufacture.getId(), manufacture.getName())).collect(Collectors.toList());
        return null;
    }

    @Override
    public List<InventoryRelationship> convertRelationshipDtoToRelationShip(List<RelationshipDto> manufactures) {
        return manufactures.stream().map(this::convertRelationshipDtoToRelationShip).collect(Collectors.toList());
    }

    @Override
    public InventoryRelationship convertRelationshipDtoToRelationShip(RelationshipDto relationshipDto) {
        return InventoryRelationship.builder()
                .id(relationshipDto.getId())
                .inventoryChild(convertInventoryDtoInventory(relationshipDto.getInventoryChild()))
                .inventoryParent(relationshipDto.getInventoryParent() == null ? null : convertInventoryDtoInventory(relationshipDto.getInventoryParent()))
                .quantity(relationshipDto.getQuantity())
                .build();
    }

    @Override
    public Inventory convertInventoryDtoInventory(InventoryDto inventoryDto) {
        return Inventory.builder()
                .id(inventoryDto.getId())
//                .manufactureEntity(convertManufactureDtoManufacture(inventoryDto.getManufacture()))
                .imageExists(false)
                .year(inventoryDto.getYear())
                .partNum(cleanSpaces(inventoryDto.getPartNum()))
                .partNumOEM(cleanSpaces(inventoryDto.getPartNumOEM()))
                .partName(inventoryDto.getPartName())
                .type(convertTypeDtoType(inventoryDto.getType()))
                .build();
    }

    @Override
    public Type convertTypeDtoType(TypeDto typeDto) {
        return Type.builder()
                .name(typeDto.getName())
                .build();
    }

    @Override
    public ManufactureEntity convertManufactureDtoManufacture(ManufactureDtoOld manufactureDto) {
        return null;
//        return Manufacture.builder()
//                .name(manufactureDto.getName())
//                .build();
    }

    @Override
    public List<RelationshipDto> convertInventoryRelationshipsToRelationshipsDto(List<InventoryRelationship> relationships) {
        return relationships
                .stream()
                .map(this::convertInventoryRelationshipToRelationshipDto)
                .collect(Collectors.toList());
    }

    @Override
    public RelationshipDto convertInventoryRelationshipToRelationshipDto(InventoryRelationship relationship) {
        System.out.println(relationship);
        var dto = new RelationshipDto();
        dto.setQuantity(relationship.getQuantity());
        dto.setId(relationship.getId());
        dto.setInventoryChild(convertInventoryToInventoryDto(relationship.getInventoryChild()));
//        dto.setInventoryParent(relationship.getIdParent() == null ? null : convertInventoryToInventoryDto(relationship.getInventoryParent()));
        return dto;
    }
    @Override
    public List<PointDto> convertPointsToPointsDto(List<Point> points){
        return points.stream()
                .map(this:: convertPointToPointDto)
                .collect(Collectors.toList());
    }
    @Override
    public PointDto convertPointToPointDto(Point point){
//        return new PointDto(point.getPivotX(),
//                point.getPivotY(),
//                point.getRelationship().getInventoryChild().getPartName(),
//                point.getRelationship().getIdChild());
        return null;
    }

 */
}
