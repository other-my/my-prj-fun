package ru.octory.marketplace.contentservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Моя версия БД.
 * Класс содержит информацию о кол-во узлов в каждой машине.
 * Возмодно эту сущность не надо создавать
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "count_node_machine", schema = "catalog")
public class CountNodeMachineEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "count", nullable = false)
    private Integer count;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "machine_id")
    private MachineEntity machineEntity;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "node_machine_id")
    private NodeMachineEntity nodeMachineEntity;

    /*

<changeSet id="create-table-book" author="uPagge">
    <createTable tableName="book">
        <column name="id" type="int" autoIncrement="true">
            <constraints nullable="false" primaryKey="true"/>
        </column>
        <column name="name" type="varchar(64)"/>
        <column name="author_id" type="int"/>
    </createTable>

    <addForeignKeyConstraint baseTableName="book" baseColumnNames="author_id"
                             constraintName="book_author_id_person_id"
                             referencedTableName="person"
                             referencedColumnNames="id"
                             onUpdate="CASCADE"/>
</changeSet>

добавить вот это
    <addUniqueConstraint
        columnNames="product_id, tournament_id"
        constraintName="your_constraint_name"
        tableName="person"
        />
     */

}
