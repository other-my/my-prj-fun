package ru.octory.marketplace.contentservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.octory.marketplace.common.data.RestApiConstants;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.common.dto.data.ListResponse;
import ru.octory.marketplace.contentservice.service.MachineConvertService;
import ru.octory.marketplace.contentservice.service.MachineService;

/**
 * Контроллер обработки вспех запросов касающихся машин
 * @author alex
 */
@Slf4j
@RestController
@RequestMapping(MachineController.API_URL)
@AllArgsConstructor
@Tag(name = "Content API", description = "Контроллер обработки вспех запрсов касающихся машин")
public class MachineController {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "machine";

    private final MachineConvertService machineConvertService;
    private final MachineService machineService;

    @Operation(summary = "Добавить машину", description = "Добавление в БД новой машины")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Машина добавлена",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PostMapping("/add")
    public ResponseEntity<DataResponse<MachineDto>> addMachine(@RequestBody DataRequest<MachineDto> request) {
        log.info("MachineController -> addMachine -> start");
        log.info("MachineController request = {}", request);
        return machineConvertService.postEntity(request);
    }

    @Operation(summary = "Изменить машину", description = "Изменить в БД машину")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Машина изменена",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PutMapping("/update")
    public ResponseEntity<DataResponse<MachineDto>> updateMachine(@RequestBody DataRequest<MachineDto> request) {
        log.info("MachineController -> updateManufacture -> start");
        log.info("MachineController request = {}", request);
        return machineConvertService.putEntity(request);
    }

    @Operation(summary = "Получить машину", description = "Получение из БД машины")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Машину получен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @GetMapping("/get/" + RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<MachineDto>> getMachine(@PathVariable(RestApiConstants.PARAM_ID) Long id) {
        log.info("MachineController -> addManufacture -> start");
        return machineConvertService.getEntity(id);
    }

    @Operation(summary = "Получить всех машины", description = "Получение из БД всех машин")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Машины получены",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @GetMapping("/get-all")
    public ResponseEntity<ListResponse<MachineDto>> getAllMachine() {
        log.info("MachineController -> getAllMachine -> start");
        return machineConvertService.getEntity();
    }

    @Operation(summary = "Получить машину", description = "Получение из БД машины")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Машина получена",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @GetMapping("/get-by-name/" + RestApiConstants.VARIABLE_NAME)
    public ResponseEntity<DataResponse<MachineDto>> getByName(@PathVariable(RestApiConstants.PARAM_NAME) String code) {
        log.info("MachineController -> getByName -> start");
        return machineConvertService.getEntity(code);
    }

    @Operation(summary = "Удалить машину", description = "Удаление из БД машины")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Машина удален",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @DeleteMapping("/delete/" + RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<MachineDto>> delete(@PathVariable(RestApiConstants.PARAM_ID) Long id) {
        log.info("MachineController -> delete -> start");
        return machineConvertService.deleteEntity(id);
    }

}
