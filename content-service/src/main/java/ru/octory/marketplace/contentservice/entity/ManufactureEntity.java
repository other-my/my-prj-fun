package ru.octory.marketplace.contentservice.entity;

import lombok.*;
import ru.octory.marketplace.contentservice.data.AbstractIdEntity;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "manufacture", schema = "catalog")
public class ManufactureEntity extends AbstractIdEntity<Long> {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    // производитель
    @Column(name = "name", nullable = false, unique = true)
    private String name;
}
