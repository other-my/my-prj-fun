package ru.octory.marketplace.contentservice.service;

import ru.octory.marketplace.common.dto.data.ListResult;
import ru.octory.marketplace.contentservice.dao.MachineDao;
import ru.octory.marketplace.contentservice.entity.MachineEntity;

import java.util.Optional;

public interface MachineService extends MachineDao {

    Optional<MachineEntity> getByName(String code);

    ListResult<MachineEntity> getAll();

}
