package ru.octory.marketplace.contentservice.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.ListAttributes;
import ru.octory.marketplace.common.dto.data.ListResult;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.repository.ManufactureRepository;
import ru.octory.marketplace.contentservice.service.ManufactureService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
@Transactional
public class ManufactureServiceImpl implements ManufactureService {
    // LATER: Переделать на CrudService вместо Dao

    private final ManufactureRepository manufactureRepository;

    @Override
    public void create(ManufactureEntity object) {
        manufactureRepository.save(object);
    }

    @Override
    public void update(ManufactureEntity object) {
        manufactureRepository.save(object);
    }

    @Override
    public Optional<ManufactureEntity> get(Long id) {
        log.info("ManufactureServiceImpl -> get -> start");
        Optional<ManufactureEntity> manufacture = manufactureRepository.findById(id);
        // если значение представлено - вернуть его
        if (manufacture.isPresent()) {
            return manufacture;
        } else {
            log.info("notFoundEntity");
            return Optional.empty();
        }
    }

    @Override
    public ListResult<ManufactureEntity> getAll() {
        log.info("ManufactureServiceImpl -> get -> start");
        List<ManufactureEntity> manufacture = manufactureRepository.findAll();
        ListResult<ManufactureEntity> listResult = new ListResult<>();
        listResult.setList(manufacture);
        return listResult;
    }


    @Override
    public Optional<ManufactureEntity> getByName(String name) {
        log.info("ManufactureServiceImpl -> getByName -> start");
        Optional<ManufactureEntity> manufacture =
                manufactureRepository.findManufactureEntityByName(name);
        // если значение представлено - вернуть его
        if (manufacture.isPresent()) {
            return manufacture;
        } else {
            log.info("notFoundEntity");
            return Optional.empty();
        }
    }

    @Override
    public boolean delete(Long id) {
        log.info("ManufactureServiceImpl -> delete -> start");
        try {
            manufactureRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException exp) {
            log.error("Такая запись не найдена в БД.");
            return false;
        }
    }

    @Override
    public ListResult<ManufactureEntity> find(ListAttributes listAttributes) {
        return null;
    }


    @Override
    public ManufactureEntity create(Long id, ManufactureEntity object) {
        return null;
    }

    @Override
    public boolean createOrUpdate(ManufactureEntity object) {
        return false;
    }

    @Override
    public boolean update(ManufactureEntity object, Map<String, Object> data) {
        return ManufactureService.super.update(object, data);
    }




    /* Тут все устаревшие функции
    @Override
    public List<ManufactureDto> getAll() {
        return manufactureRepository.findAll();
    }

    @Override
    public List<ManufactureDto> search(String... searchString) {
        //        при необходимости задействовать этот метод
        //        return authorRepository.findByFioContainingIgnoreCaseOrderByFio(searchString[0]);
        return null;
    }

    @Override
    public ManufactureDto get(Long id) {
        // Optional - обертка, в котором может быть значение или пусто (используется для исключение ошибки NullPointerException
        Optional<ManufactureDto> categoryId = manufactureRepository.findById(id);
        // если значение представлено - вернуть его
        if (categoryId.isPresent()) {
            return categoryId.get();
        } else {
            return null;
        }
    }

    @Override
    public ManufactureDto save(ManufactureDto obj) {
        return null;
    }

    @Override
    public ManufactureDto update(ManufactureDto obj) {
        return null;
    }

    @Override
    public void delete(ManufactureDto object) {

    }

     */


}
