package ru.octory.marketplace.contentservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Моя версия БД.
 * Класс стоимость узлов.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "node_price", schema = "catalog")
public class NodePriceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // Цена
    @Column(name = "price", nullable = false)
    private Double price;
    // Дата обновления цены
    @Column(name = "date_update", nullable = false)
    private Date dateUpdate;
}
