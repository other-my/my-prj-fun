package ru.octory.marketplace.contentservice.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.RequestBody;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.*;
import ru.octory.marketplace.common.exception.ConverterException;
import ru.octory.marketplace.contentservice.converter.ManufactureDtoToEntityConverter;
import ru.octory.marketplace.contentservice.converter.ManufactureEntityToDtoConverter;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.service.ManufactureConvertService;
import ru.octory.marketplace.contentservice.service.ManufactureService;

import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
public class ManufactureConvertServiceImpl implements ManufactureConvertService {

    private final ManufactureDtoToEntityConverter manufactureDtoToEntityConverter;
    private final ManufactureEntityToDtoConverter manufactureEntityToDtoConverter;
    private final ManufactureService manufactureService;

    @Override
    public ResponseEntity<DataResponse<ManufactureDto>> postEntity(DataRequest<ManufactureDto> request) {
        log.info("ManufactureConvertServiceImpl -> postEntity -> start");
        log.info("request = {}", request);
        final ManufactureEntity entity =
                manufactureDtoToEntityConverter.convert(request.getData());
        log.info("ManufactureConvertServiceImpl -> postEntity -> create");
        manufactureService.create(entity);
        log.info("ManufactureConvertServiceImpl -> postEntity -> create--end");
        log.info("entity = {}", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataResponse<ManufactureDto>> putEntity(DataRequest<ManufactureDto> request) {
        log.info("ManufactureConvertServiceImpl -> postEntity -> start");
        log.info("request = {}", request);
        final ManufactureEntity entity =
                manufactureDtoToEntityConverter.convert(request.getData());
        log.info("ManufactureConvertServiceImpl -> postEntity -> create");
        manufactureService.update(entity);
        log.info("ManufactureConvertServiceImpl -> postEntity -> create--end");
        log.info("entity = {}", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataResponse<ManufactureDto>> getEntity(Long id) {
        log.info("ManufactureConvertServiceImpl -> getEntity -> start");
        log.info("id = " + id);
        final Optional<ManufactureEntity> optionalEntity = manufactureService.get(id);
        if (optionalEntity.isEmpty()) {
            log.info("notFoundEntity");
            return notFoundEntity(id);
        }
        final ManufactureEntity entity = optionalEntity.get();
        log.info("entity = {} ", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ListResponse<ManufactureDto>> getEntity() {
        log.info("ManufactureConvertServiceImpl -> getEntity -> start");
        final ListResult<ManufactureEntity> listResult = manufactureService.getAll();
        if (listResult.getList().isEmpty()) {
            log.info("notFoundEntity");
            return badRequestListResponse("Производители не найдены");
        }
        try {
            final ListResult<ManufactureDto> responseListResult = convertListResult(listResult);
            final ListResponse<ManufactureDto> response = createListResponse(responseListResult);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ConverterException e) {
            if (log.isErrorEnabled()) {
                log.error(e.getMessage());
            }
            return badRequestListResponse(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<DataResponse<ManufactureDto>> getEntity(String name) {
        log.info("ManufactureConvertServiceImpl -> getEntity -> start");
        log.info("id = " + name);
        final Optional<ManufactureEntity> optionalEntity = manufactureService.getByName(name);
        if (optionalEntity.isEmpty()) {
            log.info("notFoundEntity");
            return notFoundEntity(name);
        }
        final ManufactureEntity entity = optionalEntity.get();
        log.info("entity = {} ", entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataResponse<ManufactureDto>> deleteEntity(Long id) {
        log.info("ManufactureConvertServiceImpl -> deleteEntity -> start");
        try {
            final boolean result = manufactureService.delete(id);
            if (!result) {
                log.info("false");
                return notFoundEntity(id);
            }
            final DataResponse<ManufactureDto> response = new DataResponse<>();
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (UnexpectedRollbackException exp) {
            log.info("false");
            return notFoundEntity(id);
        }
    }

    private ResponseEntity<DataResponse<ManufactureDto>> convertEntityToDtoAndCreateResponse(
            ManufactureEntity entity, HttpStatus created) {
        final ManufactureDto dto = manufactureEntityToDtoConverter.convert(entity);
        final DataResponse<ManufactureDto> response = createDataResponse(dto);
        return new ResponseEntity<>(response, created);
    }

    protected ListResult<ManufactureDto> convertListResult(ListResult<ManufactureEntity> listResult) {
        final ListResult<ManufactureDto> responseListResult = new ListResult<>();
        responseListResult.setCount(listResult.getCount());
        responseListResult.setTotalCount(listResult.getTotalCount());
        listResult.getList().forEach(e -> {
            final ManufactureDto dto = manufactureEntityToDtoConverter.convert(e);
            responseListResult.addObject(dto);
        });
        return responseListResult;
    }

    private DataResponse<ManufactureDto> createDataResponse(ManufactureDto dto) {
        final DataResponse<ManufactureDto> dataResponse = new DataResponse<>();
        dataResponse.setData(dto);
        return dataResponse;
    }

    protected ListResponse<ManufactureDto> createListResponse(ListResult<ManufactureDto> listResult) {
        final ListResponse<ManufactureDto> listResponse = new ListResponse<>();
        listResponse.setListResult(listResult);
        return listResponse;
    }

    protected ResponseEntity<DataResponse<ManufactureDto>> notFoundEntity(Long id) {
        final DataResponse<ManufactureDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        final String message = "Not found entity by id: " + id;
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    /**
     * После прехода на поиск объектов удалить
     * @param name
     * @return
     */
    protected ResponseEntity<DataResponse<ManufactureDto>> notFoundEntity(String name) {
        final DataResponse<ManufactureDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        final String message = "Not found entity by id: " + name;
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    protected ResponseEntity<ListResponse<ManufactureDto>> badRequestListResponse(String message) {
        final ListResponse<ManufactureDto> response = new ListResponse<>();
        response.setStatus(StatusType.ERROR);
        log.error(message);
        response.addMessage(HttpStatus.BAD_REQUEST.value(), message);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}
