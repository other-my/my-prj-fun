package ru.octory.marketplace.contentservice.service.old;


import ru.octory.marketplace.common.dto.inventory.*;
import ru.octory.marketplace.contentservice.db.old.Inventory;
import ru.octory.marketplace.contentservice.db.old.InventoryRelationship;
import ru.octory.marketplace.contentservice.db.old.Point;
import ru.octory.marketplace.contentservice.db.old.Type;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import java.util.List;

public interface ConverterInventoryService {
    /**
     * Конвертация списка entity деталей -> список DTO деталей
     *
     * @param inventories - список entity деталей
     * @return список DTO деталей
     */
    List<InventoryDto> convertInventoriesToInventoriesDto(List<Inventory> inventories);

    /**
     * Конвертация entity детали -> DTO детали
     *
     * @param inventory - entity детали
     * @return DTO детали
     */
    InventoryDto convertInventoryToInventoryDto(Inventory inventory);

    /**
     * Конвертация списка entity производителей -> список DTO производителей
     *
     * @param manufactureEntities - список entity производителей
     * @return список DTO производителей
     */
    List<ManufactureDtoOld> convertManufactureToManufactureDto(List<ManufactureEntity> manufactureEntities);
    /**
     * Конвертация списка DTO узлов -> список entity узлов
     *
     * @param relationships - список DTO узлов
     * @return список entity узлов
     */
    List<InventoryRelationship> convertRelationshipDtoToRelationShip(List<RelationshipDto> relationships);
    /**
     * Конвертация списка DTO узлов -> список entity узлов
     *
     * @param relationships - список DTO узлов
     * @return список entity узлов
     */
    List<RelationshipDto> convertInventoryRelationshipsToRelationshipsDto(List<InventoryRelationship> relationships);

    RelationshipDto convertInventoryRelationshipToRelationshipDto(InventoryRelationship relationship);

    Inventory convertInventoryDtoInventory(InventoryDto inventoryDto);

    Type convertTypeDtoType(TypeDto typeDto);

    ManufactureEntity convertManufactureDtoManufacture(ManufactureDtoOld manufactureDto);

    InventoryRelationship convertRelationshipDtoToRelationShip(RelationshipDto relationshipDto);

    List<PointDto> convertPointsToPointsDto(List<Point> points);

    PointDto convertPointToPointDto(Point point);
}
