package ru.octory.marketplace.contentservice.service;

import ru.octory.marketplace.common.dto.data.ListAttributes;
import ru.octory.marketplace.common.service.StringConverters;

public interface RestApiRequestService {

    StringConverters getStringConverters();

    ListAttributes createListAttributes(Integer limit, Integer offset, String sort, String filters);
}
