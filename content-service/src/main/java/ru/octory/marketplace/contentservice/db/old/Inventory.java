package ru.octory.marketplace.contentservice.db.old;

import lombok.*;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "inventories", schema = "inventories")
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "part_num_oem")
    private String partNumOEM;
    @Column(name = "part_num")
    private String partNum;
    @Column(name = "part_name")
    private String partName;
    private String version;
    private Integer year;
    @Column(name = "image_exists")
    private Boolean imageExists;


    @ManyToOne
    @JoinColumn(name = "id_type", nullable = false)
    private Type type;
//    @Column(name = "id_type", insertable = false, updatable = false)
//    private Integer idType;

//    @ManyToOne
//    @JoinColumn(name = "id_manufacture", nullable = false)
//    private ManufactureEntity manufactureEntity;
//    @Column(name = "id_manufacture", insertable = false, updatable = false)
//    private Integer idManufacture;

    @Column(name = "date_added")
    private LocalDateTime dateAdded;
    @Column(name = "request_id")
    private String requestId;

    /**
     * Возможно надо создать в Liqubase ключи?
     */
//    @OneToMany(mappedBy = "inventory", fetch = FetchType.EAGER)
//    @Setter(AccessLevel.NONE)
//    private List<PriceList> priceLists = new ArrayList<>();

//    @OneToMany(mappedBy = "inventoryParent")
//    @Setter(AccessLevel.NONE)
//    private List<InventoryRelationship> children = new ArrayList<>();
//    @OneToMany(mappedBy = "inventoryChild")
//    @Setter(AccessLevel.NONE)
//    private List<InventoryRelationship> parents = new ArrayList<>();



//    public void addParent(InventoryRelationship parent) {
//        children.add(parent);
//        parent.setInventoryChild(this);
//    }
//
//    public void removeParent(InventoryRelationship parent) {
//        children.remove(parent);
//        parent.setInventoryChild(null);
//    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Inventory.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("partNumOEM='" + partNumOEM + "'")
                .add("partNum='" + partNum + "'")
                .add("partName='" + partName + "'")
                .add("version='" + version + "'")
                .add("year=" + year)
                .add("imageExists='" + imageExists + "'")
                .add("type=" + type)
//                .add("manufacture=" + manufactureEntity)
                .add("dateAdded=" + dateAdded)
                .toString();
    }
}
