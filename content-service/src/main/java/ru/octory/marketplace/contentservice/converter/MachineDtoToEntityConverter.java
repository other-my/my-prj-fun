package ru.octory.marketplace.contentservice.converter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.dto.content.MachineDto;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.content.NodeMachineDto;
import ru.octory.marketplace.common.dto.converter.Converter;
import ru.octory.marketplace.common.dto.converter.impl.AbstractIdentifiableConverter;
import ru.octory.marketplace.common.dto.converter.impl.ListConverter;
import ru.octory.marketplace.contentservice.entity.MachineEntity;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.entity.NodeMachineEntity;

//@AllArgsConstructor
@Component
public class MachineDtoToEntityConverter extends AbstractIdentifiableConverter<Long,
        MachineDto, MachineEntity> {

    private final Converter<ManufactureDto, ManufactureEntity> manufactureDtoToEntityConverter;
    private final ListConverter<NodeMachineDto, NodeMachineEntity> nodeMachineDtoListConverter;

    @Autowired
    public MachineDtoToEntityConverter(
            ManufactureDtoToEntityConverter manufactureConverter,
            NodeMachineDtoToEntityConverter nodeMachineConverter) {
        super();
        this.manufactureDtoToEntityConverter = manufactureConverter;
        nodeMachineDtoListConverter = new ListConverter<>(nodeMachineConverter);
    }

    @Override
    public MachineEntity convert(MachineDto input) {
        final MachineEntity output = super.convert(input);
        output.setCode(input.getCode());
        output.setModel(input.getModel());
        output.setYear(input.getYear());
        output.setImageFileName(input.getImageFileName());
        output.setManufactureEntity(manufactureDtoToEntityConverter.convert(
                input.getManufactureDto()));
        output.setNodeMachineEntities(nodeMachineDtoListConverter.convert(
                input.getNodeMachinesDto()));
        return output;
    }

    @Override
    protected MachineEntity createOutput() {
        return new MachineEntity();
    }

}
