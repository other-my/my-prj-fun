package ru.octory.marketplace.contentservice.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.octory.marketplace.common.dto.data.ListAttributes;
import ru.octory.marketplace.common.dto.data.ListResult;
import ru.octory.marketplace.contentservice.entity.MachineEntity;
import ru.octory.marketplace.contentservice.repository.MachineRepository;
import ru.octory.marketplace.contentservice.service.MachineService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
@Transactional
public class MachineServiceImpl implements MachineService {

    // LATEST Переделать на CrudService вместо Dao

    private final MachineRepository machineRepository;

    @Override
    public void create(MachineEntity object) {
        machineRepository.save(object);
    }

    @Override
    public void update(MachineEntity object) {
        machineRepository.save(object);
    }

    @Override
    public Optional<MachineEntity> get(Long id) {
        log.info("MachineServiceImpl -> get -> start");
        Optional<MachineEntity> machine = machineRepository.findById(id);
        // если значение представлено - вернуть его
        if (machine.isPresent()) {
            return machine;
        } else {
            log.info("notFoundEntity");
            return Optional.empty();
        }
    }

    @Override
    public ListResult<MachineEntity> getAll() {
        log.info("MachineServiceImpl -> get -> start");
        List<MachineEntity> machineList = machineRepository.findAll();
        ListResult<MachineEntity> listResult = new ListResult<>();
        listResult.setList(machineList);
        return listResult;
    }

    @Override
    public Optional<MachineEntity> getByName(String code) {
        log.info("MachineServiceImpl -> getByName -> start");
        Optional<MachineEntity> manufacture =
                machineRepository.findMachineEntityByCode(code);
        // если значение представлено - вернуть его
        if (manufacture.isPresent()) {
            return manufacture;
        } else {
            log.info("notFoundEntity");
            return Optional.empty();
        }
    }

    @Override
    public boolean delete(Long id) {
        log.info("ManufactureServiceImpl -> delete -> start");
        try {
            machineRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException exp) {
            log.error("Такая запись не найдена в БД.");
            return false;
        }
    }

    @Override
    public ListResult<MachineEntity> find(ListAttributes listAttributes) {
        return null;
    }

    @Override
    public MachineEntity create(Long id, MachineEntity object) {
        return null;
    }

    @Override
    public boolean createOrUpdate(MachineEntity object) {
        return false;
    }

    @Override
    public boolean update(MachineEntity object, Map<String, Object> data) {
        return MachineService.super.update(object, data);
    }


}
