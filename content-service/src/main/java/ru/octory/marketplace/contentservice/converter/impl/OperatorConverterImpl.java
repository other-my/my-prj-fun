package ru.octory.marketplace.contentservice.converter.impl;

import java.util.HashMap;
import org.springframework.stereotype.Component;
import ru.octory.marketplace.common.exception.ConverterException;

@Component
public class OperatorConverterImpl extends HashMap<String, Integer> {

    private static final int OP_EQUAL = 0;
    private static final int OP_NOT_EQUAL = 1;
    private static final int OP_LESS_THAN = 2;
    private static final int OP_GREATER_THAN = 3;
    private static final int OP_LESS_OR_EQUAL = 4;
    private static final int OP_GREATER_OR_EQUAL = 5;
    private static final int OP_LIKE = 6;
    private static final int OP_ILIKE = 7;
    private static final int OP_NULL = 10;
    private static final int OP_NOT_NULL = 11;

    private static final String OPERATOR_EQ = "EQ";
    private static final String OPERATOR_NE = "NE";
    private static final String OPERATOR_LT = "LT";
    private static final String OPERATOR_GT = "GT";
    private static final String OPERATOR_LE = "LE";
    private static final String OPERATOR_GE = "GE";
    private static final String OPERATOR_LIKE = "LIKE";
    private static final String OPERATOR_ILIKE = "ILIKE";
    private static final String OPERATOR_NULL = "NULL";
    private static final String OPERATOR_NOT_NULL = "NOT_NULL";

    /**
     * Конструктор конвертера операций.
     */
    public OperatorConverterImpl() {
        super();
        put(OPERATOR_EQ, OP_EQUAL);
        put(OPERATOR_NE, OP_NOT_EQUAL);
        put(OPERATOR_LT, OP_LESS_THAN);
        put(OPERATOR_GT, OP_GREATER_THAN);
        put(OPERATOR_LE, OP_LESS_OR_EQUAL);
        put(OPERATOR_GE, OP_GREATER_OR_EQUAL);
        put(OPERATOR_LIKE, OP_LIKE);
        put(OPERATOR_ILIKE, OP_ILIKE);
        put(OPERATOR_NULL, OP_NULL);
        put(OPERATOR_NOT_NULL, OP_NOT_NULL);
    }

    @Override
    public Integer get(Object strOperator) {
        final Integer operator = super.get(strOperator);
        if (operator == null) {
            throw new ConverterException("No converter found for operator: '" + strOperator + "'");
        }
        return operator;
    }
}
