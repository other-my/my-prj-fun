package ru.octory.marketplace.contentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import java.util.Optional;

@Repository
public interface ManufactureRepository extends JpaRepository<ManufactureEntity, Long> {
    //Здесь будут добавлены специфичные методы для данной таблицы, все стандартные методы уже добавлены автоматически
//    void deleteByCategoryName(String name);

    // получение объекта по имени
    Optional<ManufactureEntity> findManufactureEntityByName(String name);

    void deleteById(Long id);

//    List<ManufactureEntity> getAllBy


}
