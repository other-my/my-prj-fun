package ru.octory.marketplace.contentservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Моя версия БД.
 * Класс содержит информацию о кол-во деталей в каждом узле.
 * Возмодно эту сущность не надо создавать
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "count_component_node", schema = "catalog")
public class CountComponentNodeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "count", nullable = false)
    private Integer count;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "node_machine_id")
    private NodeMachineEntity nodeMachineEntity;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "component_node_id")
    private ComponentNodeEntity componentNodeEntity;

}
