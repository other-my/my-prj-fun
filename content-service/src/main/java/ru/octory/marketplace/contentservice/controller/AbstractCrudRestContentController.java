package ru.octory.marketplace.contentservice.controller;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import ru.octory.marketplace.common.dto.converter.Converter;
import ru.octory.marketplace.common.dto.data.*;
import ru.octory.marketplace.common.service.CrudService;
import ru.octory.marketplace.contentservice.service.RestApiRequestService;
import ru.octory.marketplace.common.exception.ConverterException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Optional;

/**
 * Абстрактный CRUD контроллер.
 * Переделанная реализация под Jpa
 */
@Slf4j
public abstract class AbstractCrudRestContentController
        <I extends Serializable, E extends Identifiable<I>, D extends Identifiable<I>,
        S extends CrudService<E, I>, R extends Converter<E, D>, P extends Converter<D, E>> {

    @Autowired
    private RestApiRequestService restApiRequestService;

    @Getter
    private final S crudService;

    @Getter
    private final R entityToDtoConverter;

    @Getter
    private final P dtoToEntityConverter;

    protected AbstractCrudRestContentController(
            S crudService, R entityToDtoConverter, P dtoToEntityConverter) {
        this.crudService = crudService;
        this.entityToDtoConverter = entityToDtoConverter;
        this.dtoToEntityConverter = dtoToEntityConverter;
    }

    protected ResponseEntity<DataResponse<D>> notFoundEntity(I id) {
        final DataResponse<D> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        final String message = "Not found entity by id: " + id;
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<DataResponse<D>> convertEntityToDtoAndCreateResponse(
            E entity, HttpStatus created) {
        final D dto = entityToDtoConverter.convert(entity);
        final DataResponse<D> response = createDataResponse(dto);
        return new ResponseEntity<>(response, created);
    }

    protected ResponseEntity<DataResponse<D>> getEntity(I id) {
        final Optional<E> optionalEntity = crudService.get(id);
        if (optionalEntity.isEmpty()) {
            return notFoundEntity(id);
        }
        final E entity = optionalEntity.get();
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.OK);
    }

    protected ResponseEntity<DataResponse<D>> postEntity(@RequestBody DataRequest<D> request) {
        final E entity = dtoToEntityConverter.convert(request.getData());
        crudService.create(entity);
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.CREATED);
    }

    protected ResponseEntity<DataResponse<D>> postEntity(I id, @RequestBody DataRequest<D> request) {
        final Optional<E> optionalEntity = crudService.get(id);
        if (optionalEntity.isEmpty()) {
            final E createEntity = dtoToEntityConverter.convert(request.getData());
            final E createdEntity = crudService.create(id, createEntity);
            return convertEntityToDtoAndCreateResponse(createdEntity, HttpStatus.CREATED);
        } else {
            return updateEntity(id, request);
        }
    }

    protected ResponseEntity<DataResponse<D>> putEntity(I id, @RequestBody DataRequest<D> request) {
        final Optional<E> optionalEntity = crudService.get(id);
        if (optionalEntity.isEmpty()) {
            return notFoundEntity(id);
        }
        return updateEntity(id, request);
    }

    private ResponseEntity<DataResponse<D>> updateEntity(I id, DataRequest<D> request) {
        request.getData().setId(id);
        final E updateEntity = dtoToEntityConverter.convert(request.getData());
        crudService.update(updateEntity);
        return convertEntityToDtoAndCreateResponse(updateEntity, HttpStatus.OK);
    }

    protected ResponseEntity<DataResponse<D>> patchEntity(
            I id, DataRequest<HashMap<String, Object>> request) {
        final Optional<E> optionalEntity = crudService.get(id);
        if (optionalEntity.isEmpty()) {
            return notFoundEntity(id);
        }
        final E entity = optionalEntity.get();
        final HashMap<String, Object> data = request.getData();
        final boolean result = crudService.update(entity, data);
        if (!result) {
            return badRequestDataResponse(id, request);
        }
        return convertEntityToDtoAndCreateResponse(entity, HttpStatus.OK);
    }

    protected ResponseEntity<DataResponse<D>> badRequestDataResponse(I id, DataRequest<?> request) {
        final DataResponse<D> response = new DataResponse<>();
        response.setStatus(StatusType.ERROR);
        final String message = "For id: '" + id + "' bad request: " + request.toString();
        log.error(message);
        response.addMessage(HttpStatus.BAD_REQUEST.value(), message);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    protected ResponseEntity<DataResponse<D>> deleteEntity(I id) {
        final boolean result = crudService.delete(id);
        if (!result) {
            return notFoundEntity(id);
        }
        final DataResponse<D> response = new DataResponse<>();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Поиск списка данных.
     *
     * @param limit   лимит
     * @param offset  смещение
     * @param sort    сортировка
     * @param filters фильтры
     * @return ответ со списочным результатом данных
     */
    protected ResponseEntity<ListResponse<D>> findEntities(
            Integer limit, Integer offset, String sort, String filters) {
        try {
            final ListAttributes listAttributes =
                    restApiRequestService.createListAttributes(limit, offset, sort, filters);
            final ListResult<E> listResult = crudService.find(listAttributes);
            final ListResult<D> responseListResult = convertListResult(listResult);
            final ListResponse<D> response = createListResponse(responseListResult);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ConverterException e) {
            if (log.isErrorEnabled()) {
                log.error(e.getMessage());
            }
            return badRequestListResponse(e.getMessage());
        }
    }

    protected DataResponse<D> createDataResponse(D dto) {
        final DataResponse<D> dataResponse = new DataResponse<>();
        dataResponse.setData(dto);
        return dataResponse;
    }

    protected ListResponse<D> createListResponse(ListResult<D> listResult) {
        final ListResponse<D> listResponse = new ListResponse<>();
        listResponse.setListResult(listResult);
        return listResponse;
    }

    protected ResponseEntity<ListResponse<D>> badRequestListResponse(String message) {
        final ListResponse<D> response = new ListResponse<>();
        response.setStatus(StatusType.ERROR);
        log.error(message);
        response.addMessage(HttpStatus.BAD_REQUEST.value(), message);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    protected ResponseEntity<ListResponse<D>> badRequestListResponse(
            ResponseMessage responseMessage) {
        final ListResponse<D> response = new ListResponse<>();
        response.setStatus(StatusType.ERROR);
        if (log.isErrorEnabled()) {
            log.error(responseMessage.getMessage());
        }
        response.addMessage(responseMessage);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    protected ListResult<D> convertListResult(ListResult<E> listResult) {
        final ListResult<D> responseListResult = new ListResult<>();
        responseListResult.setCount(listResult.getCount());
        responseListResult.setTotalCount(listResult.getTotalCount());
        listResult.getList().forEach(e -> {
            final D dto = entityToDtoConverter.convert(e);
            responseListResult.addObject(dto);
        });
        return responseListResult;
    }

    protected ResponseEntity<DataResponse<D>> errorDataResponse(
            HttpStatus httpStatus, String message) {
        final DataResponse<D> response = new DataResponse<>();
        response.setStatus(StatusType.ERROR);
        log.error(message);
        response.addMessage(httpStatus.value(), message);
        return new ResponseEntity<>(response, httpStatus);
    }

}
