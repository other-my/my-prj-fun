package ru.octory.marketplace.contentservice.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
//public class PriceListServiceImpl implements PriceListService {
public class PriceListServiceImpl {

//    private final MarketplacePricelistRepository marketplacePricelistRepository;
//    private final PriceListRepository priceListRepository;
//    private final ClientService clientService;
//    private final ConverterPriceListService converterPriceListService;
//    private final InventoryRepository inventoryRepository;
/*
    @Override
    public List<PricelistDto> findPricelistsByInventory(Long id) {
        var pricelists = marketplacePricelistRepository.findByPriceList_idInventory(id);
        if (pricelists == null || pricelists.isEmpty()) {
            return Collections.emptyList();
        }
        var pricelistDto = converterPriceListService.convertMarketplacePlaylistsToPlaylistsDto(pricelists);
        Map<String, UserInfoDto> idToOrganization = clientService.findOrganizationsByIds(
                pricelists.stream().map(MarketplacePricelist::getIdMarketplace)
                        .collect(Collectors.toList())).
                stream().collect(Collectors.toMap(UserInfoDto::getId, info -> info));
        return pricelistDto.stream()
                .peek(prclst -> prclst.setOrganization(idToOrganization.get(prclst.getOrganization().getId())))
                .collect(Collectors.toList());

        return null;
    }

    @Override
    public Result addPricelist(PricelistDto dto) {
        var result = Result.SUCCESS;
        try {
            var inventory = inventoryRepository.findById(dto.getInventory().getId()).orElseThrow(() -> {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            });
            var priceList = converterPriceListService.convertPriceListDtoToPricelist(dto, inventory);
            marketplacePricelistRepository.save(converterPriceListService.convertPlaylistToMarketplacePricelist(priceList,dto.getOrganization().getId()));
        } catch (Exception e){
            log.error("addPricelist({}) -> {}", dto, e.getMessage());
            result = Result.FAIL;
        }
        log.info("addPricelist({}) -> {}", dto, result);
        return result;

        return null;
    }

    @Override
    public List<PriceList> findPriceListsByPartNumOEM(String partNumOEM, Integer page, Integer pageSize){
        if (page == null || pageSize == null){
            return priceListRepository.findByInventory_partNumOEM(partNumOEM);
        } else {
            return new ArrayList<>();
        }
    }

 */

}
