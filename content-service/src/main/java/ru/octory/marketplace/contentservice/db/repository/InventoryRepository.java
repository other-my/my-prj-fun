package ru.octory.marketplace.contentservice.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.octory.marketplace.contentservice.db.old.Inventory;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {

    Inventory findByPartNumOEM(String  partNumOEM);

    Inventory findByPartNameAndManufacture(String partName, ManufactureEntity manufactureEntity);

    Inventory findByPartName(String partName);

    List<Inventory> findByIdManufactureAndIdType(Integer idManufacture, Integer idType);
}
