package ru.octory.marketplace.contentservice.db.old;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.StringJoiner;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "inventory_relationship", schema = "inventories")
public class InventoryRelationship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_parent", nullable = false)
    private Inventory inventoryParent;
//    @Column(name = "id_parent", insertable = false, updatable = false)
//    private Long idParent;




    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_child", nullable = false)
    private Inventory inventoryChild;
//    @Column(name = "id_child", insertable = false, updatable = false)
//    private Long idChild;



    private Integer quantity;

    @OneToOne
    @JoinColumn(name = "point_id")
    private Point point;


    @Override
    public String toString() {
        return new StringJoiner(", ", InventoryRelationship.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("inventoryParent=" + inventoryParent)
                .add("inventoryChild=" + inventoryChild)
                .add("quantity=" + quantity)
                .add("point_id=" + point)
//                .add("id_parent=" + idParent)
//                .add("id_child=" + idChild)
                .toString();
    }
}
