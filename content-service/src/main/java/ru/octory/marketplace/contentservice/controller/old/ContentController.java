package ru.octory.marketplace.contentservice.controller.old;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.octory.marketplace.common.data.RestApiConstants;
import ru.octory.marketplace.common.dto.inventory.*;
import ru.octory.marketplace.common.enums.universal.Result;
import ru.octory.marketplace.contentservice.service.ContentService;

import java.util.List;

import static ru.octory.marketplace.common.utils.StringUtils.md5FromString;

@RestController
@RequestMapping("/inventory")
@AllArgsConstructor
@Tag(name = "Content API", description = "Контроллер для получение данных относящихся к деталям или тесно с ними связанных")
public class ContentController {

//    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "content";
//
//    private final ContentService contentService;

    /** Новые функции **/

//    @Operation(summary = "Добавить машину", description = "Добавить машину")
//    @PostMapping("/machine/add")
//    public String addMachine(@RequestBody List<RelationshipDto> relationships) {
//        var requestId = generateRequestId();
//        contentService.addInventories(relationships, requestId);
//        return requestId;
//    }


    /* ------------------------------------ ***/
    /** Тут все старые функции **/

    /*
    @Operation(summary = "Поиск оборудования", description = "Поиск Inventory по part_num, по part_num_oem, part_name с использованием пагинации")
    @GetMapping("/search")
    public P<List<RelationshipDto>> getInventoryByQuery(@RequestParam String query,
                                                        @RequestParam Integer offset,
                                                        @RequestParam Integer limit) {
        return contentService.findInventoriesByQuery(query, offset, limit);
    }

    @Operation(summary = "Производители", description = "Получение списка всех Manufacture")
    @GetMapping("/manufactures")
    public List<ManufactureDtoOld> getManufactures() {
        return contentService.findManufactures();
    }

    @Operation(summary = "Машины", description = "Получение списка всех Inventory по id manufacture, у которых type = machine")
    @GetMapping("/machines/{id}")
    public List<InventoryDto> getMachinesByManufacture(@PathVariable Integer id) {
        return contentService.findMachines(id);
    }

    @Operation(summary = "Связанные детали", description = "Получение списка children inventory")
    @GetMapping("/children/{id}")
    public List<RelationshipDto> getChildren(@PathVariable Long id) {
        return contentService.findChildren(id);
    }

    @Operation(summary = "Добавить список запчастей", description = "Добавить список запчастей")
    @PostMapping("/inventories")
    public String addInventories(@RequestBody List<RelationshipDto> relationships) {
        var requestId = generateRequestId();
        contentService.addInventories(relationships, requestId);
        return requestId;
    }

    @Operation(summary = "Связанные детали", description = "Получение parent inventory")
    @GetMapping("/parent/{id}")
    public List<RelationshipDto> getParent(@PathVariable Long id) {
        return contentService.findParent(id);
    }

    @Operation(summary = "Получить точки изображений", description = "Получить все обозначения деталей на рисунке")
    @GetMapping("/points/{id}")
    List<PointDto> getInventoryPoints(@PathVariable Long id){
        return contentService.getInventoryPoints(id);
    }

    @Operation(summary = "Получить инфо о детали", description = "Получить инфо о детали по id")
    @GetMapping("/{id}")
    public InventoryDto getInventoryById(@PathVariable Long id){
        return contentService.findInventoryById(id);
    }

    @Operation(summary = "Добавить изображение", description = "Добавить изображение схемы к детали")
    @PostMapping("/schema")
    public Result saveSchema(@RequestPart MultipartFile file, @RequestPart Long id){
        return contentService.saveSchema(file, id);
    }
    @Operation(summary = "Поиск нескольких деталей", description = "Поиск нескольких деталей по их partNum или partNumOEM")
    @GetMapping("/multi-search")
    public P<List<RelationshipDto>> getInventoriesByPartNumbers(@RequestParam List<String> partNum,
                                                                @RequestParam Integer offset,
                                                                @RequestParam Integer limit){
        return contentService.findInventoriesByPartNumbers(partNum, offset, limit);
    }

    @Operation(summary = "Получение всех деталей", description = "Получение последне добавленных деталей")
    @GetMapping("/latest")
    public P<List<RelationshipDto>> getLastAddedInventories(@RequestParam(required = false, defaultValue = "0") int offset,
                                                            @RequestParam(required = false, defaultValue = "15") int limit) {
        return contentService.getLastAddedInventories(offset, limit);
    }

    @GetMapping("/latest/{requestId}")
    public P<List<RelationshipDto>> getLastAddedMachine(@PathVariable String requestId,
                                                        @RequestParam(required = false, defaultValue = "0") int offset,
                                                        @RequestParam(required = false, defaultValue = "15") int limit) {
        return contentService.getLastAddedMachine(requestId, offset, limit);
    }

    private String generateRequestId() {
        long time = System.currentTimeMillis();
        return md5FromString(String.valueOf(time));
    }

     */
}
