@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = ChannelEntity.TABLE_NAME)
public class ChannelEntity extends AbstractNamedUuidIdEntity {

    protected static final String TABLE_NAME = "channel";
    private static final String TABLE_CHANNEL_PROPERTY_MAP = "channel_property_map";

    private static final String COLUMN_APPLICATION_ID = "application_id";
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_DIRECTION = "direction";
    private static final String COLUMN_CODE = "code";
    private static final String COLUMN_CHANNEL_ID = "channel_id";
    private static final String COLUMN_PROPERTY_ID = "property_id";

    @Column(name = COLUMN_APPLICATION_ID, nullable = false)
    private UUID applicationId;

    @Enumerated(EnumType.STRING)
    @Column(name = COLUMN_TYPE, nullable = false)
    private ChannelType type;

    @Enumerated(EnumType.STRING)
    @Column(name = COLUMN_DIRECTION, nullable = false)
    private Direction direction;

    @Column(name = COLUMN_CODE, nullable = false)
    private String code;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = TABLE_CHANNEL_PROPERTY_MAP,
            joinColumns = {@JoinColumn(name = COLUMN_CHANNEL_ID, referencedColumnName = ID_COLUMN)},
            inverseJoinColumns = {
                    @JoinColumn(name = COLUMN_PROPERTY_ID, referencedColumnName = ID_COLUMN)
            })
    @MapKey(name = AbstractNamedIdEntity.COLUMN_NAME)
    private Map<String, ChannelPropertyEntity> properties;
}

