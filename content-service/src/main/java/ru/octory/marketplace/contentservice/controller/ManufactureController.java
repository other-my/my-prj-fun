package ru.octory.marketplace.contentservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.octory.marketplace.common.data.RestApiConstants;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.common.dto.data.ListResponse;
import ru.octory.marketplace.contentservice.service.ManufactureConvertService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Контроллер обработки всех запрсов касающихся производителй
 * @author alex
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(ManufactureController.API_URL)
@Tag(name = "Content API", description = "Контроллер обработки вспех запрсов касающихся машин")
public class ManufactureController {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "manufacture";

    private final ManufactureConvertService manufactureConvertService;

    @Operation(summary = "Добавить производителя", description = "Добавление в БД производителя " +
            "машины или детали")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель добавлен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PostMapping("/add")
    public ResponseEntity<DataResponse<ManufactureDto>> addManufacture(@RequestBody DataRequest<ManufactureDto> request) {
        log.info("ManufactureController -> addManufacture -> start");
        log.info("ManufactureController request = {}", request);
        return manufactureConvertService.postEntity(request);
    }

    @Operation(summary = "Изменить производителя", description = "Изменить в БД производителя " +
            "машины или детали")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель Изменен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PutMapping("/update")
    public ResponseEntity<DataResponse<ManufactureDto>> updateManufacture(@RequestBody DataRequest<ManufactureDto> request) {
        log.info("ManufactureController -> updateManufacture -> start");
        log.info("ManufactureController request = {}", request);
        return manufactureConvertService.putEntity(request);
    }

    @Operation(summary = "Получить производителя", description = "Получение из БД производителя, " +
            "машины или детали")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель получен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @GetMapping("/get/" + RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<ManufactureDto>> getManufacture(@PathVariable(RestApiConstants.PARAM_ID) Long id) {
        log.info("ManufactureController -> addManufacture -> start");
        return manufactureConvertService.getEntity(id);
    }

    @Operation(summary = "Получить всех производителей", description = "Получение из БД " +
            "всех производителей ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель получен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @GetMapping("/get-all")
    public ResponseEntity<ListResponse<ManufactureDto>> getAllManufacture() {
        log.info("ManufactureController -> getAllManufacture -> start");
        return manufactureConvertService.getEntity();
    }

    @Operation(summary = "Получить производителя", description = "Получение из БД " +
            "производителя по имени ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель получен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @GetMapping("/get-by-name/" + RestApiConstants.VARIABLE_NAME)
    public ResponseEntity<DataResponse<ManufactureDto>> getByName(@PathVariable(RestApiConstants.PARAM_NAME) String name) {
        log.info("ManufactureController -> addManufacture -> start");
        return manufactureConvertService.getEntity(name);
    }

    @Operation(summary = "Удалить производителя", description = "Удаление из БД производителя, " +
            "машины или детали")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Производитель удален",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ManufactureDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Not Acceptable",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @DeleteMapping("/delete/" + RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<ManufactureDto>> delete(@PathVariable(RestApiConstants.PARAM_ID) Long id) {
        log.info("ManufactureController -> addManufacture -> start");
        return manufactureConvertService.deleteEntity(id);
    }


}
