package ru.octory.marketplace.contentservice.it;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = AbstractSpringBootIT.TEST_PROPERTIES)
public abstract class AbstractSpringBootIT {

    protected AbstractSpringBootIT() {
    }

    protected static final String TEST_PROPERTIES =
            "spring.main.allow-bean-definition-overriding=true";
}
