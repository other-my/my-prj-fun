package ru.octory.marketplace.contentservice.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.contentservice.ITUtil;
import ru.octory.marketplace.contentservice.converter.ManufactureEntityToDtoConverter;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

@SpringBootTest
public class ManufactureEntityToDtoConverterIT {

    @Autowired
    private ManufactureEntityToDtoConverter manufactureEntityToDtoConverter;

    @Test
    @DisplayName("Тест Converter Entity to ManufactureDto")
//    @Tag("Тест Converter Entity to ManufactureDto")
    public void convert() {
        final ManufactureEntity manufactureEntity = ITUtil.createManufactureEntity(
                ITUtil.MANUFACTURE_ID_02, ITUtil.MANUFACTURE_NAME_02);
        final ManufactureDto manufactureDto = ITUtil.createManufactureDto(
                ITUtil.MANUFACTURE_ID_02, ITUtil.MANUFACTURE_NAME_02);

        final ManufactureDto ManufactureDtoTest = manufactureEntityToDtoConverter
                .convert(manufactureEntity);

        Assertions.assertEquals(manufactureDto, ManufactureDtoTest);
    }

}
