package ru.octory.marketplace.contentservice.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.contentservice.ITUtil;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.it.AbstractSpringBootIT;
import ru.octory.marketplace.contentservice.service.impl.ManufactureServiceImpl;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
//@SpringBootTest
//@ExtendWith(SpringExtension.class)
public class ManufactureServiceIT extends AbstractSpringBootIT {

    @Autowired
    private ManufactureServiceImpl manufactureService;

    @Autowired
    private ManufactureConvertService manufactureConvertService;



//    @Test
    @DisplayName("Тест Добавление производителя в сервисе с проверкой")
//    @Tag("Тест Добавление производителя в сервисе с проверкой")
    public void shouldAddManufactureAndGet() {

        final Optional<ManufactureEntity> optional =
                manufactureService.get(ITUtil.MANUFACTURE_ID_02);
        if (optional.isEmpty()) {
            log.info("optional = EMPY");
        } else {
            log.info("optional = " + optional.get().getName());
        }


        final ManufactureDto manufactureDto = ITUtil.createManufactureDto(
                ITUtil.MANUFACTURE_ID_02, ITUtil.MANUFACTURE_NAME_02);

        DataRequest<ManufactureDto> dataRequest = ITUtil.createDataRequest(manufactureDto);

        ResponseEntity<DataResponse<ManufactureDto>> response =
                manufactureConvertService.postEntity(dataRequest);

        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getData());

        final ManufactureDto manufactureDtoTest = response.getBody().getData();
        assertEquals(manufactureDto, manufactureDtoTest);

        final ManufactureDto manufactureDtoNext = ITUtil.createManufactureDto(
                ITUtil.MANUFACTURE_ID_03, ITUtil.MANUFACTURE_NAME_03);

        DataRequest<ManufactureDto> dataRequestNext = ITUtil.createDataRequest(manufactureDtoNext);

        ResponseEntity<DataResponse<ManufactureDto>> responseNext =
                manufactureConvertService.postEntity(dataRequestNext);
        assertNotNull(responseNext.getBody());
        assertNotNull(responseNext.getBody().getData());
        final ManufactureDto manufactureDtoTestNext = responseNext.getBody().getData();
        assertEquals(manufactureDtoNext, manufactureDtoTestNext);

        final Optional<ManufactureEntity> optionalEntity =
                manufactureService.get(ITUtil.MANUFACTURE_ID_02);
        assertFalse(optionalEntity.isEmpty());
        final ManufactureEntity entityTest = optionalEntity.get();
        final ManufactureEntity manufactureEntity = ITUtil.createManufactureEntity(
                ITUtil.MANUFACTURE_ID_02, ITUtil.MANUFACTURE_NAME_02);
        assertEquals(manufactureEntity,entityTest);

        final Optional<ManufactureEntity> optionalEntityNext =
                manufactureService.get(ITUtil.MANUFACTURE_ID_03);
        assertFalse(optionalEntityNext.isEmpty());
        final ManufactureEntity entityNextTest = optionalEntityNext.get();
        final ManufactureEntity manufactureEntityNext = ITUtil.createManufactureEntity(
                ITUtil.MANUFACTURE_ID_03, ITUtil.MANUFACTURE_NAME_03);
        assertEquals(manufactureEntityNext,entityNextTest);

    }



        /**
         * Очистим базу
         */
    @AfterEach
    public void after() {
//        manufactureService.deleteAll();
    }


}
