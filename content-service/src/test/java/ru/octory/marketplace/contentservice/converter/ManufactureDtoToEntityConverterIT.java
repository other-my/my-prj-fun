package ru.octory.marketplace.contentservice.converter;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.contentservice.ITUtil;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

@Slf4j
@SpringBootTest
public class ManufactureDtoToEntityConverterIT {

    @Autowired
    private ManufactureDtoToEntityConverter manufactureDtoToEntityConverter;

    @Test
    @DisplayName("Тест Converter ManufactureDto To Entity")
//    @Tag("Тест Converter ManufactureDto To Entity")
    public void convert() {
        final ManufactureEntity manufactureEntity = ITUtil.createManufactureEntity(
                ITUtil.MANUFACTURE_ID_02, ITUtil.MANUFACTURE_NAME_02);
        final ManufactureDto manufactureDto = ITUtil.createManufactureDto(
                ITUtil.MANUFACTURE_ID_02, ITUtil.MANUFACTURE_NAME_02);

        final ManufactureEntity manufactureEntityTest = manufactureDtoToEntityConverter
                .convert(manufactureDto);
        log.info("manufactureDto = {}", manufactureDto);
        log.info("manufactureEntity = {}", manufactureEntity);
        log.info("manufactureEntityTest = {}", manufactureEntityTest);

        Assertions.assertEquals(manufactureEntity, manufactureEntityTest);
    }

}
