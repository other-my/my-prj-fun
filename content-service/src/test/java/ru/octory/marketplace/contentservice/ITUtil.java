package ru.octory.marketplace.contentservice;

import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.DataRequest;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;

import java.time.LocalDate;
import java.util.UUID;

public final class ITUtil {

    public static final Long MANUFACTURE_ID_01 = 1L;
    public static final String MANUFACTURE_NAME_01 = "NAME_01";
    public static final Long MANUFACTURE_ID_02 = 2L;
    public static final String MANUFACTURE_NAME_02 = "NAME_02";
    public static final Long MANUFACTURE_ID_03 = 3L;
    public static final String MANUFACTURE_NAME_03 = "NAME_03";



    public static final String CURRENCY_CODE_101 = "AUD";
    public static final String ID_101 = CURRENCY_CODE_101;
    public static final String NAME_101 = "Australian Dollar";
    public static final Integer NUMERIC_CODE_101 = 36;
    public static final Integer DIGITS_CODE_101 = 2;

    public static final String CURRENCY_CODE_102 = "EUR";
    public static final String ID_102 = CURRENCY_CODE_102;
    public static final String NAME_102 = "Euro";
    public static final Integer NUMERIC_CODE_102 = 978;
    public static final Integer DIGITS_CODE_102 = 2;

    public static final String BASE_CURRENCY_CODE_201 = "RUB";
    public static final String EXCHANGE_CURRENCY_CODE_201 = "EUR";
    public static final Double BASE_AMOUNT_201 = 31.35;
    public static final Double EXCHANGE_AMOUNT_201 = 0.34;
    public static final String EXCHANGE_DATE_201 = "2020-10-30";
    public static final LocalDate EXCHANGE_LOCAL_DATE_201 = LocalDate.parse("2020-10-30");

    public static final String BASE_CURRENCY_CODE_202 = "RUB";
    public static final String EXCHANGE_CURRENCY_CODE_202 = "AUD";
    public static final Double BASE_AMOUNT_202 = 21.35;
    public static final Double EXCHANGE_AMOUNT_202 = 0.38;
    public static final LocalDate EXCHANGE_LOCAL_DATE_202 = LocalDate.now();

    private ITUtil() {
    }

    public static ManufactureEntity createManufactureEntity(String name) {
        final ManufactureEntity manufactureEntity = new ManufactureEntity(name);
        return manufactureEntity;
    }

    public static ManufactureEntity createManufactureEntity(Long id, String name) {
        final ManufactureEntity manufactureEntity = new ManufactureEntity(name);
        manufactureEntity.setId(id);
        return manufactureEntity;
    }


    public static ManufactureDto createManufactureDto(Long id, String name) {
        final ManufactureDto manufactureDto = new ManufactureDto();
        manufactureDto.setId(id);
        manufactureDto.setName(name);
        return manufactureDto;
    }

    public static DataRequest<ManufactureDto> createDataRequest(ManufactureDto dto) {
        final DataRequest<ManufactureDto> dataRequest = new DataRequest<>();
        dataRequest.setData(dto);
        return dataRequest;
    }

}
