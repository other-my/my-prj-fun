package ru.octory.marketplace.contentservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import ru.octory.marketplace.common.data.RestApiConstants;
import ru.octory.marketplace.common.dto.content.ManufactureDto;
import ru.octory.marketplace.common.dto.data.DataResponse;
import ru.octory.marketplace.common.dto.data.StatusType;
import ru.octory.marketplace.contentservice.ITUtil;
import ru.octory.marketplace.contentservice.entity.ManufactureEntity;
import ru.octory.marketplace.contentservice.it.AbstractSpringBootIT;
import ru.octory.marketplace.contentservice.service.ManufactureConvertService;
import ru.octory.marketplace.contentservice.service.impl.ManufactureConvertServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

//@SpringBootTest
@AutoConfigureMockMvc
public class ManufactureControllerIT extends AbstractSpringBootIT {
//public class ManufactureControllerIT {

    private static final String API_URL_VARIABLE_ID =
            ManufactureController.API_URL + "/" + RestApiConstants.VARIABLE_ID;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

//    @MockBean
//    private ManufactureConvertService manufactureConvertService;

//    @InjectMocks
//    private ManufactureConvertServiceImpl manufactureConvertService;

    @Autowired
    private ManufactureConvertService manufactureConvertService;

//    @InjectMocks
//    private ManufactureController manufactureController;

    @BeforeEach
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

//    @Test пока не получилось работать с H2
    @DisplayName("REST test addManufacture")
//    @Tag("Тест REST Добавление производителя")
    public void addManufactureId() throws Exception {
        // Сформируем запрос
        final MockHttpServletRequestBuilder request =
                MockMvcRequestBuilders.post(ManufactureController.API_URL + "/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "\t\"data\": {\n" +
                                "\t\t\"id\": " + ITUtil.MANUFACTURE_ID_01 + ",\n" +
                                "\t\t\"name\": \"" + ITUtil.MANUFACTURE_NAME_01 + "\"\n" +
                                "\t}\n" +
                                "}");


        // Проверяем что получены нужные значения
        mockMvc.perform(request)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.status").value(StatusType.SUCCESSFUL.name()))
                .andExpect(jsonPath("$.data.id").value(ITUtil.MANUFACTURE_ID_01.toString()))
                .andExpect(jsonPath("$.data.name").value(ITUtil.MANUFACTURE_NAME_01));

    }

    @Test
    @DisplayName("REST test addManufacture")
//    @Tag("Тест REST Добавление производителя")
    public void addManufacture() throws Exception {
        // Сформируем запрос
        final MockHttpServletRequestBuilder request =
                MockMvcRequestBuilders.post(ManufactureController.API_URL + "/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "\t\"data\": {\n" +
                                "\t\t\"name\": \"" + ITUtil.MANUFACTURE_NAME_01 + "\"\n" +
                                "\t}\n" +
                                "}");
        // Проверяем что получены нужные значения
        mockMvc.perform(request)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.status").value(StatusType.SUCCESSFUL.name()))
                .andExpect(jsonPath("$.data.id").isNumber())
                .andExpect(jsonPath("$.data.name").value(ITUtil.MANUFACTURE_NAME_01));
        // Почистим БД
        final ResponseEntity<DataResponse<ManufactureDto>> response =
                manufactureConvertService.getEntity(ITUtil.MANUFACTURE_NAME_01);
        assertNotNull(response.getBody());
        final ManufactureDto manufactureDtoTest = ITUtil.createManufactureDto(
                ITUtil.MANUFACTURE_ID_01, ITUtil.MANUFACTURE_NAME_01);
        final ManufactureDto manufactureDto = response.getBody().getData();
        assertEquals(manufactureDto.getName(), manufactureDtoTest.getName());
        manufactureConvertService.deleteEntity(manufactureDto.getId());
    }

}
