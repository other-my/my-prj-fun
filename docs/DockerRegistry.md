
**Docker Registry** — это инструмент, хранения и обмена docker образами не используя Docker Hub. Его образ можно скачать и запустить с официального репозитория **Docker Hub.**

как настроить **Docker Registry** в качестве своего приватного репозиторий Docker. Docker Registry можно использовать для хранения и обмена образами docker.

Для запуска docker registry выполните в консоли:

`docker run -d -p 5000:5000 --name registry registry:2`

Эта команда скачает **registry** и запустит контейнер на порту **5000** (ключ -p 5000:5000). Данный контейнер будет запускаться автоматически при старте docker.

В минимальной конфигурации мы запустили наш частный аналог **docker hub**. Давайте попробуем отправить свой образ в только что созданный репозиторий. Для это нам нужно протегировать образ. Возьмем для примера, образ, который мы создали [в прошлой статье](https://winitpro.ru/index.php/2021/02/17/sozdaem-prostoj-mikroservis-v-docker/). Либо можете использовать любой другой образ:

`# sudo docker tag microsevice_v1 <host ip>:5000/microsevice_v1`


Pull (or build) some image from the hub

`docker pull ubuntu`

Tag the image so that it points to your registry

`docker image tag ubuntu localhost:5000/myfirstimage`

Push it

`docker push localhost:5000/myfirstimage`

Pull it back

`docker pull localhost:5000/myfirstimage`

Now stop your registry and remove all data

`docker container stop registry && docker container rm -v registry`

