import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManufactureDialogComponent } from './edit-manufacture-dialog.component';

describe('EditManufactureDialogComponent', () => {
  let component: EditManufactureDialogComponent;
  let fixture: ComponentFixture<EditManufactureDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditManufactureDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManufactureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
