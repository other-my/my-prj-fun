import {Component, Inject, OnInit} from '@angular/core';
import {DialogAction, DialogResult} from "../../../data/shema/general/DialogAction";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ManufactureDto} from "../../../data/classes/ManufactureDto";
import {ActivatedRoute} from "@angular/router";

/**
 * создание/редактирование производителя
 */
@Component({
  selector: 'app-edit-manufacture-dialog',
  templateUrl: './edit-manufacture-dialog.component.html',
  styleUrls: ['./edit-manufacture-dialog.component.scss']
})
export class EditManufactureDialogComponent implements OnInit {

  constructor(
    // для работы с текущим диалог. окном
    private dialogRef: MatDialogRef<EditManufactureDialogComponent>,
    // данные, которые передали в диалоговое окно
    @Inject(MAT_DIALOG_DATA) private data: [ManufactureDto, string],
    // для открытия нового диалогового окна (из текущего) - например для подтверждения удаления
    private dialog: MatDialog,

    public route: ActivatedRoute
  ) { }

  // текст для диалогового окна
  dialogTitle: string | undefined;

  // переданный объект для редактирования
  manufactureDto!: ManufactureDto;
  // можно ли удалять объект (активна ли кнопка удаления)
  canDelete = true;


  ngOnInit(): void {
    // получаем переданные в диалоговое окно данные
    this.manufactureDto = this.data[0];
    this.dialogTitle = this.data[1];

    // если было передано значение, значит это редактирование, поэтому делаем удаление возможным (иначе скрываем иконку)
    if (this.manufactureDto && this.manufactureDto.id && this.manufactureDto.id !== 0) {
      this.canDelete = true;
    }
  }

  /**
   * нажали ОК
   */
  confirm(): void {
    this.dialogRef.close(new DialogResult(DialogAction.SAVE, this.manufactureDto));
  }

  /**
   * нажали отмену
   */
  cancel(): void {
    this.dialogRef.close(new DialogResult(DialogAction.CANCEL));
  }

  /**
   * нажали Удалить
   */
  delete(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dialogTitle: 'Подтвердите действие',
        message: `Вы действительно хотите удалить производителя: "${this.manufactureDto?.name}"? (сами задачи не удаляются)`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      // если просто закрыли окно, ничего не нажав
      if (!(result)) {
        return;
      }
      if (result.action === DialogAction.OK) {
        // нажали удалить
        this.dialogRef.close(new DialogResult(DialogAction.DELETE));
      }
    });
  }

}
