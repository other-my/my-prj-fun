import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {MachineDto} from "../../../data/classes/MachineDto";
import {DialogAction, DialogResult} from "../../../data/shema/general/DialogAction";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";
import {ManufactureDto} from "../../../data/classes/ManufactureDto";
import {Manufacture} from "../../../data/shema/catalog/Manufacture";

@Component({
  selector: 'app-edit-machine-dialog',
  templateUrl: './edit-machine-dialog.component.html',
  styleUrls: ['./edit-machine-dialog.component.scss']
})
export class EditMachineDialogComponent implements OnInit {

  constructor(
    // для работы с текущим диалог. окном
    private dialogRef: MatDialogRef<EditMachineDialogComponent>,
    // данные, которые передали в диалоговое окно
    @Inject(MAT_DIALOG_DATA) private data: [MachineDto, string, Manufacture[]],
    // для открытия нового диалогового окна (из текущего) - например для подтверждения удаления
    private dialog: MatDialog,

    public route: ActivatedRoute
  ) { }

  // текст для диалогового окна
  dialogTitle: string | undefined;

  manufactures!: Manufacture[];
  // переданный объект для редактирования
  machineDto!: MachineDto;
  // можно ли удалять объект (активна ли кнопка удаления)
  canDelete = true;

  // Переменная используется как двунаправленая на форме для вывода значения текудей переменной
  // и для сохранения значения выбранной на форме, т.к. в переменную machineDto не попадет, то
  // что мы выбираем в выпадающем списке
  curManufactureId!: number;

  ngOnInit(): void {
    // получаем переданные в диалоговое окно данные
    this.machineDto = this.data[0];
    this.dialogTitle = this.data[1];
    // категории для выпадающего списка
    this.manufactures = this.data[2];


    console.log("manufacture Id = " + this.machineDto.manufacture.id);
    if (this.machineDto.manufacture.id) {
      this.curManufactureId = this.machineDto.manufacture.id;
      console.log("curManufactureId = " + this.curManufactureId);
    }

    // если было передано значение, значит это редактирование, поэтому делаем удаление возможным (иначе скрываем иконку)
    if (this.machineDto && this.machineDto.id && this.machineDto.id !== 0) {
      this.canDelete = true;
    }
  }

  /**
   * нажали ОК
   */
  confirm(): void {
    this.machineDto.manufacture._id = this.curManufactureId;
    this.machineDto.manufacture._name = "1";
    this.dialogRef.close(new DialogResult(DialogAction.SAVE, this.machineDto));
  }

  /**
   * нажали отмену
   */
  cancel(): void {
    this.dialogRef.close(new DialogResult(DialogAction.CANCEL));
  }

  /**
   * нажали Удалить
   */
  delete(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dialogTitle: 'Подтвердите действие',
        message: `Вы действительно хотите удалить машину: "${this.machineDto?.model}"? (сами задачи не удаляются)`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      // если просто закрыли окно, ничего не нажав
      if (!(result)) {
        return;
      }
      if (result.action === DialogAction.OK) {
        // нажали удалить
        this.dialogRef.close(new DialogResult(DialogAction.DELETE));
      }
    });
  }

}
