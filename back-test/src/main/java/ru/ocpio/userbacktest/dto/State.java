package ru.ocpio.userbacktest.dto;

public class State {
    int code;
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public State(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
