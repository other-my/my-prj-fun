package ru.ocpio.userbacktest.dto;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Ответ с данными.
 *
 * @param <T> тип данных ответа
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DataResponse<T extends Serializable> extends Response {

    /**
     * Данные ответа.
     */
    private T data;
}
