package ru.ocpio.userbacktest.dto;

public class Answer {
    State status;
    Object responses;

    public State getStatus() {
        return status;
    }

    public void setStatus(State status) {
        this.status = status;
    }

    public Object getResponses() {
        return responses;
    }

    public void setResponses(Object responses) {
        this.responses = responses;
    }

    public Answer(State status, Object responses) {
        this.status = status;
        this.responses = responses;
    }
}
