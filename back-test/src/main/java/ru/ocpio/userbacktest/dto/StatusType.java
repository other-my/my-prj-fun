package ru.ocpio.userbacktest.dto;

import lombok.ToString;

/**
 * Тип статуса.
 */
@ToString
public enum StatusType {

    SUCCESSFUL,
    WARNING,
    ERROR;
}
