package ru.ocpio.userbacktest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserBackTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserBackTestApplication.class, args);
    }

}
