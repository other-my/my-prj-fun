package ru.ocpio.userbacktest.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ocpio.userbacktest.service.EmailService;

/**
 * Авторизация для сервиса Octory
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/octory/auths") // базовый адрес
@Slf4j
//@CrossOrigin(origins = "http://localhost:4200")
public class AuthOctoryController {

    private final EmailService emailService;

    /**
     * Получен запрос на формирование и отправку сообщения клиенту
     * @return HttpStatus
     */
    @GetMapping("/create-send-messsage")
    public ResponseEntity<?> createSendMesssage() {
        log.info("AuthOctoryController --- createSendMesssage --- start");
        emailService.sendMessageForActivateEmail();

        return new ResponseEntity<>("Сообщение сформировано и отправлено клиенту",
                HttpStatus.OK);
    }

}
