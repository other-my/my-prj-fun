package ru.ocpio.userbacktest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ocpio.userbacktest.dto.Answer;
import ru.ocpio.userbacktest.dto.DataResponse;
import ru.ocpio.userbacktest.dto.State;
import ru.ocpio.userbacktest.dto.StatusType;
import ru.ocpio.userbacktest.model.RegisterClient;
import ru.ocpio.userbacktest.model.ResponsePhone;

/**
 * Авторизация для сервиса Ocpio
 */
@RestController
@RequestMapping("/Auths") // базовый адрес
@Slf4j
@CrossOrigin(origins = "http://localhost:4200")
public class AuthController {

//    @GetMapping("/code{phone}")
    @GetMapping("/code")
//    public ResponseEntity<?> sendPhoneForGetCode(@PathVariable String phone) {
    public ResponseEntity<?> sendPhoneForGetCode(@RequestParam(value="phone") String phone) {
        log.info("AuthController --- makeCode --- start");
        log.info("phone = " + phone);
        State state = new State(200, "OKK");
        Answer answer = new Answer(state,"Телефон отправлен");

//        ResponsePhone responsePhone = new ResponsePhone(phone);
//        ResponseEntity<ResponsePhone> response = new ResponseEntity<ResponsePhone>(responsePhone,
//                HttpStatus.OK);
        return new ResponseEntity<>(answer, HttpStatus.OK);
//        return new ResponseEntity<>(phone, HttpStatus.OK);

//        final DataResponse<String> response = new DataResponse<>();
//        return new ResponseEntity<>(response, HttpStatus.OK);

        /*
        final DataResponse<String> response = new DataResponse<>();
        response.setStatus(StatusType.SUCCESSFUL);
        final String message = "Телефон отправлен";
        response.addMessage(HttpStatus.OK.value(), message);
        return new ResponseEntity<>(response, HttpStatus.OK);

         */


//        protected DataResponse<D> createDataResponse(D dto) {
//            final DataResponse<D> dataResponse = new DataResponse<>();
//            dataResponse.setData(dto);
//            return dataResponse;
//        }

    }
    /*
    public List<User> getByParameter(@RequestParam Map<String, String> parameters) {
    logger.info("PARAMETER VALUE " + parameters.get("phone"));
    return service.getByParameter(parameters);
    }

}
     */


    @PostMapping("/client")
    public ResponseEntity<?> register(@RequestBody RegisterClient registerClient) {
        log.info("AuthController --- registerClient = {}:{}", registerClient);
        log.info("AuthController --- register --- start");
        return null;
    }

//    @PostMapping("/client")
//    public ResponseEntity<?> add(@RequestBody RegisterClient registerClient) {
//
//    }
}
