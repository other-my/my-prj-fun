package ru.ocpio.userbacktest.service;

import ru.ocpio.userbacktest.model.EmailDto;
import ru.ocpio.userbacktest.model.EmailTestDto;
import ru.ocpio.userbacktest.model.User;
import ru.ocpio.userbacktest.model.UserTest;

public interface EmailService {

    void sendMessageForActivateEmail();
    EmailDto createDtoForActivateEmail(User user, Integer code);
    EmailDto createDtoTestEmail(UserTest user);
}
