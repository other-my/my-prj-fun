package ru.ocpio.userbacktest.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.ocpio.userbacktest.model.EmailDto;
import ru.ocpio.userbacktest.model.ResultRest;
import ru.ocpio.userbacktest.service.ClientService;

import java.nio.charset.Charset;

@RequiredArgsConstructor
@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    private final RestTemplate restTemplate;

    @Override
    /**
     * Отправка POST запроса email-сервису на отправку собщения на почту клиента с кодом
     */
    public ResultRest sendActivateEmail(EmailDto dto){



        String emailPath = "http://localhost:9500/octory-email/api-v1";
        String emailNew = "http://email";


        log.info(" -- ClientServiceImpl -- sendActivateEmail --- start ");

        log.info("emailPath  = {0}:{} {1}:{}" , emailPath, emailNew);

//        ResponseEntity<BlockedSite> entity = restTemplate.getForEntity(fullServerUrl, BlockedSite.class);

        var result = ResultRest.FAIL;
        try {

            HttpHeaders headers = new HttpHeaders();
//            Charset utf8 = Charset.forName("UTF-8");
//            MediaType mediaType = new MediaType("text", "html", utf8);
//            headers.setContentType(mediaType);
//            headers.set("User-Agent", "mozilla");
//            headers.set("Accept-Language", "ko");
            headers.setContentType(MediaType.APPLICATION_JSON);
            var httpEntity = new HttpEntity<>(dto, headers);
            log.info(" -- httpEntity -- > exchange");
//            var response = restTemplate.exchange(
            ResponseEntity<String> response= restTemplate.exchange(
//                    emailPath + "/email/sendmail",
                    emailPath + "/email/sendmail",
                    HttpMethod.POST,
                    httpEntity,
                    String.class
                    );
            if (response.getStatusCode() == HttpStatus.OK) {
                result = ResultRest.SUCCESS;
            }


//            result = response.getBody();
            log.info("sendActivateEmail({}) -> {}", dto, result);
        } catch (Exception e){
            e.printStackTrace();

            log.error("sendActivateEmail({0}) -> {} {1}:{}", dto, e.getMessage());

        }
        return result;
    }
}
