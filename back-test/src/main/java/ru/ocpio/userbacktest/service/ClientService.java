package ru.ocpio.userbacktest.service;

import ru.ocpio.userbacktest.model.EmailDto;
import ru.ocpio.userbacktest.model.ResultRest;

public interface ClientService {
    ResultRest sendActivateEmail(EmailDto dto);
}
