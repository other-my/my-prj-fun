package ru.ocpio.userbacktest.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.ocpio.userbacktest.model.*;
import ru.ocpio.userbacktest.service.ClientService;
import ru.ocpio.userbacktest.service.EmailService;

@RequiredArgsConstructor
@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

//    public static final String DATA_TEXT = "<text, %1s";

    public static final String DATA_TEXT =
            "<body style='margin:0;padding:0;'> " +
                    "<table role='table' style='width:100%%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;'> " +
                    "<tr> <td align='center' style='padding:0;'> " +
                    "<table role='table' style='width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;'> " +
                    "<tr> <td align='center' style='padding:4px 0 4px;background:#FFFFFF;border:1px solid #cccccc;'> " +
                    "<img src='https://i.logo.co/redfggq7/logo.png' alt='my_logo' width='170' " +
                    "</td></tr><tr> <td style='padding:36px 30px 42px 30px;background:#F0F4F2;font-family:Roboto,sans-serif;'> " +
                    "<p style='font-family: Roboto, sans-serif;'>Текст авпвапва вар о пор ораоа про прп" +
                    "</p><p style='font-family: Roboto, sans-serif;'>" +
                    "</p>" +
                    "</td></tr><tr> <td style='padding:30px;background:#198754;text-align:center;border:1px solid #cccccc;'> " +
                    "<a style='text-decoration:none;color:#ffffff;opacity:50%%' href='#'>Octory.com</a> " +
                    "</td>" +
                    "</tr>" +
                    "</table> " +
                    "</td>" +
                    "</tr>" +
                    "</table> </body>";


    private final ClientService clientService;

    @Override
    public void sendMessageForActivateEmail() {
        User user = new User();
        user.setEmail("man_angelina@mail.ru");
        user.setFullName("Ангелина");
        clientService.sendActivateEmail(createDtoForActivateEmail(user, 4954));

        UserTest userTest = new UserTest();
        userTest.setEmail("man_angelina@mail.ru");
        userTest.setFullName("Ангелина");

//        clientService.sendActivateEmail(createDtoTestEmail(userTest));


    }

    @Override
    public EmailDto createDtoForActivateEmail(User user, Integer code) {
        var email = new EmailDto();
        email.setTo(user.getEmail());
        email.setSubject(AuthConstants.EMAIL_SUBJECT);
        email.setBody(user.getFullName());
        email.setFullName(user.getFullName());
        email.setCode(code);
        log.info("createDtoForActivateEmail {}", email);
        log.info("createDtoForActivateEmail end");
        return email;
    }

    @Override
    public EmailDto createDtoTestEmail(UserTest user) {
        var email = new EmailDto();
//        email.setTo(user.getEmail());
//        email.setSubject(AuthConstants.EMAIL_SUBJECT);
        email.setBody(
                DATA_TEXT
    );
        log.info("createDtoForActivateEmail {}", email);
        log.info("createDtoForActivateEmail end");
        return email;
    }
}
