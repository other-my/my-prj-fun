package ru.ocpio.userbacktest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailTestDto {
    private static final long serialVersionUID = 1L;
    private String to;
    private String subject;
    private String body;
}
