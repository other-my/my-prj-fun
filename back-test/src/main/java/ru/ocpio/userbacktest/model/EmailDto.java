package ru.ocpio.userbacktest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailDto {
//    private static final long serialVersionUID = 1L;
    private String to;
    private String subject;
    private String body;
    private String fullName;
    private Integer code;
}
