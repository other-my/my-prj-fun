package ru.ocpio.userbacktest.model;

public enum ResultRest {
    SUCCESS,
    FAIL,
    ALREADY_EXISTS,
    NOT_FOUND,
    PASSWORD_INCORRECT,
    NOT_ACTIVATE,
    PHONE_EXISTS,
    CODE_REASON_EXISTS,
    IDENTIFICATION_NUMBER_EXISTS
}
