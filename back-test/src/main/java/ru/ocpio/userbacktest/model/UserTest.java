package ru.ocpio.userbacktest.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTest {
    private String email;
    private String fullName;
}
