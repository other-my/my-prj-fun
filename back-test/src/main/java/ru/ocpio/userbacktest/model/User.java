package ru.ocpio.userbacktest.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private String email;
    private String fullName;
}
