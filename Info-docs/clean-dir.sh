#Чтобы сделать файл исполняемым в linux выполните:
#
# chmod ugo+x clean-dir.sh
#
#Теперь выполняем нашу небольшую первую программу:
#
# ./clean-dir.sh
#!/bin/bash
echo "Начинаем чистку"
echo "Информационные и старые каталоги"
rm -r ../common-data
rm -r ../octory-marketplace-email-api
rm -r ../email-service
rm -r ../converter-service
rm -r ../.idea
rm -r ../orders-service
rm -r ../octory-marketplace-web-docker-compose
rm -r ../web
echo "-------------------------------"
echo "Чистка authentication-api"
rm -r ../authentication-api/.mvn
rm -r ../authentication-api/src
find ../authentication-api/ -type f -not -name 'Dockerfile_one' -not -path "../authentication-api/target/*" -delete
echo "-------------------------------"
echo "Чистка common"
rm -r ../common/.mvn
rm -r ../common/src
rm ../common/*
echo "-------------------------------"
echo "Чистка content-service"
rm -r ../content-service/.mvn
rm -r ../content-service/src
find ../content-service/ -type f -not -name 'Dockerfile_one' -not -path "../content-service/target/*" -delete
echo "-------------------------------"
echo "Чистка octory-email"
rm -r ../octory-email/.mvn
rm -r ../octory-email/src
find ../octory-email/ -type f -not -name 'Dockerfile_one' -not -path "../octory-email/target/*" -delete
echo "-------------------------------"
echo "Чистка proxy-service"
rm -r ../proxy-service/.mvn
rm -r ../proxy-service/src
find ../proxy-service/ -type f -not -name 'Dockerfile_one' -not -path "../proxy-service/target/*" -delete
echo "-------------------------------"
echo "Чистка Info-docs"
find . -type f -not -name 'clean-dir.sh' -delete
echo "-------------------------------"
echo "Чистка marketplace"
find ../marketplace/ -type f -not -name 'docker-compose.yml' -delete
echo "-------------------------------"
echo "Чистка корня"
rm ../*
echo "-------------------------------"
echo "-------- Чистка выполнена ---------"

