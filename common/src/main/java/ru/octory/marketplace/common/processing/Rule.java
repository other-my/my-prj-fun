package ru.octory.marketplace.common.processing;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.function.Predicate;

@Data
@AllArgsConstructor
public class Rule <T> {

    private final Predicate<T> fn;
    private final String name;

    public boolean evaluate(T object) {
        return fn.test(object);
    };
}