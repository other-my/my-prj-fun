package ru.octory.marketplace.common.dto.data;

import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Response {

    private MetaInfo metaInfo;

    private StatusType status;

    private List<ResponseMessage> messages;

    /**
     * Конструктор ответа.
     */
    public Response() {
        super();
        this.status = StatusType.SUCCESSFUL;
    }

    /**
     * Добавить сообщение ответа.
     *
     * @param responseMessage сообщение ответа
     * @return сообщение ответа
     */
    public ResponseMessage addMessage(ResponseMessage responseMessage) {
        if (messages == null) {
            messages = new ArrayList<>();
        }
        messages.add(responseMessage);
        return responseMessage;
    }

    /**
     * Добавить сообщение ответа.
     *
     * @param code    код
     * @param message сообщение
     * @return сообщение ответа
     */
    public ResponseMessage addMessage(Integer code, String message) {
        if (messages == null) {
            messages = new ArrayList<>();
        }
        final ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setMessage(message);
        responseMessage.setCode(code);
        messages.add(responseMessage);
        return responseMessage;
    }
}
