package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.octory.marketplace.common.dto.user.OrganizationInfoDto;
import ru.octory.marketplace.common.dto.user.UserInfoDto;
//import ru.octory.marketplace.dto.user.UserInfoDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PricelistDto {
    private Long id;
    private Long inventoryId;
    private String marketplaceId;
    private OrganizationInfoDto organization;
    private Integer price;
}