package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class P <T> {
    private T content;
    private int maxPage;
    private int currentPage;
}
