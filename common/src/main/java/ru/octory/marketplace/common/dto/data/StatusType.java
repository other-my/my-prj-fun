package ru.octory.marketplace.common.dto.data;

import lombok.ToString;

/**
 * Тип статуса.
 */
@ToString
public enum StatusType {

    SUCCESSFUL,
    WARNING,
    ERROR;
}
