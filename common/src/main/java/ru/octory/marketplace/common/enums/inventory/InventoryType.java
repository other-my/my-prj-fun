package ru.octory.marketplace.common.enums.inventory;

public enum InventoryType {
    MACHINE(0),
    ASSEMBL(1),
    PART(2);

    private int value;

    InventoryType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static InventoryType valueOf(int value) {
        for (InventoryType type : values()) {
            if (type.value == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("No enum for value " + value);
    }
}