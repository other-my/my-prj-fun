package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelationshipDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private InventoryDto inventoryParent;
    private InventoryDto inventoryChild;
    private Integer quantity;
}