package ru.octory.marketplace.common.dto.content;

import lombok.*;
import ru.octory.marketplace.common.dto.data.AbstractId;

import javax.validation.constraints.NotBlank;

/**
 * производитель машины, узла, детали
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ManufactureDto extends AbstractId<Long> {
    // производитель
    @NotBlank(message = "The \"name\" attribute must not be empty")
    private String name;
}
