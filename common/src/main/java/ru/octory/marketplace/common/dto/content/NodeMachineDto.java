package ru.octory.marketplace.common.dto.content;

import lombok.*;
import ru.octory.marketplace.common.dto.data.AbstractId;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс узлов машины.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class NodeMachineDto extends AbstractId<Long> {

    /* Если это OEM, то поле code_analog будет пустое,
        а если это аналог, то в поле code_analog будет записан code_oem.
        OEM это или аналог определяет флаг oemOrAnalog.
    */
    // код оригинального узла
    @NotBlank(message = "The \"codeOEM\" attribute must not be empty")
    private String codeOEM;

    // код аналога данного узла
    private String codeAnalog;

    // Название узла
    @NotNull(message = "The \"name\" attribute must not be null")
    private String name;

    // Версия
    @NotNull(message = "The \"manufacture\" attribute must not be null")
    private String version;

    /* Если флаг = true, то это OEM, иначе аналог. */
    @NotBlank(message = "The \"oemOrAnalog\" attribute must not be empty")
    private Boolean oemOrAnalog;

    // Имя файла с изображением
    @NotNull(message = "The \"imageFileName\" attribute must not be null")
    private String imageFileName;

    // Производитель
    @NotNull(message = "The \"manufacture\" attribute must not be null")
    private ManufactureDto manufacture;

    // Стоимость узла
//    private NodePriceDto nodePrice;

    // Список машин, на которых может быть установлен данный узел
    private List<MachineDto> machines = new ArrayList<>();

    // Список деталей узла
//    private List<ComponentNodeDto> componentNodes = new ArrayList<>();

}
