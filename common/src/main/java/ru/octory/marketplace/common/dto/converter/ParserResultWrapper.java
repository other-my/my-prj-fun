package ru.octory.marketplace.common.dto.converter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.octory.marketplace.common.enums.universal.Result;

import java.io.Serial;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParserResultWrapper<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private Result result;
    private T errorPlace;
    private String requestId;
}
