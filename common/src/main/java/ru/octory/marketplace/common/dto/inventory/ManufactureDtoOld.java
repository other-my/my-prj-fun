package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManufactureDtoOld implements Serializable {
    private Integer id;
    private String name;

    public ManufactureDtoOld(String name) {
        this.name = name;
    }
}
