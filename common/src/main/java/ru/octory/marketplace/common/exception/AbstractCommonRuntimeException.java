package ru.octory.marketplace.common.exception;

public abstract class AbstractCommonRuntimeException extends RuntimeException {

    public AbstractCommonRuntimeException() {
        super();
    }

    public AbstractCommonRuntimeException(String s) {
        super(s);
    }

    public AbstractCommonRuntimeException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AbstractCommonRuntimeException(Throwable throwable) {
        super(throwable);
    }

    public AbstractCommonRuntimeException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
