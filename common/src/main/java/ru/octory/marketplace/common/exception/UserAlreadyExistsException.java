package ru.octory.marketplace.common.exception;

import ru.octory.marketplace.common.enums.universal.Result;

public class UserAlreadyExistsException extends RuntimeException{
    private final String message;

    public UserAlreadyExistsException(Result message) {
        this.message = message.name();
    }
}
