package ru.octory.marketplace.common.dto.content;

import lombok.*;
import ru.octory.marketplace.common.dto.data.AbstractId;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * стоимость детали
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ComponentPriceDto extends AbstractId<Long> {
    // Цена
    @NotNull(message = "The \"price\" attribute must not be null")
    private Double price;
    // Дата обновления цены
    @NotNull(message = "The \"dateUpdate\" attribute must not be null")
    private Date dateUpdate;
}
