package ru.octory.marketplace.common.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RegisterRequestDto {

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Long taxIdentificationNumber;
    private Long codeReason;
    private String address;
    private Long msisdn;
    private Boolean isMarketplace;
    private String nameMarketPlace;
}