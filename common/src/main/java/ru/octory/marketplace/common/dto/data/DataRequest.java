package ru.octory.marketplace.common.dto.data;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Запрос с данными.
 *
 * @param <T> тип данных запроса
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DataRequest<T extends Serializable> extends Request {

    /**
     * Данные запроса.
     */
    private T data;
}
