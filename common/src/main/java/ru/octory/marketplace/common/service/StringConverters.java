package ru.octory.marketplace.common.service;

import java.io.Serializable;
import java.util.Map;

public interface StringConverters extends Map<String, StringConverter<? extends Serializable>> {

}
