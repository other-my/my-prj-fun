package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryWrapper implements Serializable {
    private static final long serialVersionUID = 1L;
    RelationshipDto dto;
    File file;
}
