package ru.octory.marketplace.common.utils;

public class Constants {
    public static final String PROXY_AUTHENTICATION_REGISTER = "proxy.authentication.register";
    public static final String AUTHENTICATION_EMAIL = "authentication.email";
    public static final String PROXY_CONVERTER_DOCUMENT = "proxy.converter.document";
    public static final String CONVERTER_CONTENT_DOCUMENT = "converter.content.document";
    public static final String PROXY_CONTENT_INVENTORY = "proxy.content.inventory";
    public final static String INVENTORY_MACHINE = "machine";
    public final static String INVENTORY_ASSEMBLE = "assemble";
    public final static String INVENTORY_PART = "part";
    public static final String EMAIL_CLAIM = "email";
    public static final String ROLE_CLAIM = "role";
    public static final String HEADER_NAME = "octory_token";
    public static final String HEADER_PREFIX = "Bearer";
    public static final String SECRET_TOKEN = "b3JhY2xlK29jdG9yeQ";
    public static final Long EXPIRATION_TIME = 864000000L;
    public static final String CSV_FILE_LOCATION = "/opt/files/csv/";
    public static final String IMG_FILE_LOCATION = "/opt/files/img/%s.png";

}
