package ru.octory.marketplace.common.dto.history;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.octory.marketplace.common.enums.universal.Result;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoryConverterDto {
    private LocalDateTime date;
    private String document;
    private String type;
    private String partName;
    private String partNum;
    private String partNumberOEM;
    private String version;
    private String year;
    private String typeRef;
    private String partNameRef;
    private String partNumberRef;
    private String partNumberOEMRef;
    private String versionRef;
    private String yearRef;
    private String quantity;
    private Result result;
}
