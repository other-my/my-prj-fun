package ru.octory.marketplace.common.enums.user;

public enum RoleType {
    ROLE_SUBSCRIBER(0),
    ROLE_MARKETPLACE(1),
    ROLE_ADMIN(2);
    private long value;

    RoleType(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public static RoleType valueOf(int value) {
        for (RoleType roleType : values()) {
            if (roleType.value == value) {
                return roleType;
            }
        }
        throw new IllegalArgumentException("No enum for value " + value);
    }
}