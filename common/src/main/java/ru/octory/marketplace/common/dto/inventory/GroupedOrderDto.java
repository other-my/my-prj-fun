package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.octory.marketplace.common.enums.orders.OrderStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupedOrderDto {
    private String orderId;
    private LocalDateTime dateAdded;
    private List<OrderDto> orders;
    private OrderStatus status;
}
