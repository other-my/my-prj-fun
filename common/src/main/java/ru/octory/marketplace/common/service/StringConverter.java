package ru.octory.marketplace.common.service;

import java.io.Serializable;

public interface StringConverter<O extends Serializable> {

    String getType();

    O convert(String input);
}
