package ru.octory.marketplace.common.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserInfoDto {
    private String id;
    private String email;
    private String role;
    private String name;
    private String address;
    private String organization;
    private String callbackUrl;
    private Long taxIdentificationNumber;
}
