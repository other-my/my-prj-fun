package ru.octory.marketplace.common.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class MarketplaceUserRequestDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private UUID userId;
    private Integer code;
    private Integer RoleId;
    private String email;
}