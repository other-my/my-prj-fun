package ru.octory.marketplace.common.enums.orders;

public enum OrderStatus {
    PREPARE,
    SEND,
    PAYED,
    DELIVERED,
    ERROR,
}
