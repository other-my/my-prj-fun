package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;

    public TypeDto(String name) {
        this.name = name;
    }
    public TypeDto(Integer id) {
        this.id = id;
    }
}