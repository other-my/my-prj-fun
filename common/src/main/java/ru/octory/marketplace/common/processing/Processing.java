package ru.octory.marketplace.common.processing;

public interface Processing<T> {

    boolean process(T object);
}
