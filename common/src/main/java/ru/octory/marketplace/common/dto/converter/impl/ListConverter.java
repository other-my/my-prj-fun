package ru.octory.marketplace.common.dto.converter.impl;

import ru.octory.marketplace.common.dto.converter.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListConverter<I extends Serializable, O extends Serializable> {

    private final Converter<I, O> converter;

    public ListConverter(Converter<I, O> converter) {
        this.converter = converter;
    }

    /**
     * Конвертирует список данных типа I в тип O.
     *
     * @param input входящий список данных
     * @return исходящий список данных
     */
    public List<O> convert(List<I> input) {
        final List<O> output = new ArrayList<>();
        if (input != null) {
            input.forEach(i -> {
                final O d = converter.convert(i);
                output.add(d);
            });
        }
        return output;
    }
}
