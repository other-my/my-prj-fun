package ru.octory.marketplace.common.dto.converter.impl;

import ru.octory.marketplace.common.dto.data.NamedIdentifiable;

import java.io.Serializable;

/**
 * Абстракция добавляет автомитически имя и Id к классу
 * @param <T>
 * @param <I>
 * @param <O>
 */
public abstract class AbstractNamedIdentifiableConverter
        <T extends Serializable, I extends NamedIdentifiable<T>, O extends NamedIdentifiable<T>>
        extends AbstractIdentifiableConverter<T, I, O> {

    @Override
    public O convert(I input) {
        final O output = super.convert(input);
        output.setName(input.getName());
        output.setDescription(input.getDescription());
        return output;
    }
}
