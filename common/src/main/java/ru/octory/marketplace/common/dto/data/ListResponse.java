package ru.octory.marketplace.common.dto.data;

import lombok.*;

import java.io.Serializable;

/**
 * Ответ со списочным результатом данных.
 *
 * @param <T> Тип списочного результата данных
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ListResponse<T extends Serializable> extends Response {

    /**
     * Атрибуты списка.
     */
    private ListAttributes listAttributes;

    /**
     * Списочный результат данных.
     */
    private ListResult<T> listResult;
}
