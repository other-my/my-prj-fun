package ru.octory.marketplace.common.service.impl;

import ru.octory.marketplace.common.exception.ConverterException;
import ru.octory.marketplace.common.service.StringConverter;
import ru.octory.marketplace.common.service.StringConverters;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class StringConvertersImpl
        extends HashMap<String, StringConverter<? extends Serializable>> implements StringConverters {

    public StringConvertersImpl(List<StringConverter<? extends Serializable>> converters) {
        super(converters.stream().collect(Collectors.toMap(
                StringConverter::getType, Function.identity())));
    }

    @Override
    public StringConverter<? extends Serializable> get(Object type) {
        final StringConverter<? extends Serializable> stringConverter = super.get(type);
        if (stringConverter == null) {
            throw new ConverterException("No converter found for type: '" + type + "'");
        }
        return stringConverter;
    }
}
