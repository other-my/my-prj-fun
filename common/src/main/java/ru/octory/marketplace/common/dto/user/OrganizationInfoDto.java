package ru.octory.marketplace.common.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationInfoDto {
    private String email;
    private String address;
    private Long taxIdentificationNumber;
    private String organization;
    private Long msisdn;
    private Long codeReason;
}
