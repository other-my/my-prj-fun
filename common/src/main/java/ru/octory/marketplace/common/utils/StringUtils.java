package ru.octory.marketplace.common.utils;

import org.apache.commons.codec.digest.DigestUtils;
import ru.octory.marketplace.common.exception.IncorrectException;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

public class StringUtils {
    public static String encryptText(String text) {
        if (text.length() != 0 && text != null) {
            return new String(Base64.getEncoder().encode(text.getBytes(StandardCharsets.UTF_8)));
        } else {
            throw new IncorrectException();
        }
    }

    public static String decryptText(String text) {
        if (text.length() != 0 && text != null) {
            return new String(Base64.getDecoder().decode(text.getBytes(StandardCharsets.UTF_8)));
        } else {
            throw new IncorrectException();
        }
    }

    public static String md5FromString(String text) {
        return DigestUtils.md5Hex(text);
    }

    public static boolean checkLengthAllWords(String... strings){
        return Arrays.stream(strings).anyMatch(str -> str.length() > 1);
    }

    public static String checkLengthOrNull(String text) {
        return text.length() < 1 ? null : text;
    }
    public static String cleanSpaces(String str){
        return str == null ? null : str.replaceAll("\\s+", "");
    }

    public static boolean strIsEmpty(String str){
        return str == null || str.length() < 1;
    }
}
