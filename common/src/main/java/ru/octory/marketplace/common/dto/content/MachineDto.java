package ru.octory.marketplace.common.dto.content;

import lombok.*;
import ru.octory.marketplace.common.dto.data.AbstractId;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Машины
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MachineDto extends AbstractId<Long> {

    // Код машины
    @NotBlank(message = "The \"code\" attribute must not be empty")
    private String code;
    // Модель машины
    @NotNull(message = "The \"model\" attribute must not be null")
    private String model;
    // Год выпуска
    private Integer year;
    // Имя файла с изображением
    private String imageFileName;

    поменяем название переменной с  manufactureDto на manufacture
    аналогично делаем и на фронте

    // Производитель
    @NotNull(message = "The \"manufacture\" attribute must not be null")
    private ManufactureDto manufactureDto;

    // Список узлов машины
    private List<NodeMachineDto> nodeMachinesDto = new ArrayList<>();

}

