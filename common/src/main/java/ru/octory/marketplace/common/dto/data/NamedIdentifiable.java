package ru.octory.marketplace.common.dto.data;

import java.io.Serializable;

public interface NamedIdentifiable<I extends Serializable> extends Identifiable<I> {

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);
}
