package ru.octory.marketplace.common.exception;

import ru.octory.marketplace.common.exception.AbstractCommonRuntimeException;

public class ConverterException extends AbstractCommonRuntimeException {

    public ConverterException(String s) {
        super(s);
    }

    public ConverterException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
