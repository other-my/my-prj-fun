package ru.octory.marketplace.common.dto.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Ответ с данными.
 *
 * @param <T> тип данных ответа
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DataResponse<T extends Serializable> extends Response {

    /**
     * Данные ответа.
     */
    private T data;
}
