package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.octory.marketplace.common.dto.user.UserInfoDto;

import java.io.Serial;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private PricelistDto pricelist;
    private UserInfoDto consumer;
    private Integer count;
}