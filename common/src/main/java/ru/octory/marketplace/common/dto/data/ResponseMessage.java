package ru.octory.marketplace.common.dto.data;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Сообщение ответа.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class ResponseMessage implements Serializable {

    /**
     * Сообщение.
     */
    private String message;

    /**
     * Код.
     */
    private Integer code;
}
