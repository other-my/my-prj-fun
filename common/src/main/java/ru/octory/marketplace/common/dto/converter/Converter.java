package ru.octory.marketplace.common.dto.converter;

public interface Converter<I, O> {

    O convert(I input);
}
