package ru.octory.marketplace.common.dto.data;

import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Мета информация.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MetaInfo extends AbstractId<String> {

    /**
     * Метка времени.
     */
    private Long timestamp;

    /**
     * Сессия.
     */
    private Session session;

    /**
     * Id канала.
     */
    private UUID channelId;
}
