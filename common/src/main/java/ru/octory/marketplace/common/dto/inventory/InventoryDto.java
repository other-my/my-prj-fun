package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String partNumOEM;
    private String partNum;
    private String partName;
    private String version;
    private Integer year;
    private Boolean imageExists;
    private TypeDto type;
    private ManufactureDtoOld manufacture;
    private Integer minPrice;
    private Integer maxPrice;

    public InventoryDto(Long id) {
        this.id = id;
    }
}
