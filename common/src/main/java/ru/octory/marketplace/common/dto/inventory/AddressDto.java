package ru.octory.marketplace.common.dto.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String area;
    private String city;
    private String street;
    private String house;
}