package com.config;

import java.util.ArrayList;
import java.util.Date;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

  /**
   * Создать Swagger Docket.
   *
   * @return Swagger Docket
   */
  @Bean
  public Docket customImplementation() {
    return new Docket(DocumentationType.SWAGGER_2)
        .ignoredParameterTypes(AuthenticationPrincipal.class)
        .apiInfo(getApiInfo())
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.asvoip.ump.emailio.controller"))
        .paths(PathSelectors.any())
        .build()
        .directModelSubstitute(Date.class, Long.class);
  }

  private ApiInfo getApiInfo() {
    return new ApiInfo("UMP Emailio",
        "<b>UMP Emailio REST API документация<b>\n\n",
        "0.1",
        "",
        new Contact("", "", ""),
        "", "",
        new ArrayList<>());
  }
}
