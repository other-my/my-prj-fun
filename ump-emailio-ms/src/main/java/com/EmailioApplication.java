/*
UMP (Unified Modular Platform)
Copyright (C) 2019  Dmitry Cheremnov (d.cheremnov@asvoip.com)
See LICENSE for license information.
*/

package com;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ImportResource;

/**
 * Main class - EmailioApplication.
 */
@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@ImportResource("classpath:ump-emailio.xml")
@EnableFeignClients({
    "com.asvoip.ump.tenantmanager.client",
    "com.asvoip.ump.templatemanager.client"
})
@Slf4j
public class EmailioApplication {

  /**
   * Главный метод.
   *
   * @param args аргументы
   */
  public static void main(String[] args) {
    SpringApplication.run(EmailioApplication.class, args);
    log.info("Starting up: EmailioApplication");
  }
}
