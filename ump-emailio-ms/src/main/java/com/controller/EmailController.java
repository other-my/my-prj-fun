
@Api(value = EmailController.API_URL)
@RestController
@RequestMapping(EmailController.API_URL)
@Slf4j
public class EmailController
        extends AbstractCrudRestController<Long, EmailEntity, Email, EmailService,
        Converter<EmailEntity, Email>, Converter<Email, EmailEntity>>
        implements EmailResource {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + Email.EMAIL_RESOURCE;

    @Autowired
    public EmailController(
            EmailService emailService,
            Converter<EmailEntity, Email> entityToDtoConverter,
            Converter<Email, EmailEntity> dtoToEntityConverter) {
        super(emailService, entityToDtoConverter, dtoToEntityConverter);
    }

    @ApiOperation(value = "Получить сообщение")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = Email.class)})
    @GetMapping(value = RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<Email>> get(@PathVariable(RestApiConstants.PARAM_ID) Long id) {
        return getEntity(id);
    }

    @ApiOperation(value = "Создать сообщение")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_CREATED, message = RestApiConstants.MESSAGE_CREATED,
                    response = Email.class)})
    @PostMapping
    public ResponseEntity<DataResponse<Email>> post(@RequestBody DataRequest<Email> request) {
        return postEntity(request);
    }

    @ApiOperation(value = "Обновить сообщение")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = Email.class)})
    @PutMapping(value = RestApiConstants.VARIABLE_ID)
    @RestApiValidation(RestApiValidatorConfig.CONSTRAINTS_REST_API_VALIDATOR)
    public ResponseEntity<DataResponse<Email>> put(
            @PathVariable(RestApiConstants.PARAM_ID) Long id, @RequestBody DataRequest<Email> request) {
        return putEntity(id, request);
    }

    @ApiOperation(value = "Обновить статус сообщения")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = Email.class)})
    @PatchMapping(value = RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<Email>> patch(
            @PathVariable(RestApiConstants.PARAM_ID) Long id,
            @RequestBody DataRequest<HashMap<String, Object>> request) {
        return patchEntity(id, request);
    }

    @ApiOperation(value = "Удалить сообщение")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = Email.class)})
    @DeleteMapping(value = RestApiConstants.VARIABLE_ID)
    public ResponseEntity<DataResponse<Email>> delete(
            @PathVariable(RestApiConstants.PARAM_ID) Long id) {
        return deleteEntity(id);
    }

    @ApiOperation(value = "Найти сообщения")
    @ApiResponses(value = {
            @ApiResponse(code = RestApiConstants.CODE_OK, message = RestApiConstants.MESSAGE_OK,
                    response = ListResponse.class)})
    @GetMapping
    public ResponseEntity<ListResponse<Email>> find(
            @RequestParam(name = RestApiConstants.PARAM_LIMIT, required = false) Integer limit,
            @RequestParam(name = RestApiConstants.PARAM_OFFSET, required = false) Integer offset,
            @RequestParam(name = RestApiConstants.PARAM_SORT, required = false) String sort,
            @RequestParam(name = RestApiConstants.PARAM_FILTERS, required = false) String filters) {
        return findEntities(limit, offset, sort, filters);
    }
}
